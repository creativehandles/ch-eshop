<div class="message mt-2">
    @if (session()->has('success'))
    <script>
        success("{{ session()->get('success') }}");
    </script>
    @elseif (session()->has('error'))
    <script>
        error("{{ session()->get('error') }}");
    </script>
    @endif
</div>

<script>
    function success(message) {
        console.log(message);
        toastr.success(`${message}`, "{{__('alerts.Success')}}", {
            timeOut: 5000,
            title: "ABC",
            fadeOut: 1000,
            progressBar: true,
        });
    }

    function error(message) {
        toastr.error(`${message}`, "{{__('alerts.Error')}}")
    }
</script>