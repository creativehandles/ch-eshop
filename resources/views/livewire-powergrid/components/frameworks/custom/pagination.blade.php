<div class="d-flex flex-column flex-lg-row align-items-sm-center justify-content-between">

    @if($recordCount === 'full')

    <div class="dataTables_info" id="thegrid_info" role="status" aria-live="polite">
        @if($paginator->total()>0)
        {{__('livewire-powergrid::datatable.pagination.showing')}}
        {{$paginator->firstItem()}}
        {{__('livewire-powergrid::datatable.pagination.to')}}
        {{$paginator->lastItem()}}
        {{__('livewire-powergrid::datatable.pagination.of')}}
        {{$paginator->total()}}
        {{__('livewire-powergrid::datatable.pagination.results')}}
        @endif
    </div>
    
    @elseif($recordCount === 'short')
    <span class="d-block mb-2 my-md-2 me-1">
        <span class="text-secondary"> {{ $paginator->firstItem() }}</span>
        -
        <span class="text-secondary"> {{ $paginator->lastItem() }}</span>
        |
        <span class="text-secondary"> {{ $paginator->total() }}</span>
    </span>
    @elseif($recordCount === 'min')
    <span class="d-block mb-2 my-md-2 me-1">
        <span class="text-secondary"> {{ $paginator->firstItem() }}</span>
        -
        <span class="text-secondary"> {{ $paginator->lastItem() }}</span>
    </span>
    @endif
    @if ($paginator->hasPages())
    <nav>
        <ul class="pagination mb-0 ms-0 ms-sm-1">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
            <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                <span class="page-link" aria-hidden="true">@lang('pagination.previous')</span>
            </li>
            @else
            <li class="page-item">
                <button type="button" dusk="previousPage" class="page-link" wire:click="previousPage" wire:loading.attr="disabled" rel="prev" aria-label="@lang('pagination.previous')">@lang('pagination.previous')</button>
            </li>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
            <li class="page-item disabled" aria-disabled="true"><span class="page-link">{{ $element }}</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
            @foreach ($element as $page => $url)
            @if ($page == $paginator->currentPage())
            <li class="page-item active" wire:key="paginator-page-{{ $page }}" aria-current="page"><span class="page-link">{{ $page }}</span></li>
            @else
            <li class="page-item" wire:key="paginator-page-{{ $page }}"><button type="button" class="page-link" wire:click="gotoPage({{ $page }})">{{ $page }}</button></li>
            @endif
            @endforeach
            @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
            <li class="page-item">
                <button type="button" dusk="nextPage" class="page-link" wire:click="nextPage" wire:loading.attr="disabled" rel="next" aria-label="@lang('pagination.next')">@lang('pagination.next')</button>
            </li>
            @else
            <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                <span class="page-link" aria-hidden="true">@lang('pagination.next')</span>
            </li>
            @endif
        </ul>
    </nav>
    @endif
</div>