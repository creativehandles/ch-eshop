<div class="dt--top-section">
    <div>
        <x-livewire-powergrid::actions-header :theme="$theme" :actions="$this->headers" />
    </div>
    <div class="row">
        <div class="col-12 col-sm-6 d-flex justify-content-sm-start justify-content-center">

            @include(powerGridThemeRoot().'.export')

            @includeIf(powerGridThemeRoot().'.toggle-columns')

            @include(powerGridThemeRoot().'.loading')

        </div>

        @if(config('livewire-powergrid.filter') === 'outside')
        <div class="col-12 col-sm-12 mt-sm-0 mt-3">
            @include(powerGridThemeRoot().'.filter')
        </div>
        @endif

    </div>
    @include(powerGridThemeRoot().'.search')
</div>

@include(powerGridThemeRoot().'.batch-exporting')

@include(powerGridThemeRoot().'.enabled-filters')