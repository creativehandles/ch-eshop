<div class="d-flex align-items-center" style="padding-left: 10px;">
    <div wire:loading class="spinner-border" role="status" style="color: #656363;">
        <span class="visually-hidden">
            <div class="d-flex align-items-center">
                <strong>Loading...</strong>
                <div class="spinner-border ml-auto" role="status" aria-hidden="true"></div>
            </div>
        </span>
    </div>
</div>