<div class="d-flex justify-content-between">
  
  <div>
    @if(!is_array($data) && $perPageInput && !isset($hidePerPage))
    <div class="d-inline">{{trans('livewire-powergrid::datatable.labels.show')}}</div>
    <select wire:model.lazy="perPage" class="d-inline form-control form-control-sm" style="width: auto;">
      @foreach($perPageValues as $value)
      <option value="{{$value}}">
        @if($value == 0)
        {{ trans('livewire-powergrid::datatable.labels.all') }}
        @else
        {{ $value }}
        @endif
      </option>
      @endforeach
    </select>
    <div class="d-inline">{{trans('livewire-powergrid::datatable.labels.results_per_page')}}</div>
    @endif
  </div>

  <div>
    <div class="btn-toolbar mx-auto" role="toolbar" aria-label="Toolbar with button groups">

      @if($makeFilters->count() > 0)
      <div class="btn-group" role="group" aria-label="First group">
        <button @click="enableFilters = !enableFilters" type="button" class="btn btn-secondary btn-sm">
          <div x-show="enableFilters" style="display: none;">
            {{ trans('livewire-powergrid::datatable.labels.disable_filters') }}
          </div>
          <div x-show="!enableFilters">
            {{ trans('livewire-powergrid::datatable.labels.enable_filters') }}
          </div>
        </button>
      </div>
      @endif

      @if($searchInput)
      <div class="input-group">
        <label for="searchinput" class="ml-2 mr-1">
          {{ trans('livewire-powergrid::datatable.labels.search') }}
        </label>
        <div>
          <input id="searchinput" wire:model.debounce.600ms="search" type="text" class="livewire_powergrid_input form-control form-control-sm" placeholder="{{ trans('livewire-powergrid::datatable.placeholders.search') }}">
        </div>
      </div>
      @endif

    </div>
  </div>