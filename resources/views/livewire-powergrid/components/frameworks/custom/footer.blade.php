@if(!is_array($data))
    <footer">
        <div class="col-auto overflow-auto mt-2 mt-sm-0">
            @if(method_exists($data, 'links'))
                {!! $data->links(powerGridThemeRoot().'.pagination', ['recordCount' => $recordCount]) !!}
            @endif
        </div>
    </footer>
@endif
