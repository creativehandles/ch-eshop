  <div x-data={show:false} class="panel panel-default" style="display: none;">
    <div class="panel-heading" @click="show=!show">
      <button class="btn btn-primary" type="button">
        {{ trans('livewire-powergrid::datatable.buttons.filter') }}
      </button>
    </div>
    <div x-show="show" class="panel-body">
      <div>
        @php
        $customConfig = [];
        @endphp
        <div class="row">

          @foreach(data_get($makeFilters, 'date_picker', []) as $field => $date)
          <div class="col-md-12">
            <x-livewire-powergrid::frameworks.custom.filters.date-picker :date="$date" :inline="false" :tableName="$tableName" classAttr="w-full" :theme="$theme->filterDatePicker" />
          </div>
          @endforeach

          @foreach(data_get($makeFilters, 'select', []) as $field => $select)
          <div class="col-md-12">
            <x-livewire-powergrid::frameworks.custom.filters.select :select="$select" :inline="false" :theme="$theme->filterSelect" />
          </div>
          @endforeach

          @foreach(data_get($makeFilters, 'number', []) as $field => $number)
          <div class="col-md-12">
            <x-livewire-powergrid::frameworks.custom.filters.number :number="$number" :inline="false" :theme="$theme->filterNumber" />
          </div>
          @endforeach

          @foreach(data_get($makeFilters, 'input_text', []) as $field => $inputText)
          <div class="col-md-12">
            <x-livewire-powergrid::frameworks.custom.filters.input-text :enabledFilters="$enabledFilters" :inputTextOptions="$inputTextOptions" :inputText="$inputText" :inline="false" :theme="$theme->filterInputText" />
          </div>
          @endforeach

          @foreach(data_get($makeFilters, 'boolean_filter', []) as $field => $booleanFilter)
          <div class="col-md-12">
            <x-livewire-powergrid::frameworks.custom.filters.boolean-filter :booleanFilter="$booleanFilter" :tableName="$tableName" :inline="false" :theme="$theme->filterBoolean" />
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>