@props([
'column' => null,
'theme' => null,
'sortField' => null,
'sortDirection' => null,
'enabledFilters' => null,
'actions' => null,
'dataField' => null,
])
@php
if (filled($column->dataField)) {
$field = $column->dataField;
} else {
$field = $column->field;
}
@endphp
<th class="{{ $theme->table->thClass .' '. $column->headerClass }}" wire:key="{{ md5($column->field) }}" style="{{ $column->hidden === true ? 'display:none': '' }}; width: max-content; @if($column->sortable) cursor:pointer; @endif {{ $theme->table->thStyle.' '. $column->headerStyle }}">
    <div class="{{ $theme->cols->divClass }}" @if($column->sortable === true) wire:click="sortBy('{{ $field }}')"@endif>
        <span>
            {{ucfirst($column->title)}}
        </span>
        @if($column->sortable === true)
        <span class="text-md">
            @if ($sortField !== $field)
            <div>
                <span>&#8593;</span>
                <span>&#8595;</span>
            </div>
            @elseif ($sortDirection == 'desc')
            <div>
                <span class="text-dark">&#8593;</span>
                <span>&#8595;</span>
            </div>
            @else
            <div>
                <span>&#8593;</span>
                <span class="text-dark">&#8595;</span>
            </div>
            @endif
        </span>
        @endif
    </div>
</th>