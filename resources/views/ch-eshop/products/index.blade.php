@extends('Admin.layout')

@section('content')

<div class="content-body">

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    @include('Admin.partials.breadcumbs',['header'=>__('ch-eshop::breadcrumb.product.index.header')])
                </div>
            </div>
        </div>

        <div class="content-header-right col-md-6 col-12 mb-2">
            <div class="mb-1 pull-right">
                <a href="{{route('admin.ch-eshop.product.create')}}" class="btn btn-secondary btn-block-sm"><i class="ft-file-plus"></i>{{__('ch-eshop::breadcrumb.product.create.link_name')}}</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <livewire:ch-eshop-product-table />
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('scripts')
@yield('component-scripts')
@endsection