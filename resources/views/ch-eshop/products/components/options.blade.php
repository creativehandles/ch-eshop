@foreach($productOptions as $option)
    @php
        $productOptionValue = null;

        if (isset($product)) {
            $productOptionValueObj = $product->optionValues
                ->where('ch_eshop_product_option_id', $option->id)
                ->first();

            if (isset($productOptionValueObj)) {
                $productOptionValueObj->setLocale(request()->get('translation_locale', app()->getLocale()));
            }

            $productOptionValue = isset($productOptionValueObj->value) ?
                $productOptionValueObj->value :
                null;
        }
    @endphp

    @switch($option->type)
        @case(\Creativehandles\ChEshop\Repositories\ProductRepository::PRODUCT_OPTION_TYPE_BOOLEAN)
            <div class="form-group">
                <input type="checkbox" class="switchery" name="options[{{ $option->id }}]" value="1"
                        {{ old("option.$option->id", $productOptionValue) == true ? 'checked' : '' }}>
                <label class="ml-1">{{ $option->name . ($option->unit !== null ? " ($option->unit)" : '') }}</label>
            </div>
        @break
        @default
            <div class="form-group">
                <label>{{ $option->name . ($option->unit !== null ? " ($option->unit)" : '') }}</label>
                <input type="text" class="form-control" name="options[{{ $option->id }}]"
                       value="{{ old("option.$option->id", $productOptionValue) }}">
            </div>
        @break
    @endswitch
@endforeach