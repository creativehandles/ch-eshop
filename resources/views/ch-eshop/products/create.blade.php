@extends('Admin.layout')

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset("vendors/css/forms/toggle/switchery.min.css") }}">
@endsection

@section('content')
<div class="content-body">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    @include('Admin.partials.breadcumbs',['header' => isset($product) ? $product->getTranslation('title', app()->getLocale(), true) : __('ch-eshop::breadcrumb.product.create.header')])
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <x-ch-eshop::nav-tab :headings="$headings" :action="$action">
                @if(isset($product))
                    @method('PUT')
                @endif

                @csrf

                <x-ch-eshop::nav-tab-body :active="'active'" id="basicInfo">
                    <div class="form-group">
                        <label for="language" class="from-control form-label">{{ __('ch-eshop::product/product.language') }}</label>
                        <select id="language" name="language" class="form-control" >
                            @foreach($locales['selectData'] as $key => $value)
                                <option value="{{ $key }}" {{ request()->get('translation_locale', old('language', app()->getLocale())) == $key ? 'selected' : '' }}>{{ $value }}</option>
                            @endforeach
                        </select>
                        @if(isset($errorfield))
                            @error($errorfield) <span class="text-danger">{{ $message }}</span> @enderror
                        @endif
                    </div>

                    <!-- form input -->
                    <x-ch-eshop::form-input :title="__('ch-eshop::product/product.title')" name="product[title]" value="{{ old('product.title', $product->title ?? null) }}" />

                    <!-- form input -->
                    <x-ch-eshop::form-input :title="__('ch-eshop::product/product.short_description')" name="product[short_description]" value="{{old('product.short_description',$product->short_description ?? null)}}" />

                    <!-- form input -->
                    <x-ch-eshop::textarea value="{{old('product.description',$product->description ?? null)}}" label="{{__('ch-eshop::product/product.description')}}" colSize="" name="product[description]" />
                </x-ch-eshop::nav-tab-body>

                <x-ch-eshop::nav-tab-body id="options">
                    @include('ch-eshop::products.components.options')
                </x-ch-eshop::nav-tab-body>

                <x-ch-eshop::nav-tab-body id="variants">
                    <label>Tax rates:
                        @foreach($locales['configData'] as $locale)
                            <span class="badge badge-secondary">{{ $locale['country'] }} - {{ $locale['tax']['rate'] }}%</span>
                        @endforeach
                    </label>

                    <hr>

                    <div class="row mt-2">
                        <div class="col-3">
                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                <a class="nav-link active" id="v-pills-create-tab" data-toggle="pill" href="#v-pills-create" role="tab" aria-controls="v-pills-create" aria-selected="false">{{__('ch-eshop::product/product.variant.create')}}</a>

                                @if(isset($product))
                                    @foreach($product->variants as $variant)
                                        @php
                                        $variant->setLocale(request()->get('translation_locale', app()->getLocale()));
                                        @endphp

                                        <a class="nav-link" id="v-pills-{{ $variant->id }}-tab" data-toggle="pill"
                                           href="#v-pills-{{ $variant->id }}" role="tab" aria-controls="v-pills-{{ $variant->id }}"
                                           aria-selected="false">{{ $variant->code }}</a>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col-9">
                            <div class="tab-content" id="v-pills-tabContent">
                                <div class="tab-pane fade show active" id="v-pills-create" role="tabpanel" aria-labelledby="v-pills-create-tab">
                                    {{--it is necessary to set the variant param to null
                                    because a variable with the same name is being declared above in the view--}}
                                    @include('ch-eshop::products.variant', ['variant' => null])
                                </div>

                                @if(isset($product))
                                    @foreach($product->variants as $variant)
                                        <div class="tab-pane fade" id="v-pills-{{ $variant->id }}" role="tabpanel" aria-labelledby="v-pills-{{ $variant->id }}-tab">
                                            @include('ch-eshop::products.variant', ['variant' => $variant])
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </x-ch-eshop::nav-tab-body>

                <x-ch-eshop::nav-tab-body id="seo">
                    <!-- form input -->
                    <x-ch-eshop::form-input :title="__('ch-eshop::product/product.meta_title')" name="product[meta_title]" value="{{old('product.meta_title',$product->meta_title ?? null)}}" />

                    <!-- form input -->
                    <x-ch-eshop::textarea value="{{old('product.meta_description',$product->meta_description ?? null)}}" label="{{__('ch-eshop::product/product.meta_description')}}" colSize="" name="product[meta_description]" />
                </x-ch-eshop::nav-tab-body>
            </x-ch-eshop::nav-tab>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    @yield('component-scripts')

    <script src="{{asset('vendors/js/forms/toggle/switchery.min.js')}}"></script>
    <script>
        const currentUrl = '{{ request()->fullUrlWithQuery(['translation_locale' => 'sampleTranslationLocale']) }}';
        const elems1 = document.querySelectorAll('.switchery');

        for (i = 0; i < elems1.length; i++) {
            const switchery = new Switchery(elems1[i]);
        }

        @if(isset($product))
        $('#language').on('change', function (e) {
            const locale = this.value;

            window.location.href = currentUrl.replace('sampleTranslationLocale', locale);
        });
        @endif
    </script>
@endsection