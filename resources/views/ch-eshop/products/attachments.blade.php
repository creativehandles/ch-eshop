@php
    $inputNamePrefix = isset($variant) ? "variant_update[$variant->id]" : 'variant';
@endphp
<div class="form-group">
     <label for="images" class="form-label">{{__('ch-eshop::product/product.images')}}</label>
     <input accept="image/png, image/jpeg, image/jpg" class="form-control" type="file" value="" name="{{ $inputNamePrefix }}[attachments][]" id="images" multiple>
</div>

 @if(isset($variant))
     <ul class="list-group">
         @foreach($variant->attachments as $attachment)
             @if(! empty($attachment->value))
                <livewire:delete-product-attachment :attachment="$attachment" :parent="$variant" />
             @endif
         @endforeach
     </ul>
 @endif