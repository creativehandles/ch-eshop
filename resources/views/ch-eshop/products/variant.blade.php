@php
    $inputNamePrefix = isset($variant) ? "variant_update[$variant->id]" : 'variant';
    $oldValues = isset($variant) ? old("variant_update.$variant->id", []) : old('variant', []);
@endphp

<!-- form input -->
<div class="form-group">
    <input type="checkbox" class="switchery" name="{{ $inputNamePrefix }}[is_visible]" value="1"
            {{ \Illuminate\Support\Arr::get($oldValues, 'is_visible', (isset($variant) ? $variant->is_visible : null)) == true ? 'checked' : '' }}>
    <label class="ml-1">{{ __('ch-eshop::product/product.variant.visibility') }}</label>
</div>

<!-- form input -->
<div class="form-group">
    <label>{{__('ch-eshop::product/product.variant.type')}}</label>
    <input type="text" class="form-control" name="{{ $inputNamePrefix }}[type]"
           value="{{ \Illuminate\Support\Arr::get($oldValues, 'type', (isset($variant) ? $variant->type : null)) }}">
</div>

<!-- form input -->
<div class="form-group">
    <label>{{__('ch-eshop::product/product.variant.code')}}</label>
    <input type="text" class="form-control" name="{{ $inputNamePrefix }}[code]"
           value="{{ \Illuminate\Support\Arr::get($oldValues, 'code', (isset($variant) ? $variant->code : null)) }}">
</div>

<!-- form input -->
<div class="form-group">
    <label>{{__('ch-eshop::product/product.variant.ean')}}</label>
    <input type="text" class="form-control" name="{{ $inputNamePrefix }}[ean]"
           value="{{ \Illuminate\Support\Arr::get($oldValues, 'ean', (isset($variant) ? $variant->ean : null)) }}">
</div>

<!-- form input -->
<div class="form-group">
    <label>{{__('ch-eshop::product/product.variant.color')}}</label>
    <input type="text" class="form-control" name="{{ $inputNamePrefix }}[color]"
           value="{{ \Illuminate\Support\Arr::get($oldValues, 'color', (isset($variant) ? $variant->color : null)) }}">
</div>

<!-- form input -->
<div class="form-group">
    <label>{{__('ch-eshop::product/product.variant.unit_price_wo_tax')}}</label>
    <input type="text" class="form-control" name="{{ $inputNamePrefix }}[unit_price_wo_tax]"
           value="{{ \Illuminate\Support\Arr::get($oldValues, 'unit_price_wo_tax', (isset($variant) ? $variant->unit_price_wo_tax : null)) }}">
</div>

<!-- form input -->
<div class="form-group">
    <label>{{__('ch-eshop::product/product.variant.selling_price_wo_tax')}}</label>
    <input type="text" class="form-control" name="{{ $inputNamePrefix }}[selling_price_wo_tax]"
           value="{{ \Illuminate\Support\Arr::get($oldValues, 'selling_price_wo_tax', (isset($variant) ? $variant->selling_price_wo_tax : null)) }}">
</div>

<!-- form input -->
<div class="form-group">
    <label>{{__('ch-eshop::product/product.variant.availability')}}</label>
    <input type="text" class="form-control" name="{{ $inputNamePrefix }}[availability]"
           value="{{ \Illuminate\Support\Arr::get($oldValues, 'availability', (isset($variant) ? $variant->availability : null))  }}">
</div>

<!-- form input -->
<div class="form-group">
    <label>{{__('ch-eshop::product/product.variant.qty_available')}}</label>
    <input type="text" class="form-control" name="{{ $inputNamePrefix }}[qty_available]"
           value="{{ \Illuminate\Support\Arr::get($oldValues, 'qty_available', (isset($variant) ? $variant->qty_available : null)) }}">
</div>

<!-- form input -->
<div class="form-group">
    <label>{{__('ch-eshop::product/product.variant.delivery_in_days')}}</label>
    <input type="number" class="form-control" name="{{ $inputNamePrefix }}[delivery_in_days]"
           value="{{ \Illuminate\Support\Arr::get($oldValues, 'delivery_in_days', (isset($variant) ? $variant->delivery_in_days : null))  }}">
</div>

@include('ch-eshop::products.attachments')
