@extends('Admin.layout')

@section('content')

<div class="content-body">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    @include('Admin.partials.breadcumbs',['header'=>__('ch-eshop::breadcrumb.order.create.header')])
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="card col-md-12">

            <x-ch-eshop::nav-tab :headings="$headings" :formOverride="true">                
                <x-ch-eshop::nav-tab-body :active="'active'" id="order_details">
                <form action="{{$action??'#'}}" method="post" id="OrderMainForm" enctype="multipart/form-data">
                    @if(isset($modify) && $modify == true)
                        @method('PATCH')
                    @endif

                    @csrf

                    @if(isset($order))
                    <div class="form-row">
                        <x-ch-eshop::input disabled value="{{ $order->order_no }}" type="text" label="{{ __('ch-eshop::order/create.order_no') }}" colSize="col-md-6" />
                        <livewire:change-order-state :label="__('ch-eshop::order/create.order_status')" :default="__('ch-eshop::order/create.order_status_default')" :order="$order" />
                    </div>
                    @endif

                    <div class="form-row">
                        <x-ch-eshop::select name="language" colSize="col-md-6" :data="$countryData"
                                            title="{{ __('ch-eshop::order/create.choose') }}"
                                            selected="{{ old('language', $order->language ?? null) }}"
                                            label="{{ __('ch-eshop::order/create.country') }}"/>
                        <x-ch-eshop::input name="currency" type="text" colSize="col-md-6" readonly
                                           value="{{ $order->currency ?? null }}"
                                           label="{{ __('ch-eshop::order/create.currency') }}"/>
                    </div>

                    <div class="form-row">
                        <x-ch-eshop::select name="delivery_type" colSize="col-md-3" :data="$deliveryMethodsSelectData"
                                            title="{{ __('ch-eshop::order/create.choose') }}"
                                            selected="{{ old('delivery_type', $order->delivery_type ?? null) }}"
                                            label="{{ __('ch-eshop::order/create.type') }}"/>
                        <x-ch-eshop::input name="delivery_cost_wo_tax" type="text" colSize="col-md-3"
                                           value="{{old('delivery_cost_wo_tax', $order->delivery_cost_wo_tax ?? null )}}"
                                           label="{{ __('ch-eshop::order/create.delivery_cost_wo_tax') }}"/>
                        <x-ch-eshop::select name="payment_method" colSize="col-md-3" :data="$paymentMethodsSelectData"
                                            title="{{ __('ch-eshop::order/create.choose') }}"
                                            selected="{{ old('payment_method', $order->payment_method ?? null) }}"
                                            label="{{ __('ch-eshop::order/create.payment_method') }}"/>
                        <x-ch-eshop::input name="payment_price" type="text" colSize="col-md-3"
                                           value="{{old('payment_price', $order->payment_price ?? null)}}"
                                           label="{{ __('ch-eshop::order/create.payment_price') }}"/>
                    </div>

                    <div class="form-row">
                        <x-ch-eshop::textarea value="{{ old('client_note',$order->client_note ?? null) }}" label="{{ __('ch-eshop::order/create.client_note') }}" name="client_note" />
                    </div>

                    @if(isset($order))
                    <livewire:order-price :order="$order" />
                    @endif
                </x-ch-eshop::nav-tab-body>

                <x-ch-eshop::nav-tab-body id="customer_details" x-data="{ showFields: {{ old('is_company', isset($order) && $order->is_company ? 'true' : 'false') }} }">
                    <div class="form-group form-check">
                        <input @click="showFields = !showFields" name="is_company" class="form-check-input" value="1"
                               type="checkbox" id="chkbox" {{ old('is_company', isset($order) && $order->is_company) ? 'checked' : '' }}>
                        <label class="form-check-label" for="chkbox">
                            {{ __('ch-eshop::order/create.company') }}
                        </label>
                    </div>

                    <div x-show="showFields" class="form-row">
                        <x-ch-eshop::input value="{{ old('company',$order->company ?? null) }}" label="{{ __('ch-eshop::order/create.company_name') }}" colSize="col-md-4" name="company" />
                        <x-ch-eshop::input value="{{ old('cin',$order->cin ?? null) }}" label="{{ __('ch-eshop::order/create.cin') }}" colSize="col-md-4" name="cin" />
                        <x-ch-eshop::input value="{{ old('vat_no',$order->vat_no ?? null) }}" label="{{ __('ch-eshop::order/create.vat_no') }}" colSize="col-md-4" name="vat_no" />
                    </div>

                    <h5 class="card-title">{{__('ch-eshop::order/create.invoice_details')}}</h5>
                    <hr>

                    <div class="form-row">
                        <x-ch-eshop::input value="{{ old('billing_address_name',$order->billing_address_name ?? null) }}" label="{{ __('ch-eshop::order/create.billing_address_name') }}" colSize="col-md-4" name="billing_address_name" />
                        <x-ch-eshop::input value="{{ old('billing_address_surname',$order->billing_address_surname ?? null) }}" label="{{ __('ch-eshop::order/create.billing_address_surname') }}" colSize="col-md-4" name="billing_address_surname" />
                        <x-ch-eshop::input value="{{ old('billing_address_street',$order->billing_address_street ?? null) }}" label="{{ __('ch-eshop::order/create.billing_address_street') }}" colSize="col-md-4" name="billing_address_street" />
                        <x-ch-eshop::input value="{{ old('billing_address_zip',$order->billing_address_zip ?? null) }}" label="{{ __('ch-eshop::order/create.billing_address_zip') }}" colSize="col-md-4" name="billing_address_zip" />
                        <x-ch-eshop::input value="{{ old('billing_address_city',$order->billing_address_city ?? null) }}" label="{{ __('ch-eshop::order/create.billing_address_city') }}" colSize="col-md-4" name="billing_address_city" />
                        <x-ch-eshop::input value="{{ old('billing_address_country',$order->billing_address_country ?? null) }}" label="{{ __('ch-eshop::order/create.billing_address_country') }}" colSize="col-md-4" name="billing_address_country" />
                        <x-ch-eshop::input value="{{ old('billing_address_phone',$order->billing_address_phone ?? null) }}" label="{{ __('ch-eshop::order/create.billing_address_phone') }}" colSize="col-md-4" name="billing_address_phone" />
                        <x-ch-eshop::input value="{{ old('email',$order->email ?? null) }}" type="email" label="{{ __('ch-eshop::order/create.email') }}" colSize="col-md-4" name="email" />
                    </div>

                    <h5 class="card-title">{{__('ch-eshop::order/create.delivery_details')}}</h5>
                    <hr>
                    <div class="form-row">
                        <x-ch-eshop::input value="{{ old('delivery_address_name',$order->delivery_address_name ?? null) }}" label="{{ __('ch-eshop::order/create.delivery_address_name') }}" colSize="col-md-4" name="delivery_address_name" />
                        <x-ch-eshop::input value="{{ old('delivery_address_surname',$order->delivery_address_surname ?? null) }}" label="{{ __('ch-eshop::order/create.delivery_address_surname') }}" colSize="col-md-4" name="delivery_address_surname" />
                        <x-ch-eshop::input value="{{ old('delivery_address_street',$order->delivery_address_street ?? null) }}" label="{{ __('ch-eshop::order/create.delivery_address_street') }}" colSize="col-md-4" name="delivery_address_street" />
                        <x-ch-eshop::input value="{{ old('delivery_address_zip',$order->delivery_address_zip ?? null) }}" label="{{ __('ch-eshop::order/create.delivery_address_zip') }}" colSize="col-md-4" name="delivery_address_zip" />
                        <x-ch-eshop::input value="{{ old('delivery_address_city',$order->delivery_address_city ?? null) }}" label="{{ __('ch-eshop::order/create.delivery_address_city') }}" colSize="col-md-4" name="delivery_address_city" />
                        <x-ch-eshop::input value="{{ old('delivery_address_country',$order->delivery_address_country ?? null) }}" label="{{ __('ch-eshop::order/create.delivery_address_country') }}" colSize="col-md-4" name="delivery_address_country" />
                        <x-ch-eshop::input value="{{ old('delivery_address_phone',$order->delivery_address_phone ?? null) }}" label="{{ __('ch-eshop::order/create.delivery_address_phone') }}" colSize="col-md-4" name="delivery_address_phone" />
                    </div>
                </x-ch-eshop::nav-tab-body>


                <x-ch-eshop::nav-tab-body id="notes">

                    <div class="form-row">
                        <x-ch-eshop::textarea value="{{ old('admin_note',$order->admin_note ?? null) }}" label="{{ __('ch-eshop::order/create.admin_note') }}" name="admin_note" />
                    </div>
                </form>
                </x-ch-eshop::nav-tab-body>
                
                
                <x-ch-eshop::nav-tab-body id="orderItems">
                    @if(isset($order))
                        <livewire:ch-eshop-order-item-table :order="$order" />
                        @livewire('add-order-item', [
                        'orderItems'=>$orderItems,
                        'order'=>$order
                        ])
                    @endif
                </x-ch-eshop::nav-tab-body>
            </x-ch-eshop::nav-tab>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    @yield('component-scripts')

    <script>
        const localeData = {!! json_encode($localeData) !!};
        const deliveryMethods = {!! json_encode($deliveryMethods) !!};
        const paymentMethods = {!! json_encode($paymentMethods) !!};
        const is_company = {!! json_encode((boolean) isset($order) && $order->is_company ? $order->is_company: false) !!};

        function setDeliveryPrice(locale, deliveryMethod) {
            const deliveryPrice = deliveryMethods[deliveryMethod]?.prices[locale]?.price_wo_tax ?? null;

            $('[name="delivery_cost_wo_tax"]').val(deliveryPrice);
        }

        function setPaymentPrice(locale, paymentMethod) {
            const paymentPrice = paymentMethods[paymentMethod]?.prices[locale]?.price_wo_tax ?? null;

            $('[name="payment_price"]').val(paymentPrice);
        }

        window.livewire.onError(statusCode => {
            if (statusCode > 400) {
                toastr.error("{{ session()->get('error') }}", "{{__('alerts.Error')}}");
            }
        });

        $(document).on('click', '#MainFormSubmit', function (e) {
            e.preventDefault();

            $('#OrderMainForm').submit();
        });

        $('[name="language"]').on('change', function(e) {
            const locale = $(this).val();
            const currencyCode = localeData[locale]?.currency?.code;

            $('[name="currency"]').val(currencyCode);

            const deliveryMethod = $('[name="delivery_type"]').val();
            const paymentMethod = $('[name="payment_method"]').val();

            setDeliveryPrice(locale, deliveryMethod);
            setPaymentPrice(locale, paymentMethod);
        });

        $('[name="delivery_type"]').on('change', function(e) {
            const locale = $('[name="language"]').val();
            const deliveryMethod = $(this).val();

            setDeliveryPrice(locale, deliveryMethod);
        });

        $('[name="payment_method"]').on('change', function(e) {
            const locale = $('[name="language"]').val();
            const paymentMethod = $(this).val();

            setPaymentPrice(locale, paymentMethod);
        });
    </script>
@endsection