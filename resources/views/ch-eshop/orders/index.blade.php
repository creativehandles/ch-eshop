@extends('Admin.layout')

@section('content')
<div class="content-body">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    @include('Admin.partials.breadcumbs',['header'=>__('ch-eshop::breadcrumb.order.index.header')])
                </div>
            </div>
        </div>

        <div class="content-header-right col-md-6 col-12 mb-2">
            <div class="mb-1 pull-right">
                <a href="{{route('admin.ch-eshop.order.create')}}" class="btn btn-secondary btn-block-sm"><i class="ft-file-plus"></i>{{__('ch-eshop::breadcrumb.order.create.link_name')}}</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <livewire:ch-eshop-order-table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
@yield('component-scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.26.1/axios.min.js" integrity="sha512-bPh3uwgU5qEMipS/VOmRqynnMXGGSRv+72H/N260MQeXZIK4PG48401Bsby9Nq5P5fz7hy5UGNmC/W1Z51h2GQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>
    $(document).on('change', '.statusSelect', function(e) {
        e.preventDefault();

        axios.post($(this).data('link') + '/change-state/' + this.value, {
            'ch_eshop_status_id': this.value
        }).then(function(res) {
            location.reload();
        }).catch(function(err) {
            console.log(err);
        });
    });
    
    hideActionButtons($('.form-check-input:checkbox:checked').length);

    $(document).on('change', '.form-check-input', function(e) {
        e.preventDefault();
        hideActionButtons($('.form-check-input:checkbox:checked').length);
    });

    Livewire.on('bulk-messages', messages => {
        hideActionButtons($('.form-check-input:checkbox:checked').length);

        messages.forEach(message => {
            if (message.type == 'success') {
                success(message.body);
            } else if (message.type == 'error') {
                error(message.body);
            }
        });
    });

    function success(message) {
        toastr.success(`${message}`, "{{__('alerts.Success')}}", {
            timeOut: 5000,
            title: "ABC",
            fadeOut: 1000,
            progressBar: true,
        });
    }

    function error(message) {
        toastr.error(`${message}`, "{{__('alerts.Error')}}")
    }

    function hideActionButtons(count) {
        if (count > 0) {
            $('.action').removeClass('d-none');
        } else {
            $('.action').addClass('d-none');
        }
    }
</script>
@endsection