@extends('ch-eshop::emails.base-template')

@section('content')
    <table class="row"
        style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
        <tbody>
            <tr style="text-align: left; vertical-align: top; padding: 0;">
                <th class="small-12 large-12 columns first last"
                    style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0 auto; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; hyphens: auto; line-height: 25px; text-align: left; vertical-align: top; width: 560px; word-wrap: break-word; padding: 0 20px 10px 20px;">
                    <table
                        style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
                        <tbody>
                            <tr style="text-align: left; vertical-align: top; padding: 0;">
                                <th
                                    style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; hyphens: auto; line-height: 25px; text-align: left; vertical-align: top; word-wrap: break-word; padding: 0;">
                                    <table class="spacer"
                                        style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
                                        <tbody>
                                            <tr style="text-align: left; vertical-align: top; padding: 0;">
                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 20px; font-weight: 300; hyphens: auto; line-height: 20px; mso-line-height-rule: exactly; text-align: left; vertical-align: top; word-wrap: break-word; padding: 0;"
                                                    height="20">&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <h1
                                        style="margin: 0; margin-bottom: 10px; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 25px; font-weight: 600; line-height: 25px; text-align: left; word-wrap: normal; padding: 0;">
                                        @lang('ch-eshop::emails.failed-order-title')</h1>
                                    <table class="spacer"
                                        style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
                                        <tbody>
                                            <tr style="text-align: left; vertical-align: top; padding: 0;">
                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 20px; font-weight: 300; hyphens: auto; line-height: 20px; mso-line-height-rule: exactly; text-align: left; vertical-align: top; word-wrap: break-word; padding: 0;"
                                                    height="20">&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <p
                                        style="margin: 0;color: #18414e;font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;font-size: 14px;font-weight: 300;line-height: 25px;text-align: left;padding: 0;">
                                        @lang('ch-eshop::emails.dear-customer'),</p>
                                    <p
                                        style="margin: 0; margin-bottom: 10px; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; line-height: 25px; text-align: left; padding: 0;">
                                        @lang('ch-eshop::emails.failed-order-text', ['order_no' => $order->order_no, 'order_date' => $order->created_at->toDateString()])</p>
                                    <table class="spacer"
                                        style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
                                        <tbody>
                                            <tr style="text-align: left; vertical-align: top; padding: 0;">
                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 20px; font-weight: 300; hyphens: auto; line-height: 20px; mso-line-height-rule: exactly; text-align: left; vertical-align: top; word-wrap: break-word; padding: 0;"
                                                    height="20">&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>





                                    <h1
                                        style="margin: 0; margin-bottom: 10px; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 25px; font-weight: 600; line-height: 25px; text-align: left; word-wrap: normal; padding: 0;">
                                        @lang('ch-eshop::emails.order-items')</h1>
                                </th>
                                <th class="expander"
                                    style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; hyphens: auto; line-height: 25px; padding: 0; text-align: left; vertical-align: top; visibility: hidden; width: 0; word-wrap: break-word;">
                                    &nbsp;</th>
                            </tr>
                        </tbody>
                    </table>
                </th>
            </tr>
        </tbody>
    </table>
    <table class="row c-table__pricing--row"
        style="border-bottom: 1px solid #EEE; border-collapse: collapse; border-spacing: 0; display: table; margin-top: 10px!important; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
        <tbody>
            @foreach ($order->items as $item)
                <tr style="text-align: left; vertical-align: top; padding: 0;">

                    <th class="small-2 large-2 columns"
                        style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0 auto; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; hyphens: auto; line-height: 25px; text-align: left; vertical-align: top; width: 76.67px; word-wrap: break-word; padding: 0 10px 10px 10px;">
                        <table
                            style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
                            <tbody>
                                <tr style="text-align: left; vertical-align: top; padding: 0;">
                                    <th
                                        style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; hyphens: auto; line-height: 25px; text-align: left; vertical-align: top; word-wrap: break-word; padding: 0;">
                                        {{-- <center
                                                                            style="min-width: 36.67px; width: 100%;">
                                                                            <img class="float-center"
                                                                                style="-ms-interpolation-mode: bicubic; margin: 0 auto; border: none; clear: both; display: block; float: none; max-width: 100%; outline: 0; text-align: center; text-decoration: none; width: auto;"
                                                                                src="#IMAGE_PATH" alt="#PRODUCT_NAME"
                                                                                align="center"></center> --}}
                                    </th>
                                </tr>
                            </tbody>
                        </table>
                    </th>
                    <th class="u-align__v--middle small-7 large-7 columns"
                        style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0 auto; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; hyphens: auto; line-height: 25px; text-align: left; vertical-align: middle; width: 318.33px; word-wrap: break-word; padding: 0 10px 10px 10px;">
                        <table
                            style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
                            <tbody>
                                <tr style="text-align: left; vertical-align: top; padding: 0;">
                                    <th
                                        style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; hyphens: auto; line-height: 25px; text-align: left; vertical-align: top; word-wrap: break-word; padding: 0;">
                                        <span
                                            style="color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-weight: 300; line-height: 25px; padding: 0; text-align: left; text-decoration: underline;">{{ json_decode($item->object)->title->{$order->language} ?? '' }}
                                            x {{ $item->quantity }}</span>
                                    </th>
                                </tr>
                            </tbody>
                        </table>
                    </th>
                    <th class="u-align__v--middle small-3 large-3 columns last"
                        style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0 auto; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; hyphens: auto; line-height: 25px; text-align: left; vertical-align: middle; width: 125px; word-wrap: break-word; padding: 0 20px 10px 10px;">
                        <table
                            style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
                            <tbody>
                                <tr style="text-align: left; vertical-align: top; padding: 0;">
                                    <th
                                        style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; hyphens: auto; line-height: 25px; text-align: left; vertical-align: top; word-wrap: break-word; padding: 0;">
                                        <p class="text-right"
                                            style="margin: 0; margin-bottom: 0; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; line-height: 25px; text-align: right; padding: 0;">
                                            <strong>{!! $localeService->formatPriceWithCurrencySymbol($order->language, $item->total_price_w_tax) !!}</strong>
                                        </p>
                                    </th>
                                </tr>
                            </tbody>
                        </table>
                    </th>
                </tr>
            @endforeach
        </tbody>
    </table>
    <table class="row c-table__pricing--row"
        style="border-bottom: 1px solid #EEE; border-collapse: collapse; border-spacing: 0; display: table; margin-top: 10px!important; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
        <tbody>
            <tr style="text-align: left; vertical-align: top; padding: 0;">

                <th class="small-2 large-2 columns"
                    style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0 auto; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; hyphens: auto; line-height: 25px; text-align: left; vertical-align: top; width: 76.67px; word-wrap: break-word; padding: 0 10px 10px 10px;">
                    <table
                        style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
                        <tbody>
                            <tr style="text-align: left; vertical-align: top; padding: 0;">
                                <th
                                    style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; hyphens: auto; line-height: 25px; text-align: left; vertical-align: top; word-wrap: break-word; padding: 0;">
                                    <center style="min-width: 36.67px; width: 100%;">
                                    </center>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </th>
                <th class="u-align__v--middle small-7 large-7 columns"
                    style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0 auto; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; hyphens: auto; line-height: 25px; text-align: left; vertical-align: middle; width: 318.33px; word-wrap: break-word; padding: 0 10px 10px 10px;">
                    <table
                        style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
                        <tbody>
                            <tr style="text-align: left; vertical-align: top; padding: 0;">
                                <th
                                    style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; hyphens: auto; line-height: 25px; text-align: left; vertical-align: top; word-wrap: break-word; padding: 0;">
                                    <p
                                        style="margin: 0; margin-bottom: 0; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; line-height: 25px; text-align: left; padding: 0;">
                                        {{ __('ch-eshop::config.deliveryMethods.'.$order->delivery_type) }}</p>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </th>
                <th class="u-align__v--middle small-3 large-3 columns last"
                    style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0 auto; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; hyphens: auto; line-height: 25px; text-align: left; vertical-align: middle; width: 125px; word-wrap: break-word; padding: 0 20px 10px 10px;">
                    <table
                        style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
                        <tbody>
                            <tr style="text-align: left; vertical-align: top; padding: 0;">
                                <th
                                    style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; hyphens: auto; line-height: 25px; text-align: left; vertical-align: top; word-wrap: break-word; padding: 0;">
                                    <p class="text-right"
                                        style="margin: 0; margin-bottom: 0; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; line-height: 25px; text-align: right; padding: 0;">
                                        <strong>{!! $localeService->formatPriceWithCurrencySymbol($order->language, $order->delivery_cost_w_tax) !!}</strong>
                                    </p>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </th>
            </tr>
        </tbody>
    </table>
    <table class="row c-table__pricing--row"
        style="border-bottom: 1px solid #EEE; border-collapse: collapse; border-spacing: 0; display: table; margin-top: 10px!important; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
        <tbody>
            <tr style="text-align: left; vertical-align: top; padding: 0;">

                <th class="small-2 large-2 columns"
                    style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0 auto; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; hyphens: auto; line-height: 25px; text-align: left; vertical-align: top; width: 76.67px; word-wrap: break-word; padding: 0 10px 10px 10px;">
                    <table
                        style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
                        <tbody>
                            <tr style="text-align: left; vertical-align: top; padding: 0;">
                                <th
                                    style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; hyphens: auto; line-height: 25px; text-align: left; vertical-align: top; word-wrap: break-word; padding: 0;">
                                    <center style="min-width: 36.67px; width: 100%;">
                                    </center>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </th>
                <th class="u-align__v--middle small-7 large-7 columns"
                    style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0 auto; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; hyphens: auto; line-height: 25px; text-align: left; vertical-align: middle; width: 318.33px; word-wrap: break-word; padding: 0 10px 10px 10px;">
                    <table
                        style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
                        <tbody>
                            <tr style="text-align: left; vertical-align: top; padding: 0;">
                                <th
                                    style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; hyphens: auto; line-height: 25px; text-align: left; vertical-align: top; word-wrap: break-word; padding: 0;">
                                    <p
                                        style="margin: 0; margin-bottom: 0; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; line-height: 25px; text-align: left; padding: 0;">
                                        {{ __('ch-eshop::config.paymentMethods.'.$order->payment_method) }}</p>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </th>
                <th class="u-align__v--middle small-3 large-3 columns last"
                    style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0 auto; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; hyphens: auto; line-height: 25px; text-align: left; vertical-align: middle; width: 125px; word-wrap: break-word; padding: 0 20px 10px 10px;">
                    <table
                        style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
                        <tbody>
                            <tr style="text-align: left; vertical-align: top; padding: 0;">
                                <th
                                    style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; hyphens: auto; line-height: 25px; text-align: left; vertical-align: top; word-wrap: break-word; padding: 0;">
                                    <p class="text-right"
                                        style="margin: 0; margin-bottom: 0; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; line-height: 25px; text-align: right; padding: 0;">
                                        <strong>{!! $localeService->formatPriceWithCurrencySymbol($order->language, $order->payment_price_w_tax) !!}</strong>
                                    </p>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </th>
            </tr>
        </tbody>
    </table>
    <table class="spacer"
        style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
        <tbody>
            <tr style="text-align: left; vertical-align: top; padding: 0;">
                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 25px; font-weight: 300; hyphens: auto; line-height: 25px; mso-line-height-rule: exactly; text-align: left; vertical-align: top; word-wrap: break-word; padding: 0;"
                    height="25">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <table class="row"
        style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
        <tbody>
            <tr style="text-align: left; vertical-align: top; padding: 0;">
                <th class="small-12 large-12 columns first last"
                    style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0 auto; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; hyphens: auto; line-height: 25px; text-align: left; vertical-align: top; width: 560px; word-wrap: break-word; padding: 0 20px 10px 20px;">
                    <table
                        style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
                        <tbody>
                            <tr style="text-align: left; vertical-align: top; padding: 0;">
                                <th
                                    style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; hyphens: auto; line-height: 25px; text-align: left; vertical-align: top; word-wrap: break-word; padding: 0;">
                                    <p class="text-right u-font--18"
                                        style="margin: 0; margin-bottom: 10px; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 18px; font-weight: 300; line-height: 25px; text-align: right; padding: 0;">
                                        <strong>{!! $localeService->formatPriceWithCurrencySymbol($order->language, $order->total_cost_w_tax) !!}</strong>
                                    </p>
                                </th>
                                <th class="expander"
                                    style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; hyphens: auto; line-height: 25px; padding: 0; text-align: left; vertical-align: top; visibility: hidden; width: 0; word-wrap: break-word;">
                                    &nbsp;</th>
                            </tr>
                        </tbody>
                    </table>
                </th>
            </tr>
        </tbody>
    </table>
    <table class="row"
        style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
        <tbody>
            <tr style="text-align: left; vertical-align: top; padding: 0;">
                <th class="small-12 large-12 columns first last"
                    style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0 auto; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; hyphens: auto; line-height: 25px; text-align: left; vertical-align: top; width: 560px; word-wrap: break-word; padding: 0 20px 10px 20px;">
                    <table
                        style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
                        <tbody>
                            <tr style="text-align: left; vertical-align: top; padding: 0;">
                                <th
                                    style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; hyphens: auto; line-height: 25px; text-align: left; vertical-align: top; word-wrap: break-word; padding: 0;">
                                    <table class="spacer"
                                        style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
                                        <tbody>
                                            <tr style="text-align: left; vertical-align: top; padding: 0;">
                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 20px; font-weight: 300; hyphens: auto; line-height: 20px; mso-line-height-rule: exactly; text-align: left; vertical-align: top; word-wrap: break-word; padding: 0;"
                                                    height="20">&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <p
                                        style="margin: 0;margin-bottom: 10px;color: #18414e;font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;font-size: 14px;font-weight: 300;line-height: 25px;text-align: left;padding: 0;">
                                        @lang('ch-eshop::emails.failed-order-payment')</p>
                                    <p
                                        style="margin: 0;color: #18414e;font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;font-size: 14px;font-weight: 300;line-height: 25px;text-align: left;padding: 0;">
                                        IBAN: CZ60 2010 0000 0022 0187 3595</p>






                                    <p
                                        style="margin: 0; margin-bottom: 10px; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; line-height: 25px; text-align: left; padding: 0;">
                                        BIC/SWIFT: FIOBCZPPXXX</p>
                                    <p
                                        style="margin: 0;margin-bottom: 10px;color: #18414e;font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;font-size: 14px;font-weight: 300;line-height: 25px;text-align: left;padding: 0;">
                                        @lang('ch-eshop::emails.failed-order-payment-data')</p>
                                    <p
                                        style="margin: 0;color: #18414e;font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;font-size: 14px;font-weight: 300;line-height: 25px;text-align: left;padding: 0;">
                                        @lang('ch-eshop::emails.failed-order-recipient')</p>
                                    <p
                                        style="margin: 0;color: #18414e;font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;font-size: 14px;font-weight: 300;line-height: 25px;text-align: left;padding: 0;">
                                        Loonoy s.r.o.</p>
                                    <p
                                        style="margin: 0;color: #18414e;font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;font-size: 14px;font-weight: 300;line-height: 25px;text-align: left;padding: 0;">
                                        Jiřího z Poděbrad 860</p>
                                    <p
                                        style="margin: 0;color: #18414e;font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;font-size: 14px;font-weight: 300;line-height: 25px;text-align: left;padding: 0;">
                                        664 62 Hrušovany u Brna</p>
                                </th>
                                <th class="expander"
                                    style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; hyphens: auto; line-height: 25px; padding: 0; text-align: left; vertical-align: top; visibility: hidden; width: 0; word-wrap: break-word;">
                                    &nbsp;</th>
                            </tr>
                        </tbody>
                    </table>
                </th>
            </tr>
        </tbody>
    </table>
@endsection
