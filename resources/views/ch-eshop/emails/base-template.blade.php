<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en" style="background:#f1f4f8!important">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <title>Loonoy.com</title>

    <style>
        @media only screen {
            html {
                min-height: 100%;
                background: 0 0
            }
        }

        @media only screen and (max-width:600px) {
            table.body img {
                width: auto;
                height: auto
            }

            table.body center {
                min-width: 0 !important
            }

            table.body .container {
                width: 95% !important
            }

            table.body .columns {
                height: auto !important;
                -moz-box-sizing: border-box;
                -webkit-box-sizing: border-box;
                box-sizing: border-box;
                padding-left: 20px !important;
                padding-right: 20px !important
            }

            table.body .columns .columns {
                padding-left: 0 !important;
                padding-right: 0 !important
            }

            th.small-1 {
                display: inline-block !important;
                width: 8.33333% !important
            }

            th.small-2 {
                display: inline-block !important;
                width: 16.66667% !important
            }

            th.small-3 {
                display: inline-block !important;
                width: 25% !important
            }

            th.small-6 {
                display: inline-block !important;
                width: 50% !important
            }

            th.small-7 {
                display: inline-block !important;
                width: 58.33333% !important
            }

            th.small-12 {
                display: inline-block !important;
                width: 100% !important
            }

            .columns th.small-12 {
                display: block !important;
                width: 100% !important
            }

            table.menu {
                width: 100% !important
            }

            table.menu td,
            table.menu th {
                width: auto !important;
                display: inline-block !important
            }

            table.menu.vertical td,
            table.menu.vertical th {
                display: block !important
            }
        }
    </style>
</head>

<body
    style="-moz-box-sizing:border-box;-ms-text-size-adjust:100%;-webkit-box-sizing:border-box;-webkit-text-size-adjust:100%;Margin:0;background:#f1f4f8!important;box-sizing:border-box;color:#18414e;font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;font-size:14px;font-weight:300;line-height:25px;margin:0;min-width:100%;padding:0;padding-bottom:0;padding-left:0;padding-right:0;padding-top:0;text-align:left;width:100%!important">
    <table class="spacer"
        style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
        <tbody>
            <tr style="text-align: left; vertical-align: top; padding: 0;">
                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 25px; font-weight: 300; hyphens: auto; line-height: 25px; mso-line-height-rule: exactly; text-align: left; vertical-align: top; word-wrap: break-word; padding: 0;"
                    height="25">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <p><span class="preheader"
            style="color: transparent; display: none!important; font-size: 1px; line-height: 1px; max-height: 0; max-width: 0; mso-hide: all!important; opacity: 0; overflow: hidden; visibility: hidden;">Děkujeme
            za Vaši objednávku a projevenou důvěru!</span></p>
    <table class="body"
        style="margin: 0; background: #f1f4f8!important; border-collapse: collapse; border-spacing: 0; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; height: 100%; line-height: 25px; text-align: left; vertical-align: top; width: 100%; padding: 0;">
        <tbody>
            <tr style="text-align: left; vertical-align: top; padding: 0;">
                <td class="center"
                    style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; hyphens: auto; line-height: 25px; text-align: left; vertical-align: top; word-wrap: break-word; padding: 0;"
                    align="center" valign="top">
                    <center style="min-width: 580px; width: 100%;">
                        <table class="container float-center"
                            style="margin: 0 auto; background: #fefefe; background-color: #fff; border-collapse: collapse; border-spacing: 0; float: none; text-align: center; vertical-align: top; width: 580px; padding: 0;"
                            align="center">
                            <tbody>
                                <tr style="text-align: left; vertical-align: top; padding: 0;">
                                    <td
                                        style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; hyphens: auto; line-height: 25px; text-align: left; vertical-align: top; word-wrap: break-word; padding: 0;">
                                        <!-- Include header -->

                                        <table class="row c-header"
                                            style="border-bottom: 1px solid #000; border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
                                            <tbody>
                                                <tr style="text-align: left; vertical-align: top; padding: 0;">
                                                    <th class="small-12 large-6 columns first"
                                                        style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0 auto; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; hyphens: auto; line-height: 25px; text-align: left; vertical-align: top; width: 270px; word-wrap: break-word; padding: 0 10px 10px 20px;">
                                                        <table
                                                            style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
                                                            <tbody>
                                                                <tr
                                                                    style="text-align: left; vertical-align: top; padding: 0;">
                                                                    <th
                                                                        style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; hyphens: auto; line-height: 25px; text-align: left; vertical-align: top; word-wrap: break-word; padding: 0;">
                                                                        <table class="spacer"
                                                                            style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
                                                                            <tbody>
                                                                                <tr
                                                                                    style="text-align: left; vertical-align: top; padding: 0;">
                                                                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 10px; font-weight: 300; hyphens: auto; line-height: 10px; mso-line-height-rule: exactly; text-align: left; vertical-align: top; word-wrap: break-word; padding: 0;"
                                                                                        height="10">&nbsp;</td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                        <a style="color: #000; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 15px; font-weight: bold; line-height: 25px; padding: 0; text-align: left; text-decoration: underline;"
                                                                            href="https://loonoy.com" target="_blank"
                                                                            rel="noopener"> <img
                                                                                style="-ms-interpolation-mode: bicubic; border: none; clear: both; display: block; max-width: 100%; outline: 0; text-decoration: none; width: auto;"
                                                                                src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAK8AAAA6CAYAAADGOQmFAAAAAXNSR0IArs4c6QAAHUNJREFUeF7tXXl81NW1/547EwKCCEjValvXtlZqRRKWTCYhYQ9LkCUCggouWLTKIphkkpn8MllB2awrLiDIZgQEBCQQErJMZAnQ2tb2PQuoxYfigoBCYOae97m/mQlJZpJMyFL63tw/SMjvLuee+53zO/dsQz2m5C4QoOsZsj30Jtw/IEEQPzBwuGJposXzx+CPIAcuGw5Q2JRcJwADQADYTZjnh/474buKpUldLhuKg4QEOeDhAIVNmfstwJ2ExJ2uEJLsMhA5Xcwh3EYwDoH504plyTcFORbkwOXGAQXeHwC0q1ia6NUXqmgMm5KrZPDxiqVJP73cCA/SE+SAAu8pgDtULE1UqkOVwnBHgtamXYd2lQAfq1ia9LMgq4IcuNw40AB421YCCIL3cju1ID3u61jYlNxTAOqQvEHwBnFy+XIgqDZcvmcTpKwBDngkL3WoWPqMH503KHmDCLp8OdCA2tCuEszHKpYFL2yX7xH+/6WsYfAGrQ3/f9FRx84jBmtdftbxju/z8hJkazAnISFP5OXd66q9VgNqQ9NNZT3uzx1MBurFzHcRRDeGvI6AUAAnwfgMwAEmPiTJUHho2TP/3RhmKHNeW2PbwTBwGIC7AdwB4BoCjMz4DoQjzDhALA8aJBfsW5P6eWPmv9S+PRJyhgiSfcDck0HdAHQFQx30cSL8mRgVkFy2b31q8aWu0ZhxvYZm/4oMzhgi6k4MxatfAOgERiUJ/pIk/goSh6R07Snfkra9obmjh9rfYmAQgPOevRmVf9bbLhpcfT221Z0JF/sRqJZ31z0fQz0BWABcyUw7iPjlHTvS86tZG+rQedu3rQQ13lQW9uC8W8FyOggPAtwR7N2a14xMYFaEuVs1b3QpGK8ceDt5ZX0MDJuYe6dkfooIDxIQoo+/+M9FR3d1N7e7yw6AXj24OnldQwfU2OdhE7SudD5kBgR+D8bVVeM9NFTt1UOn5///VPtt/51zUVGRptz0zdpM8fZJzPR7AJEXgeKx5hOcYBi93PcGBxDjNMDLjS5eVLRd+8QfQdFxGYOI2QNyPk1AB+9B1gBxrcOtesbuYIQqsOv9FGOqgVj97+L5KQRV4Z4YT27bpb1Qv9qgwKvsvAHqvLc9+XzoVd//sADA49XwtJaAdcTiz07j2S8PLUv7XpEVM1lre9oZ0pnZeCMJGQvGZAZ+5dnQYWJMq1iZrH/CvK3XA9lXX3DRfGIdtGA3YpcSYxMb6C8hXPnVnpWaMv0hbIR2hbOtsbMIMd4qpGsAQFMAeJ0tfyKJJ/a/YylrDrSEJWRPI8kv6dJAn5DyGHItseHgD4bKr/6Wp53RaUrIvYpw4TrhpD4A7oNbeqmNfAPGo3s3pm5oDnpMwzMGAfQywLfo9DD+ysTLQSh0keEz57Gu31ZUPHZBPYqMn3ulrDx7rYHoToOg0cyYVA1VL7XnTrO2bXtK4aBGix2cXgLAzMyjivK195qD7vrmGDFCu8J1BsOJsFYB3cD0Swqb7LHzLvPjYbvCA963Gr6whU/KiXER1gjgWgZOEdh63kUrPlqV/F2gGwu7P7s3S0oEMErHpcCSA28nP6bGd78vd6SAXAtGKBhfEFHaWWflGi8wAlmjZ0J2XyYkMXiIG2OcW7E2NTmQsf763Bb3fGiXK06tZcZInV7iHBhcz+/L044HMmdEfM5tkuRsgPU9EvDKhxtTpwUytq4+pmH2VwmYqqQWgdYy8bOlW2wVgc7ZO07rGMqGCYJlFkF/g3zFxOOKPtCKqs8xYID9bhZ8AIyPC3akKXWtVdrw2LSnCLRYEJZR2IMe9/BbfsDbToGXj1UsT67XPdxjUvYUIvGmUgXA/GLHkPOzi5Zp5y51N3dPzOpPkt4GoPTj98AoA+FZBRBJyD242nLJgFM09RibPVoQv6WcMwA2789LiW8srb8b9Gz70A6VewjopvRqyXxvxXvWfzZ2HtU/YmSmCZKVKnMdwNvKN9uGNnaeiIQF7YxnzhSAEAHG50RiUvHWS9epExLeMXz3/d+eY6YZIKXm4dFdO2yvV6er/0Btv2CEkaT4/F1pmxtL86X0H9U/+Wp2tfkajP+isAdyTgHUoWK5L3jbtg3V1YYDK+oGb9iknEcAvKa/wInHHFhhWX8pRNUeo9SKM+farGRgtOfZEXK54vbnWf/RHPMrtQJtQl4kYDIY5fvXp5gCnTds6qshhhMnDoLRDcTL926wPhjo2Lr6KXpC2aAA0A9MOxxbUt0qRQDNA9yDBPyaGGuuufL2Sf5u5wFM5dOl/yBtuJC02aOATtuxQ3vF22lwrL0bDPIv6uK9vUC78VLmb+yY0X1tsQDtIomdFHZ/zikGOhxYkeTrpGjrkbx1gFepCpJRqAgQYPP+lc2jQ1bfUPj4nBUs5SSAPkKnq8Mqlrh1teZo4WOy5pDkeZ4b3oZ9G1K9H5R6p+99T2Y+GAMJvObDjdYJzUGLdw7z0PRyQNeJny/dapseyNx9h6SXE6MPwMuKtmtKt2/WNrif1gtEe5RFgIG47QXaB94F4vpppQREguXYrYX2Zr8I197I2Ki0EiKYWdII6nF/7ikwdzjwti9424aGVoJx7MBKX8mrLk9OJ5RuZyRwv4pVKTqIW6KFJ2S9CmCq5z76yP48yxtNWScyXrveaTC+rg4CwF8BnIY6fKakvRtT5tY3d++RGU8Q4wUCDpVvsirzXLM2Zf7rekb8D4AuDMSUbLPtrm+B2CHpC8CYKSS2FOxIG96sxFSbbGisvTdIfqj+ZJD4yeYi7Wv1e3x/7XdS4k8EHNtcqLVo9OG4SK0PhCwHcHRtif1m6jFJqQ3wD94QBV4+dmC1xYeoHhOydwAYQES2ilXJGS3FNO+84QlZc8CY5zYw0E4WMr0iL7W0Met2v0fr1IaNj4OgLiPqFr7Xee1PzEqa9x6ZKQlM4gLd5Nia+qm/eWMStA6V5wzKNIQLLtdP920N7GLWGBpV39hB9jAWvB+MEz/p+Juf1qUCDByohUuifepSVZCfdm1j12ls/+Gx2iwC5gO8a3Nhen/v+JF9NXUnMTFh5KZCbVNj5w20/4Ro2z4wwonEsFXF2lbqMTHHLXlXJfuoDW2Nbdw6by3w9hiX2YdIqE/AJxVrLL8MdPGm9gtPyOxFLrys7lz6XIyPGPyqQSD/F+LXh/0dsnlYTucLbWQ4JCvT1GQd+qy//mx7NlmrPnQR8faHwPQGQAXlm1MH+KM1YkTGMmWmE4yk0i3WeiV0U/caO0hbSaD7QPTkru22F/zNN2CA9nel5zIQu3NnTWtAU9eva/zIGO3PAN8pgKgNRem68IiP0cINzPsIOLp+d/rNLbH2fWZrmADtZ8bRlWV2fQ3qcV/2KTA6HFjtC952Bjd4K9bUlLxh47IPKI8WE6IPrLEoe1+rtvBRWeMF89MAwr0Lk9va/k9ifMoEKRidpdvO2cXrBSHAyUxLhAvz/ElX0/AM5fH7OZPhN+WbLX+vvillDyVXpXpLXSjdYmvT0hvu3z/rWhLO4wR8t2NHmk8O4cCB9kjBUjl1yrcXaAFfNptK95hYazeW4i9KbVq3O71KbRoTrRUTOArEQ9/dbd/W1HVqj7/fbCsjsAmS7lnusG/UwRs2wQ3eijV+wCs84F17Ebw9x2f+XEpSh/xxxTsprWbf88eMnvdkdRfM4wCOJeC3YHgyoGt4Z5QKsBfg90JPy/VFRXWb8CKHZYwm8DpivFKy1VbD3tp3qP0pyVgMYF7JNpuyRbd4GzRA206MQUQc9cFOt5TztiH9tV0ExEIK87ZCW7M4WwLd0NjoNGXZ6O6SdOv6Uu2wGjfObL0LRIeY8K93iu0/D3SuQPo9bNJ+6yLXRwCOLCvLuKVKYIWNy3YHo6/1A16EuCVvXkqVzttzTGYOC0oC8Nj+vJQlgSzeWn1Mo7RrXLJNVzAbSVw4RSEdvyzPm3U24PU1TUTtESoA5ILhrLyiusu27xC7A+AIYv5lXW7TgNcJsOOQfumjiLCemV//YJf2qHdYXNzzoaLy23NgPr6lML3V8wvHRdsmgvE2EVvXFGdkeumaEGVzgBEBEqNWlzSf1+0hs20PWPYC5NA3y7KrpDqF3Zvllrx5Fh+d9wqEVDLjWMW7F8EbPiZLSbJfhIgLV5fnad8GeA7/Md2i4+zvEmMMQcZ6vUoxMVpbCiX1IfixcHtalXRv6U0lJGhtfvwGyuJzckuh1tm73vBYbSgBW5Q5bXOhFpA5rTlpnRCjdSWXPAHg81UldhXko7f7TFp3IVwHBfDF8tKMG5pjzcciU8IlxD5ieXiJI+vW6nNS2NisUyqwYv+7fsArQypZSd51bvAq47z4+oSKJPp+3/rUTs1B3OU2R+wg+1QQvwqm6YX5tucVff2H2CMg2cFM63ftsI1pTZpHxKR9TMDtfCW137xZ+1GtHR+T9pwAnpZEYzYVas3iFGrsniaZrUcJuNFgNHReVqSd9I5/MNK2D8ThBIpfVmpvstftMZNtD4iVnTn+1VrzUfhYt+Tdv84XvO1dRl1t2Lc+1Q3eMdm3G6T8GODdezdYYxq74f+E/rEDtaGCaAuBF+3M12bq4B2UPlEwlLs6aceOtBa1MtTm0T2x2iowT5AuumtTibrpA6OjbUUg6msg+mVekf/Ir5bm9f2R1ncNwBhB6P5macafvOtNjrDeKQQUnUffLMtokuXhcZOlOyAOAviflxyZ19feE/UcnanrvPvWp/ioDR2cbvDu3eAGb69RGf2IqQDAm3veS324pRn075h/kLKxSrlfxVTk79RGKRoGDkx/QjC/wEyT8wvSVExEq7Ux0WmLQXhKggdt2G1XtnWMiUr7RBBuhaAr84rcEWut3aaYbS8R8zRi2fcNR1aNuORHIq1VXrfXHFmX7HX7gynVoUI/mDHixfLM933A22uUG7x7N/iC98rzbvDu2egBb3zWMEHyfWIsLt9kndHaDGuN9eL6ZdwqyfUJQRR9UKD86MCQ2PSZRLwAROO3FaStbQ06vGuMjdZyCZwI0Mi8YrcD4N4o2+cE/Ex+I0Lz/qYpNa7V28NmqwpNnUXEA18rzdxZnYDHlOWBcYgYn7/iyKjSiRtD5HSTtSeD9wrg00WOTL8Vm6jXyExd592z0Re8HSsNOng/3GTVJW/v4RmDBEGZb152vG/VY3b/r7WhA+x3Qkr12tu2dZemR3cNjbXNJIgFDIzbWqi905p7HmdOywFxEkkxck2ZG7wTomxHANzkdJ6+Iq98YeDWlGYkfKrJupgITzGo35Iyu09owONeqUkY+XJZZqO9bjNMqfsIHM7g4Ysc2epy6tOod3yGDt4PN6X6qA1XnTPosQ3l77vB22d4Vg8BqWJD33e8bx3RjLy4bKYaHqvcnryTgCWbCzU9znZEP20GSSwk0PiNRa0recebbbkCSGQgfrXnwjJJuUklhzvZcMOaMu2LfwfzpplSVwCYRKBeLzkylIu6RvtDZEo4Me1j8OEXalkJGqJ3VoT1bpA8QMCn8x1ZddbJo4gRbvA6NvuCt9NZQyUxjpVt8UjeOK1jCInvAfyrdKutWQ3RDW2otZ7Hx2gqk2AFgZM3FqXnqnXvidFmgHmhYB6/vtjeqmrDJLPNqzbEr/CA9wFz6psEmsJM/Zb7kXqtwas/mFKVEOvhZP7ZK+VZx/ytOd2UWkKAmRhxC8szqyLRGqJvtimljAATMY2aV55ZZ5YGmYZnnCJGh7ItvuDt8oOoFMCx4q22KidFdJz9tNKRxVkZ0hJ5Vw1trKWf3xOjLSLm6WAas6HYbYYaHaPNIOaF/G8A72STLZcFJ1Y3PT1ksk5mwlIwpy51ZGa1NE9qz/9k3JOh9P1V58D44fnyTHf+mp82w2TpLkAHAfpsgSMzoHjfxEhrN7D8i3L1z3Vk3Vbf3sg8zK6bykq3Wn3Uhq5nhK7zFm+7CN6+Q+yrAYwnlnFF2y/GdbY2A1tqvdF9074loLM0nu+6oSDnG7XO2ChtBhEvBHh8XitL3odMtlwQJxJR/Bseyft7k3aNk1xfgvng645Md5BSK7YZptR+ABcI4M0Fjqx6rU5zIiwfgkRvFhz/XGlWg3bfJFNKORH6QNKIHD8WhurbpOg4u25tKN7mC95rTrvBW/TBRfDGDEkzE4uSQOJH4+K0jq5KioTg3uq+505N56sBEqRiaEH/AMu9AvShy2Dck5/fOqnpdZ1zQrT1ToDUZa0gr9heFVk2Lso6g0ALQRi/ppXB+7ApNVcQJUqS8W9UO/ypJutRItxIUtb52m4pPD9tSlmjwhlAiJpfllVvWGqSbjWQewn0WW4D0tfSx9KDDKTUkU+zyurWdb37or5D0k8RqEPRBzXBq1yT35wi3T1cmJ9WI563/8D07wjoZBDG67dvT1GB0zWaijMlxlSAHlXhhypTwZvG7ImjVTUM9FTm6inQIGwkiTe3tGBMaH0HOi7KqlLjBwiBQas9NlXVf2KkdQYTLSTG+JVlravzPmpSFzZOhKjpYXpcqQ7gpSp7+mVH5kMtBdTa884eNLs9nWmrbMsnn3VkVrms61s/2ZRSQUAPFjQip9TXXusdmxppUSBXtS5GZgZgoaDYQQq86LAr31ZDbVDg/fYkVaoI+Z07aoJ3wID0RwV4CQGbtu/URnoXHxqjXecyIpsYUzwgVeBcJolKDZIOnRPGo1263HZSVVqJi0u/Ulxw3iBcxrvB6AnCBGK+1p2gTwUEV+J7hRkBZ7029fBUVJSB6JDKTF5Vaq/hl1fgFYSFxDR+eSuDd5pSGyATSXD8S9UkrwZNnDA5zxDQjgyua/5YkqNiDVq8JUWkLGNVL4MwLbcsqyqfrb6FLb0tvxNGUtkWRzLKsqqiwqqP0UyW7kxQ+vHx9LKsgIKNqL8CL6PDzh2+4D35HXTw5u/0Te8Y0l9TEvc6QJg+KLCVD43V7gVBFQtR1VNOMGSisbL9u5vKEtUFL6AWH5s+jKRMFUAfPfOdKH19kaYFNLiJnSaZbZ8Q+FYhefBbjswa9SImR+oOGd1UtrSVwfukKVWVp9fB+8daOuNTkdZHiPk1AMWLHZl9m8iCBoenRKb0kUA5IL/KKctpVOaGLdKie8sEeIxWluMTj5Fu1vMfTQweoZXm+HjT/BFHAwZopwTQIX9nmo/kPf0tdJ33gwJf8A7ub48ysCwm5iNgrANhtpK2BLK37YqsvLxL9/yMjtEmk+Sl7sIitHZdsTa+Qc42ocMDZtsiAqvorC1vlWb45IE95AEv/xvAO90DXoPg+IV+LjwzdWcAR4D4mYVl2c82gQ31Dp0ZMbPdFeKKY+oy6xIyJqckp97cutqTaVHJd5CkQyplKfScq1tSxVxlctWb3ZTUk4Rhr9J1raUN67recTS4v6owQx22F/hK3h9UOB5wbOsu/4l1w2NsaQShKYgJ0KcwYOjGAu1vzcHA8ZHa9VK4XgYonsA715ZkDGyOeWvPMdlsSyRmZc/9cmlZhnpd1SoSBTwSmTKDIBYq8L7eypJ3pik1F7rOC7/gTTIndXZKw5cAhYDlxOfKs1e1BJ9skSn7WGWuEOZllGZdUjC+PdJykoiuYiF+ZyvOUMHless0W/ao0Bl11qkBWCSqwDusn3aagfZbd2k1vlBF6bxnv4bupNhcVHdW6D2x2nIw3w/Ggfd2p6sibs3W7ouyTWPgJU9pp6IrfzQMW1LhDgtsjvaIOVVjpjQCXCwNt79R7j9Ca2qksjZgIUDjX21l8M4ypepOChKo09Q022SNMEA6PGXpps51ZCpVollacq/kq0NCxHYChxHzas2Ro3IBG92yIi33MtFaAVlkKc3RY0ZUy4qy9NALDwKfWkqzG/WtUzQ8Vi/wJt4v9AXv+RNuybuxHvAqAsb0TVNlmJTO+4MgGptX1DT77+QYra3TyYsZPJWAr8D4OxFHM/AZsZy4ogHzTEOcfaJX8tWVIQZVcGQcSfwAozF6SYmm8vL8tmkRKTOIlKmMx79UltWqHrbZylTm0Xnn1iOVLBGW/hC0012vjl/85tzXM5dULGlSjQvNnDoALPOI9IqS69LKssc2xNu6nmeblVWKr2OJO1Mc2coJobccs6WYCFHMNDq5NKtRtdooPjbtE2K6tc1PEFpdT42J0YydmS8AdGzD7obz8e/tq9khpdVNEq0hA2WtLdKqiAx005PM1qnqA0lAV8E4LCFiV5Slf/ag2bqYWAWCAET8GkvMfcuR2ajySgkJCYYux379OIPmEyGEQPuFwTX6peKsekufPuEBLxONe7Eso1UDcxIjU3KIkcQC8fWBV/HXYrL81kC0iQEVR3ucmJMzHNnLAuW9t59mTv4VkbAQs17QEIQMa0m2rbHzePvnmi0PgPAWGFuSSrOr7hS5kdZuJFwKI0cSS7L9WiHqW5PuibZNJ6JFKn51/e50VTGmSucbE52mLFfH3y0OLE9qglkbAsgXoGJN3W2dSnw0ugz5KxzaV3URMtlk7SkFDQPzIwTcoF/8CM+f+dYwp3rI35RI2zgBXuS2cqjGq4ixgdm4641y/ylJyqT0ZaQrkoGhzPx7AerkrvxC2S87MlICOZAnPeAlcN/FtWJXAxnflD7JppRniDCXCPFZAeiD2h1aG+5cOR+gP3jWPUrEb4Boq1aiZ337bVqE1sUoLgxkyFEgMc5jfz/MoCespYHHJfibfG6URTnCroQUtyeWZVaV63o2Ss88N4Mpfk4Ae6s9t46TsdFp6qan0shV+VEVSHyewEYwjyDC4bXF9hq5Qw0dxv2RtodBSAe4ur30XwI4wsAJUl9sDJXpyz8lpt8ACPU6K4h5hYRxwTKHpm6mPm1q2NQQbnPN4yRImdD0VCSPTfkoCCo1RZULVR+6jgRWFzBV3Fl59PRG4CUs5PwXS3P+q6F9eJ/PMqXMYr3YBvYDfNTtX5EXC8aq62rVZO5i4cLTx/tnEgKQnkLiAhBeN41U/kZfSrx/YqYeBNwCSaMy6wlSqT2D3Zx4F7OYQ6CJShJ4PuxnCfgYoC8AeRoQyqypElZvBtFN3gLPDDouSNosJTnNojs/G2XRI9BAGDenOFt/c81X9JHhEIE/e7okJ6C4B3/gRVzck6Edz3R5AWBVNK/qIDxb/vPqEvtdgR509X4PmFMHEGgEMVQVxpt8Cgq713IyUCyY3xVA/pIAVQFdBfji14MF00gCD4NHYleB5SIh5wSwUwLrncz5dUVA1be/p02WpwF67uIHwMsl90uq5t9reQ3dbxH9febFUA2vYi0voz8eua05/u2jDZ2LZk66RcAwRLgLFsYQuMb3TFej/TP1lpREm1JKslW2TLO1hTFaJ+mqVKVu//l0SY4ebLMgOlmFUYZLYMzsYl+7byCL1+DjxN5aR9kO1wvpCnE6DWw0usjp5B/XBgio+ha8f9Ds9m3OhHYFiy7SgDYhLsP350NxclmR9qU/81QgxFfv82RvrSOHOq/m87Kz0WgIAfPJSjZ+90o96kqga0zvbbk21Eg3u1i4QuodpO5Hvj3qH1N7wpp3LH2sICGNof/QqiU6Bkp77X7zYuZc56w0doIBV5Hg8y6n4Vtnm5BvtBZOJ5ofnfyRqq0xqziHFvW3XMsXVJ07/mJmcc4lZxnXFgKXypPguCAH6uXAoijLHCbMY2b13RgqN3A6sRw7oyT3knPcguANgq5VOLAoJsXMktUF7TCBb2HQsZnF2U2qKhkEb6scXXCR+ebkXwlB//B+lwiTiJ+1u+H43vo4FwRvEFetwoEF/VJuIKf8nEAkwUdmFec02q5bm9AgeFvl6IKL/DHyGVXQ+18qtFsC42Z5TGZN4UwQvE3hXnBswBz4Y3TqzU7Iw6pk64zibJ+SrQFPVK1jELyXwrXgmEZzYH5MSh+D5HJimja9JLAg9oYWCYK3IQ4FnzcLBxZGWaxESJ9RnO3Hn3hpSwTBe2l8C45qJAcWRadskSyPzSrJUYFXzdKC4G0WNgYnqY8Dr4ZNDTnXvuvCC0Z6dvYuvb5zs7QgeJuFjcFJ6uOAHtvgPHvj06Vzq0qhNgfH/hdM4kDeSTEPLQAAAABJRU5ErkJggg=="
                                                                                alt="Loonoy"> </a>
                                                                    </th>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </th>

                                                </tr>
                                            </tbody>
                                        </table>
                                        <!-- !Include header -->
                                        @yield("content")

                                        <!--Dodat Payment-->
                                        <table class="wrapper"
                                            style="background: #f7fdff; border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;"
                                            align="center">
                                            <tbody>
                                                <tr style="text-align: left; vertical-align: top; padding: 0;">
                                                    <td class="wrapper-inner"
                                                        style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; hyphens: auto; line-height: 25px; text-align: left; vertical-align: top; word-wrap: break-word; padding: 0;">
                                                        <table class="spacer"
                                                            style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
                                                            <tbody>
                                                                <tr
                                                                    style="text-align: left; vertical-align: top; padding: 0;">
                                                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 40px; font-weight: 300; hyphens: auto; line-height: 40px; mso-line-height-rule: exactly; text-align: left; vertical-align: top; word-wrap: break-word; padding: 0;"
                                                                        height="40">&nbsp;</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        
                                                        
                                                        <table class="row"
                                                            style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
                                                            <tbody>
                                                                <tr
                                                                    style="text-align: left; vertical-align: top; padding: 0;">
                                                                    <th class="small-12 large-12 columns first last"
                                                                        style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0 auto; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; hyphens: auto; line-height: 25px; text-align: left; vertical-align: top; width: 560px; word-wrap: break-word; padding: 0 20px 10px 20px;">
                                                                        <table
                                                                            style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
                                                                            <tbody>
                                                                                <tr
                                                                                    style="text-align: left; vertical-align: top; padding: 0;">
                                                                                    <th
                                                                                        style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; hyphens: auto; line-height: 25px; text-align: left; vertical-align: top; word-wrap: break-word; padding: 0;">
                                                                                        <p class="text-center"
                                                                                            style="margin: 0; margin-bottom: 10px; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; line-height: 25px; text-align: center; padding: 0;">
                                                                                            @lang('ch-eshop::emails.thank-you-title')<br>@lang('ch-eshop::emails.loonoy-team')
                                                                                        </p>
                                                                                    </th>
                                                                                    <th class="expander"
                                                                                        style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; hyphens: auto; line-height: 25px; padding: 0; text-align: left; vertical-align: top; visibility: hidden; width: 0; word-wrap: break-word;">
                                                                                        &nbsp;</th>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </th>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </center>
                </td>
            </tr>
        </tbody>
    </table>
    <!-- prevent Gmail on iOS font size manipulation -->
    <div style="display: none; white-space: nowrap; font: 15px courier; line-height: 0;">&nbsp; &nbsp; &nbsp; &nbsp;
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div>
</body>

</html>
