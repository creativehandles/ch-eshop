@component('mail::message')

{{-- custom styles --}}
@slot('customStyles')
    <style>
        .tbl-order-details,
        .tbl-order-items
        {
            width: 100%;
            margin-bottom: 1.5rem;
            padding: 1rem;
            background-color: #f0f8ff;
        }

        .tbl-order-details td,
        .tbl-order-items td
        {
            vertical-align: top;
        }

        .tbl-order-details tr:nth-child(even):not(:last-child) td {
            padding-bottom: 0.75rem;
        }

        .pb-075 {
            padding-bottom: 0.75rem;
        }

        .pt-065 {
            padding-top: 0.65rem;
        }

        .text-dark {
            color: #3d4852;
        }

        .span-final-prices {
            display: inline-block;
            width: 5rem;
        }

        .text-right {
            text-align: right;
        }
    </style>
@endslot

{{-- Greeting --}}
@if (! empty($greeting))
# {{ $greeting }}
@else
@if ($level === 'error')
# @lang('Whoops!')
@else
# @lang('Hello!')
@endif
@endif

{{-- Intro Lines --}}
@foreach ($introLines as $line)
{{ $line }}
@endforeach

{{-- Salutation --}}
@if (! empty($salutation))
{{ $salutation }}
@else
@lang('Regards'),<br>
{{ config('app.name') }}
@endif

<table class="tbl-order-details">
    <tr>
        <td><b>{{ __('ch-eshop::notifications/newOrderCustomerNotification.email.Order date') }}</b></td>
        <td><b>{{ __('ch-eshop::notifications/newOrderCustomerNotification.email.Order no') }}</b></td>
    </tr>
    <tr>
        <td>{{ date_format(date_create($order->created_at), 'Y-m-d') }}</td>
        <td>{{ $order->order_no }}</td>
    </tr>
    <tr>
        <td><b>{{ __('ch-eshop::notifications/newOrderCustomerNotification.email.Status') }}</b></td>
        <td><b>{{ __('ch-eshop::notifications/newOrderCustomerNotification.email.Payment method') }}</b></td>
    </tr>
    <tr>
        <td>{{ $order->status->title }}</td>
        <td>{{ __('ch-eshop::config.paymentMethods.' . $order->payment_method) }}</td>
    </tr>
    <tr>
        <td><b>{{ __('ch-eshop::notifications/newOrderCustomerNotification.email.Billing address') }}</b></td>
        <td><b>{{ __('ch-eshop::notifications/newOrderCustomerNotification.email.Delivery address') }}</b></td>
    </tr>
    <tr>
        <td>{!! $order->html_billing_address !!}</td>
        <td>{!! $order->html_delivery_address !!}</td>
    </tr>
    @if($order->is_company)
    <tr>
        <td colspan="2"><b>{{ __('ch-eshop::notifications/newOrderCustomerNotification.email.Company') }}</b></td>
    </tr>
    <tr>
        <td colspan="2">{!! $order->html_company_details !!}</td>
    </tr>
    @endif
</table>

<table class="tbl-order-items">
    @foreach($order->items as $item)
    <tr>
        <td colspan="2"><b>{{ sprintf('%s - %s', $item->variant->product->title ?? '', $item->variant->color ?? '') }}</b></td>
    </tr>
    <tr>
        <td colspan="2"><small>{{ __('ch-eshop::notifications/newOrderCustomerNotification.email.Item code') . ': ' . $item->code }}</small></td>
    </tr>
    <tr class="text-dark">
        <td class="pb-075">{{ $item->selling_price_wo_tax . ' x ' . $item->quantity }}</td>
        <td class="pb-075 text-right">{{ $item->total_price_wo_tax }}</td>
    </tr>
    @endforeach
    <tr class="text-dark">
        <td colspan="2" class="text-right pt-065" style="border-top: 1px solid #ddd;">
            {{ __('ch-eshop::notifications/newOrderCustomerNotification.email.Sub total') }}
            <span class="span-final-prices">{{ $order->total_cost_wo_tax ?? 0 }}</span>
        </td>
    </tr>
    <tr class="text-dark">
        <td colspan="2" class="text-right">
            {{ __('ch-eshop::notifications/newOrderCustomerNotification.email.Discount') }}
            <span class="span-final-prices">{{ sprintf('(%d)', $order->total_discount ?? 0) }}</span>
        </td>
    </tr>
    <tr class="text-dark">
        <td colspan="2" class="text-right">
            {{ __('ch-eshop::notifications/newOrderCustomerNotification.email.Delivery price') }}
            <span class="span-final-prices">{{ $order->delivery_cost_wo_tax ?? 0 }}</span>
        </td>
    </tr>
    <tr class="text-dark">
        <td colspan="2" class="text-right">
            {{ __('ch-eshop::notifications/newOrderCustomerNotification.email.Payment price') }}
            <span class="span-final-prices">{{ $order->payment_price ?? 0 }}</span>
        </td>
    </tr>
    <tr class="text-dark">
        <td colspan="2" class="text-right">
            {{ __('ch-eshop::notifications/newOrderCustomerNotification.email.Tax') }}
            <span class="span-final-prices">{{ $order->total_tax ?? 0 }}</span>
        </td>
    </tr>
    <tr class="text-dark">
        <td colspan="2" class="text-right" style="font-weight: bold;">
            {{ __('ch-eshop::notifications/newOrderCustomerNotification.email.Total', ['currency' => $order->currency]) }}
            <span class="span-final-prices">{{ $order->total_pay ?? 0 }}</span>
        </td>
    </tr>
</table>

{{-- Action Button --}}
@isset($actionText)
<?php
    switch ($level) {
        case 'success':
        case 'error':
            $color = $level;
            break;
        default:
            $color = 'primary';
    }
?>
@component('mail::button', ['url' => $actionUrl, 'color' => $color])
{{ $actionText }}
@endcomponent
@endisset

{{-- Subcopy --}}
@isset($actionText)
@slot('subcopy')
@lang(
    "If you're having trouble clicking the \":actionText\" button, copy and paste the URL below\n".
    'into your web browser:',
    [
        'actionText' => $actionText,
    ]
) <span class="break-all">[{{ $displayableActionUrl }}]({{ $actionUrl }})</span>
@endslot
@endisset
@endcomponent
