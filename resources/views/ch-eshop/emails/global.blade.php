<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Order {{ $order->order_no }} status has been changed</title>
</head>

<body>
    <p>Order status has been changed to {{ $status->title }}</p>
    <ul>
        <li>Order ref : {{ $order->order_no }}</li>
        <li>Order currency : {{ $order->currency }}</li>
        <li>New status : {{ $status->title }}</li>
    </ul>

</body>

</html>