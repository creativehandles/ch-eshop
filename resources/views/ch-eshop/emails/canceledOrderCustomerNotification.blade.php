@extends('ch-eshop::emails.base-template')

@section('content')
    <table class="row"
        style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
        <tbody>
            <tr style="text-align: left; vertical-align: top; padding: 0;">
                <th class="small-12 large-12 columns first last"
                    style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0 auto; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; hyphens: auto; line-height: 25px; text-align: left; vertical-align: top; width: 560px; word-wrap: break-word; padding: 0 20px 10px 20px;">
                    <table
                        style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
                        <tbody>
                            <tr style="text-align: left; vertical-align: top; padding: 0;">
                                <th
                                    style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; hyphens: auto; line-height: 25px; text-align: left; vertical-align: top; word-wrap: break-word; padding: 0;">
                                    <table class="spacer"
                                        style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
                                        <tbody>
                                            <tr style="text-align: left; vertical-align: top; padding: 0;">
                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 20px; font-weight: 300; hyphens: auto; line-height: 20px; mso-line-height-rule: exactly; text-align: left; vertical-align: top; word-wrap: break-word; padding: 0;"
                                                    height="20">&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <h1
                                        style="margin: 0; margin-bottom: 10px; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 25px; font-weight: 600; line-height: 25px; text-align: left; word-wrap: normal; padding: 0;">
                                        @lang('ch-eshop::emails.canceled-order-title')</h1>
                                    <table class="spacer"
                                        style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
                                        <tbody>
                                            <tr style="text-align: left; vertical-align: top; padding: 0;">
                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 20px; font-weight: 300; hyphens: auto; line-height: 20px; mso-line-height-rule: exactly; text-align: left; vertical-align: top; word-wrap: break-word; padding: 0;"
                                                    height="20">&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <p
                                        style="margin: 0;color: #18414e;font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;font-size: 14px;font-weight: 300;line-height: 25px;text-align: left;padding: 0;">
                                        @lang('ch-eshop::emails.dear-customer'),</p>
                                    <p
                                        style="margin: 0; margin-bottom: 10px; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; line-height: 25px; text-align: left; padding: 0;">
                                        @lang('ch-eshop::emails.canceled-order-text', ['order_no' => $order->order_no])</p>
                                </th>
                                <th class="expander"
                                    style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important; color: #18414e; font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif; font-size: 14px; font-weight: 300; hyphens: auto; line-height: 25px; padding: 0; text-align: left; vertical-align: top; visibility: hidden; width: 0; word-wrap: break-word;">
                                    &nbsp;</th>
                            </tr>
                        </tbody>
                    </table>
                </th>
            </tr>
        </tbody>
    </table>
@endsection
