<div class="form-row">
    <div>
        <table class="mb-1">
            <x-ch-eshop::price :title="__('ch-eshop::order/create.Sub total')" :price="$order->total_cost_wo_tax" :locale="$order->language" />
            <x-ch-eshop::price :title="__('ch-eshop::order/create.Delivery price')" :price="$order->delivery_cost_wo_tax" :locale="$order->language" />
            <x-ch-eshop::price :title="__('ch-eshop::order/create.Payment price')" :price="$order->payment_price" :locale="$order->language" />
            <x-ch-eshop::price :title="__('ch-eshop::order/create.Tax')" :price="$order->total_tax" :locale="$order->language" />
        </table>
        <p class="mb-2 h4">{{ __('ch-eshop::order/create.Total', ['currency' => $order->currency]) }}
            {!! \Creativehandles\ChEshop\Services\LocalesService::formatPrice($order->language, $order->total_pay) !!}</p>
    </div>
</div>