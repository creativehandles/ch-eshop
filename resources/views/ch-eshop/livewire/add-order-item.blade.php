<div>
    <form wire:submit.prevent="addItem">

        @csrf
        <h5 class="ml-1">{{ __('ch-eshop::order/create.add_item') }}</h5>

        <div class="row">
            <x-ch-eshop::select errorfield="variant_id" colSize="col-md-3 ml-1" selected="{{ old('variant_id') }}" wire:model="variant_id" title="{{ __('ch-eshop::order/order-item.select_item') }}" label="{{ __('ch-eshop::order/order-item.select_item') }}" :data="$orderItems" />


            <x-ch-eshop::input errorfield="quantity" colSize="col-md-3 ml-1" value="{{ old('quantity') }}" wire:model="quantity" label="{{ __('ch-eshop::order/order-item.quantity') }}" />

            <div class="col-md-3 mt-2">
                <button class="btn btn-primary ml-1" type="submit" style="margin-block-start: 8px;">{{ __('ch-eshop::order/order-item.submit') }}</button>
            </div>
        </div>
    </form>
</div>