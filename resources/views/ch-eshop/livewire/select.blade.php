<div class="form-group col-md-6">
    <label for="status" class="from-control form-label">{{$label}}</label>
    <select id="status" class="form-control" wire:click="changeEvent($event.target.value)">
        <option selected disabled>{{ $default}}</option>
        @foreach($this->data as $key => $value)
        @if($key == $this->selected)
        <option selected value="{{ $key }}">{{$value}}</option>
        @else
        <option value="{{ $key }}">{{$value}}</option>
        @endif
        @endforeach
    </select>
    @if(session()->has('success'))
    <script>
        toastr.success("{{ session()->get('success') }}", "{{__('alerts.Success')}}", {
            timeOut: 5000,
            title: "ABC",
            fadeOut: 1000,
            progressBar: true,
        });
    </script>

    @endif
</div>