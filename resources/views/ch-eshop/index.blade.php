@extends('Admin.layout')

@section('content')
<div class="content-body">
    <div class="row">
        @foreach(config('ch-eshop.home_item') as $menu_item)
        <div class="col-sm-6">
            <a href="{{route(__('ch-eshop::ch-eshop-home.'.$menu_item.'.route'))}}">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">{{__('ch-eshop::ch-eshop-home.'.$menu_item.'.title')}}</h5>
                        <p class="card-text">{{__('ch-eshop::ch-eshop-home.'.$menu_item.'.description')}}</p>
                    </div>
                </div>
            </a>
        </div>
        @endforeach
    </div>
</div>
@endsection

@section('scripts')
    @yield('component-scripts')
@endsection