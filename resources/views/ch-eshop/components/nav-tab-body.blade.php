<div {{ $attributes->class(['tab-pane',$active ?? null])->merge() }} role="tabpanel">
    {{$slot}}
</div>