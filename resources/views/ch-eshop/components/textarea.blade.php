@php
$label = $label ? ucfirst($label) : null;
$id = $id ?? uniqid();
$type = $type ?? 'text';
@endphp
<div class="form-group {{$colSize ?? 'col-md-12'}}">
    <label for="{{$id}}">{{$label}}</label>
    <textarea id="{{$id}}" {{ $attributes->merge(['class' => 'form-control']) }} cols="30" rows="10" placeholder="{{ $placeholder ?? $label}}">{{$value ?? null}}</textarea>
</div>
