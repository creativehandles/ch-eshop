@php
$title = $title ? ucfirst($title) : 'Choose..';
$id = $id ?? uniqid();
@endphp
<div class="form-group {{$colSize ?? 'col-md-12'}}">
    <label for="{{$id}}" class="from-control form-label">{{$label ?? null}}</label>
    <select id="{{$id}}" {{ $attributes->merge(['class' => 'form-control'])->except(['data']) }} >
        <option {{ ! isset($selected) ? 'selected' : '' }} value="">{{$title}}</option>
        @foreach($data as $key => $value)
        <option value="{{$key}}" {{ isset($selected) && $selected == $key ? 'selected' : '' }}>{{$value}}</option>
        @endforeach
    </select>
    @if(isset($errorfield))
    @error($errorfield) <span class="text-danger">{{ $message }}</span> @enderror
    @endif
</div>