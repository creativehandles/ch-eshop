@php
$label = $label ? ucfirst($label) : null;
$id = $id ?? uniqid();
$checked = ($checked)? true : false;
@endphp
<div class="form-group form-check">
    <input @if($checked) checked @endif {{ $attributes->merge(['class' => 'form-check-input']) }} value="{{$checked}}" type="checkbox" id="{{$id}}">
    <label class="form-check-label" for="{{$id}}">
        {{$label}}
    </label>
</div>