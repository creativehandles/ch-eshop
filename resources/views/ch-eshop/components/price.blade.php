<tr>
    <td style="vertical-align: top">
        <p>{{ $title }} {{ ! empty($currency) ? "($currency)" : '' }}</p>
    </td>
    <td style="vertical-align: top; text-align: right;">
        <span>{!! \Creativehandles\ChEshop\Services\LocalesService::formatPrice($locale, $price) !!}</span>
    </td>
</tr>