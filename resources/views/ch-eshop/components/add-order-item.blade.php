<form action="{{ route('admin.ch-eshop.order.add.orderItem',['order' => $order->id]) }}" method="post">
    @csrf
    <h6 class="ml-1">{{ __('ch-eshop::order/create.add_item') }}</h6>
    @csrf
    <x-ch-eshop::select selected="{{ old('item.ch_eshop_variant_id') }}" name="item[ch_eshop_variant_id]" title="{{ __('ch-eshop::order/order-item.select_item') }}" label="{{ __('ch-eshop::order/order-item.select_item') }}" :data="$orderItems" />
    <x-ch-eshop::input value="{{ old('item.quantity') }}" name="item[quantity]" label="{{ __('ch-eshop::order/order-item.quantity') }}" colSize="col-md-12" />
    <!-- <x-ch-eshop::input value="{{ old('item.discount') }}" name="item[discount]" label="{{ __('ch-eshop::order/order-item.discount') }}" colSize="col-md-12" /> -->
    <button class="btn btn-primary" type="submit">Submit</button>
</form>