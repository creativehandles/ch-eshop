<!-- form input -->
<div class="form-group">
    <label>{{$title}}</label>
    <input {{ $attributes->class(['form-control'])->merge(['type' => 'text']) }} @isset($old) value="{{old($old)}}" @endisset>
</div>