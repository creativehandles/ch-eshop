@php
$label = $label ? ucfirst($label) : null;
$id = $id ?? uniqid();
$type = $type ?? 'text';
@endphp
<div class="form-group {{$colSize ?? 'col-md-12'}}">
    <label for="{{$id}}">{{$label}}</label>
    <input type="{{$type}}" {{ $attributes->merge(['class' => 'form-control']) }} id="{{$id}}" placeholder="{{ $placeholder ?? $label}}">
    @if(isset($errorfield))
    @error($errorfield) <span class="text-danger">{{ $message }}</span> @enderror
    @endif
</div>
