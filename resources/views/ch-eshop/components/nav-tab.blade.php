<div class="card">
    <ul class="nav nav-tabs nav-underline" role="tablist">
        @foreach($headings as $item)
        <li class="nav-item">
            <a class="nav-link {{$item['active'] ?? null}}" data-toggle="tab" href="{{$item['body']}}" role="tab" aria-selected="false">
                <i class="{{$item['icon'] ?? null}}"></i>
                {{$item['title'] ?? null}}
            </a>
        </li>
        @endforeach
    </ul>
    <div class="card-content collapse show">
        <div class="card-body">
            @if(! isset($formOverride))
            <form action="{{$action??'#'}}" method="post" id="{{$id??null}}" enctype="multipart/form-data">
                @if(isset($modify) && $modify == true)
                    @method('PATCH')
                @endif

                @csrf
            @endif
                <div class="row">
                    <div class="col-md-12">

                        <div class="form-body">
                            <div class="tab-content px-1 pt-1">
                                {{$slot}}
                            </div>
                            <div class="form-actions left">
                                <button type="reset" class="btn btn-warning mr-1">
                                    <i class="ft-x"></i> {{__('ch-eshop::ch-eshop-home.general.reset')}}
                                </button>

                                <a href="{{ URL::previous() }}">
                                    <button type="button" href="" class="btn btn-warning mr-1">
                                        <i class="ft-arrow-left"></i> {{__('ch-eshop::ch-eshop-home.general.goback')}}
                                    </button>
                                </a>

                                <button id="MainFormSubmit" type="submit" class="btn btn-primary">
                                    <i class="fa fa-check-square-o"></i> {{__('ch-eshop::ch-eshop-home.general.save')}}
                                </button>
                            </div>

                        </div>
                    </div>
            @if(!isset($formOverride))
            </form>
            @endif
        </div>
    </div>
</div>