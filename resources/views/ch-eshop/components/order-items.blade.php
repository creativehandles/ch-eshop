<h5 class="card-title">{{__('ch-eshop::order/edit.order_item')}}</h5>
<div class="shadow-none p-3 mb-5 bg-light rounded col-md-12">
    <livewire:ch-eshop-order-item-table :order="$order" />
</div>