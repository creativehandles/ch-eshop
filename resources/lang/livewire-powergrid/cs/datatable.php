<?php

return [
    'buttons' => [
        'filter'            => 'Filtrovat',
        'clear_all_filters' => 'Zrušit filtr',
    ],
    'labels' => [
        'action'           => null,
        'show'             => 'Zobrazit',
        'results_per_page' => 'záznamů',
        'clear_filter'     => 'Zrušit filtr',
        'no_data'          => 'Žádné výsledky',
        'all'              => 'Vše',
        'selected'         => 'Vybráno',
        'enable_filters'   => 'Zavřít filter',
        'search'           => 'Vyhledat:',
        
        'disable_filters'  => 'Zavřít filtr',
        'enable_filters'   => 'Otevřít filtr',
    ],
    'placeholders' => [
        'search' => 'Vyhledat...',
        'select' => 'Zvolte období',
    ],
    'pagination' => [
        'showing' => 'Zobrazuji',
        'to'      => 'do',
        'of'      => 'z',
        'results' => 'výsledků',
        'all'     => 'Vše',
    ],
    'multi_select' => [
        'select' => 'Vybrat',
        'all'    => 'Vše',
    ],
    'select' => [
        'select' => 'Vybrat',
        'all'    => 'Vše',
    ],
    'boolean_filter' => [
        'all' => 'Vše',
    ],
    'input_text_options' => [
        'is'           => 'Je rovno',
        'is_not'       => 'Není rovno',
        'contains'     => 'Obsahuje',
        'contains_not' => 'Neobsahuje',
        'starts_with'  => 'Začíná na',
        'ends_with'    => 'Končí na',
        // 'is_empty'     => 'Chybí',
        // 'is_not_empty' => 'Nechybí',
        // 'is_null'      => 'Je nulový',
        // 'is_not_null'  => 'Není nulový',
        // 'is_blank'     => 'Je prázdný',
        // 'is_not_blank' => 'Není prázdný',

    ],
    'export' => [
        'exporting' => 'Zpracovává se, prosím počkejte',
        'completed' => 'Export dokončen! Soubor je připraven ke stažení',
    ],
];