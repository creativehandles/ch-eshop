<?php

return [
    "dear-customer" => "Dear Customer,",
    "order-number" => "Order number",
    "order-date" => "Order date",
    "order-items" => "Content of order",
    "delivery-details" => "Delivery details",
    "invoice-details" => "Billing information",
    "email" => "Email",
    "phone" => "Phone",
    "thank-you-title" => "Thank you for your order and have a nice day.",
    "have-a-nice-day" => "Have a nice day!",
    "loonoy-team" => "Your Loonoy team",

    "new-order-title" => "Your order at Loonoy.com",
    "new-order-text" => "We have received your order safely and are processing the payment. If the payment was successful, you will soon receive confirmation. Should you have any problems finishing the payment, please let us know by replying to this e-mail and we will provide you with assistance.",

    "sent-order-title" => "Your order from Loonoy.com has been shipped",
    "sent-order-text" => "Your order :order_no has been sent today by post service, you can track the shipment under this link: <a href=':tracking_link'>:tracking_link</a>.",

    "canceled-order-title" => "Your order from Loonoy.com has been cancelled",
    "canceled-order-text" => "We just wanted to let you know that your order :order_no has just been cancelled. If you did not request a cancellation or do not know why it happened, please do not hesitate to ask us.",

    "subject-new" => "Loonoy | Order :order_no",
    "subject-sent" => "Loonoy | Order :order_no has been shipped",
    "subject-canceled" => "Loonoy | Order :order_no has been cancelled",

    "failed-order-title" => "Invitation to pay for your order at Loonoy.com",
    "failed-order-text" => "Thank you very much for your order :order_no from :order_date, but unfortunately we have not received payment yet. If your order is not paid, the system will automatically cancel it after three working days.",
    "failed-order-payment-links" => "Please use the following link to pay with your chosen payment method:",
    "failed-order-payment" => "If you have difficulty completing payment using your chosen payment method, you can pay your order by bank transfer:",
    "failed-order-payment-data" => "Please enter your order number in the payment reference:",
    "failed-order-recipient" => "Recipient",
    "failed-order-thank-you" => "Thank you for your payment",

    "subject-failed" => "Loonoy | We have not received payment for order :order_no",
];
