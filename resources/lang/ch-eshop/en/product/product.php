<?php

return [
    'create' => [
        'created' => 'Product Created',
        'failed' => 'Product Creation Failed',
        'page_title' => 'Create Product',
    ],
    'edit' => [
        'updated' => 'Product updated',
        'failed' => 'Product update Failed',
        'page_title' => 'Update Product',
    ],

    'create_variant' => 'New variant',
    'title' => 'Title',
    'code' => 'Code',
    'meta_title' => 'Meta Title',
    'short_description' => 'Short description',
    'description' => 'Description',
    'meta_description' => 'Meta Description',
    'images' => 'Images',
    'language' => 'Language',

    'variant' => [
        'visibility' => 'Visibility',
        'type' => 'Type',
        'code' => 'Code',
        'ean' => 'EAN',
        'color' => 'Color',
        'unit_price_wo_tax' => 'Purchase price without TAX',
        'selling_price_wo_tax' => 'Selling price without TAX',
        'availability' => 'Availability',
        'qty_available' => 'Available Quantity',
        'delivery_in_days' => 'Delivery in days',
        'unit_price_w_tax' => 'Unit Price With TAX',
        'selling_price_w_tax' => 'Selling Price with TAX',
        'tax' => 'Tax',
        'in_stock' => 'Is in Stock',
        'is_visible' => 'Is Visible to Customer',
        'option_types' => ' Parameters',
        'delete' => 'Delete variant',
        'create' => 'New variant',
    ],
    'tab' => [
        'headings' => [
            'basic_info' => 'Basic info',
            'variants' => 'Variants',
            'options' => 'Options',
            'detail_info' => 'Detail info',
            'media' => 'Media',
            'seo' => 'SEO',
        ]
    ],
    'validations' => [
        'options are invalid' => 'Options are invalid'
    ]
];
