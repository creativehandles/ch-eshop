<?php

return [
    'order' => [
        'index' => [
            'header' => 'Order Page',
            'link_name' => 'Order'
        ],
        'create' => [
            'header' => 'Order Create',
            'link_name' => ' Order Create'
        ],
        'edit' => [
            'header' => 'Order Edit',
            'link_name' => 'Order'
        ]
    ],
    'product' => [
        'index' => [
            'header' => 'Product Page',
            'link_name' => 'Product'
        ],
        'create' => [
            'header' => 'Product Create',
            'link_name' => ' Product Create'
        ],
        'edit' => [
            'header' => 'Product Edit',
            'link_name' => 'Product'
        ]
    ],
];
