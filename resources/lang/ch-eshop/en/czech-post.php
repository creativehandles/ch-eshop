<?php

return [
    'Fetching parcel code returned status' => 'Fetching parcel code returned status :status.',
    'Fetching parcel code returned error' => 'Fetching parcel code returned error code :errorCode and text :errorText.',

    'Get label data of parcels returned status' => 'Get label data of parcels returned status :status.',
    'Get label data of parcels returned empty label data.' => 'Get label data of parcels returned empty label data.',
];
