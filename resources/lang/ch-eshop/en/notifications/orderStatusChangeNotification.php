<?php

return [
    'email' => [
        'subject' => config('app.name') . ' Order Status Has Been Changed (:orderNo)',
        'line1' => 'The status of your order (:orderNo) has been changed to :status.',
    ],
];
