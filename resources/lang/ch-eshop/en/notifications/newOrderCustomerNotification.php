<?php

return [
    'email' => [
        'subject' => config('app.name') . ' Order Confirmation (:orderNo)',
        'greeting' => 'Hi :name',
        'line1' => 'We just received your order. Please review your order information below.<br>Thank you for shopping with us!<br>',

        'Order date' => 'Order date',
        'Order no' => 'Order no',
        'Status' => 'Status',
        'Payment method' => 'Payment method',
        'Billing address' => 'Billing address',
        'Delivery address' => 'Delivery address',
        'Company' => 'Company',
        'Item code' => 'Item code',
        'Sub total' => 'Sub total',
        'Discount' => 'Discount',
        'Delivery price' => 'Delivery price',
        'Payment price' => 'Payment price',
        'Tax' => 'Tax',
        'Total' => 'Total (:currency)',
    ],
];
