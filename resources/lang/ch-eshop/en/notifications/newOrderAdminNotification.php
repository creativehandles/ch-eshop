<?php

return [
    'email' => [
        'subject' => config('app.name') . ' New Order Notification (:orderNo)',
        'line1' => 'You just received a new order. Please review the order information below.',

        'Order date' => 'Order date',
        'Order no' => 'Order no',
        'Status' => 'Status',
        'Payment method' => 'Payment method',
        'Billing address' => 'Billing address',
        'Delivery address' => 'Delivery address',
        'Company' => 'Company',
        'Item code' => 'Item code',
        'Sub total' => 'Sub total',
        'Discount' => 'Discount',
        'Delivery price' => 'Delivery price',
        'Payment price' => 'Payment price',
        'Tax' => 'Tax',
        'Total' => 'Total (:currency)',
    ],
];
