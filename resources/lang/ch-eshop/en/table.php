<?php
return [
    'button' => [
        'edit' => 'Edit',
        'delete' => 'Delete',
        'create-afb' => 'Create AFB Record',
        'update-afb' => 'Update AFB Record',
        'download-invoice' => 'Download Invoice',
    ],
    'order' => [
        'create' => 'Create an order',
        'order_ref' => 'Order ref',
        'order_date' => 'Order Date',
        'user_info' => 'Name and surname',
        'payment' => 'Payment',
        'delivery' => 'Delivery',
        'total' => 'Total price',
        'state' => 'State',
        'delivery' => 'delivery'
    ],
    'product' => [
        'id' => 'ID',
        'title' => 'Title',
        'code' => 'Code',
        'variant' => 'Variant',
        'no' => 'No',
        'yes' => 'Yes (:count)',
    ],
    'order-item' => [
        'product' => 'Title',
        'code' => 'Code',
        'ean' => 'EAN',
        'qty' => 'Qty',
        'unit_price_wo_tax' => 'UNIT PRICE without TAX',
        'selling_price_wo_tax' => 'Selling price without tax',
        'total' => 'Total with tax',
    ],
    'exceptions'=>[
        'filenotfound'=>"File Not Found.",
        'error-occured'=>"Unexpected Error. Please contact Admin",
        'no-abf-record'=>"Cannot Find ABRA Record",
    ],
    'abf'=>[
        'success'=>'Successfully Processed'
    ]
];
