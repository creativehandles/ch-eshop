<?php

return [
    'deliveryMethods' => [
        'czech_post' => 'Czech Post',
    ],

    'paymentMethods' => [
        'stripe' => 'Stripe',
        'paypal' => 'PayPal',
    ],
];
