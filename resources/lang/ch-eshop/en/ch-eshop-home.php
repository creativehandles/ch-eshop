<?php

return [
    'Status' => [
        'title' => 'Status',
        'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Distinctio, sit!',
        'route' => 'admin.ch-eshop.status.index',
    ],
    'Products' => [
        'title' => 'Products',
        'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Distinctio, sit!',
        'route' => 'admin.ch-eshop.product.index',
    ],
    'Orders' => [
        'title' => 'Orders',
        'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Distinctio, sit!',
        'route' => 'admin.ch-eshop.order.index',
    ],

    'general' => [
        'goback' => 'Back',
        'save' => 'Save',
        'reset' => 'Reset',
    ],
];
