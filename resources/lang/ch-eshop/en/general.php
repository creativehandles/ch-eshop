<?php

return [
    'parcel' => [
        'post' => 'Post parcel',
        'label_download' => 'Parcel label',
    ],
    "countries" => [
        "Czech Republic" => "Czech Republic",
        "Germany" => "Germany",
        "Austria" => "Austria",
        "United Kingdom" => "United Kingdom",
        "United States" => "United States",
    ],
    'czech_post' => 'Czech Post',
    'stripe' => 'Stripe',
    'paypal' => 'PayPal',
    'CIN' => 'CIN',
    'VAT' => 'VAT',
];
