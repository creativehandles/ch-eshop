<?php

return [
    'updated' => 'Order Updated',
    'failed' => 'Order Update Failed',
    'page_title' => 'Update Order',
    'total' => 'Total',
    // 'create_variant' => 'Create Order Variant',

    'choose' => 'Choose',
    'name' => 'Name',
    'surname' => 'Surname',
    'language' => 'Select Language',
    'country' => 'Select Country',
    'currency' => 'Select Currency',
    'email' => 'Email',
    'phone' => 'Phone',
    'company' => 'Is a company',

    'invoice_details' => 'Invoicing Details',
    'billing_address_name' => 'Invoice name',
    'billing_address_surname' => 'Invoice surname',
    'billing_address_street' => 'Invoice street',
    'billing_address_zip' => 'Invoice zip',
    'billing_address_city' => 'Invoice city',
    'billing_address_country' => 'Invoice country',
    'billing_address_phone' => 'Invoice phone',

    'delivery_details' => 'Delivery Details',
    'delivery_address_name' => 'Delivery name',
    'delivery_address_surname' => 'Delivery surname',
    'delivery_address_street' => 'Delivery street',
    'delivery_address_zip' => 'Delivery zip',
    'delivery_address_city' => 'Delivery city',
    'delivery_address_country' => 'Delivery country',
    'delivery_address_phone' => 'Delivery phone',

    'admin_note' => 'Admin Note',
    'client_note' => 'Client Note',

    'order_item' => 'Order Items',
    'add_order_item' => 'Add Order Item',
    'delivery_cost_w_tax'=>'Delivery Cost',
    'exclude_tax'=>'ex:Tax',
];
