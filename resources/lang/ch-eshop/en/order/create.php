<?php

return [
    'order_no' => 'Order number',
    'order_status' => 'Order status',
    'order_status_default' => 'Select status',
    'order_total_cost' => 'Payment price',

    'created' => 'Order Created, Please add some items to it',
    'failed' => 'Order Creation Failed',
    'page_title' => 'Create Order',
    'deleted' => 'Order Deleted',
    // 'create_variant' => 'Create Order Variant',

    'choose' => 'Choose',
    'name' => 'Name',
    'surname' => 'Surname',
    'language' => 'Select Language',
    'country' => 'Select Country',
    'currency' => 'Order currency',
    'email' => 'Email',
    'phone' => 'Phone',
    'company' => 'Corporate customer',
    'cin' => 'CIN',
    'vat_no' => 'Vat No',
    'company_name' => 'Company',
    'type' => 'Delivery method',
    'payment_method' => 'Payment Method',
    'payment_price' => 'Payment Price  excl. TAX',
    'delivery_cost_wo_tax' => 'Delivery price  excl. TAX',
    'vat_rate' => 'VAT rate',
    'items_tax' => 'TAX',
    'items_price_wo_tax' => 'Items without tax',
    'total' => 'Total price',
    'product_list' => 'Product List',
    'add_item' => 'Add item',

    'invoice_details' => 'Invoicing Details',
    'billing_address_name' => 'name',
    'billing_address_surname' => 'surname',
    'billing_address_street' => 'street',
    'billing_address_zip' => 'zip',
    'billing_address_city' => 'city',
    'billing_address_country' => 'country',
    'billing_address_phone' => 'phone',

    'delivery_details' => 'Delivery Details',
    'delivery_address_name' => 'name',
    'delivery_address_surname' => 'surname',
    'delivery_address_street' => 'street',
    'delivery_address_zip' => 'zip',
    'delivery_address_city' => 'city',
    'delivery_address_country' => 'country',
    'delivery_address_phone' => 'phone',

    'admin_note' => 'Admin Note',
    'client_note' => 'Client Note',

    'tab' => [
        'headings' => [
            'order_details' => 'Order details',
            'customer_details' => 'Customer details',
            'notes' => 'Admin notes',
            'order_items' => 'Order Lists'
        ]
    ],

    'Order unable to send for delivery'=> ':order_no Order unable to send for delivery',
    'Order was successfully sent for delivery'=> 'Order :order_no was successfully sent for delivery.',
    'Order delivery status updated'=> ':order_no Order delivery status updated',
    'Order unable to send for delivery due to'=> ':order_no Order unable to send for delivery due to :errors',
    'status error'=> 'Order :order_no | :errors',
    'No Action'=> 'No Action',
    'Label Created for order'=> 'Label Created for order :order_no',
    'Parcels are still being processed. Please try again in a while' => 'Parcels are still being processed. Please try again in a while.',
    'Please select at least one order.' => 'Please select at least one order.',
    'Czech post ID is empty in the order' => 'Czech post ID is empty in the order :orderNo.',
    'Cannot create labels PDF.' => 'Cannot create labels PDF.',

    'Available quantity of the variant :variant is less than ordered quantity.' => 'Available quantity of the variant :variant is less than the ordered quantity.',
    'A variant doesn\'t have price set for the locale :locale.' => 'A variant doesn\'t have price set for the locale :locale.',

    'Sub total' => 'Sub total',
    'Discount' => 'Discount',
    'Delivery price' => 'Delivery price',
    'Payment price' => 'Payment price',
    'Tax' => 'Tax',
    'Total' => 'Total (:currency)',
];
