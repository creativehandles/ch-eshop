<?php

return [
    'Status' => [
        'title' => 'Status',
        'description' => 'Správa stavů objednávek',
        'route' => 'admin.ch-eshop.status.index',
    ],
    'Products' => [
        'title' => 'Produkty',
        'description' => 'Správa produktů a variant',
        'route' => 'admin.ch-eshop.product.index',
    ],
    'Orders' => [
        'title' => 'Objednávky',
        'description' => 'Správce objednávek',
        'route' => 'admin.ch-eshop.order.index',
    ],

    'general' => [
        'goback' => 'Zpět',
        'save' => 'Uložit',
        'reset' => 'Obnovit',
    ],
];
