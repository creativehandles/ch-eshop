<?php
return [
    'button' => [
        'edit' => 'Upravit',
        'delete' => 'Smazat',
        'create-afb' => 'Vytvořit záznam v AFB',
        'update-afb' => 'Aktualizace záznamu na AFB',
        'download-invoice' => 'Stáhnout fakturu',
    ],
    'order' => [
        'create' => 'Vytvořit objednávku',
        'order_ref' => 'Kód objednávky',
        'order_date' => 'Datum objednávky',
        'user_info' => 'Jméno a příjmení',
        'payment' => 'Platební metoda',
        'delivery' => 'Dopravce',
        'total' => 'Cena celkem',
        'state' => 'Status'
    ],
    'product' => [
        'id' => 'ID',
        'title' => 'Název',
        'code' => 'Kód',
        'variant' => 'Varianty',
        'no' => 'Ne',
        'yes' => 'Ano (:count)',
    ],
    'order-item' => [
        'product' => 'Název',
        'code' => 'Kód',
        'ean' => 'EAN',
        'qty' => 'Množství',
        'unit_price_wo_tax' => 'Jednotková cena bez DPH',
        'selling_price_wo_tax' => 'Prodejní cena bez DPH',
        'total' => 'Cena celkem',
    ],
    'exceptions'=>[
        'filenotfound'=>"Soubor nebyl nalezen.",
        'error-occured'=>"Neočekávaná chyba. Prosím, kontaktujte podporu",
        'no-abf-record'=>"Nelze najít záznam ABRA",
    ],
    'abf'=>[
        'success'=>'Operace byla úspěšná'
    ]
    
];
