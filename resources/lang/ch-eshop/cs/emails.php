<?php

return [
    "dear-customer" => "Vážený zákazíku",
    "order-number" => "Kód objednávky",
    "order-date" => "Datum objednávky",
    "order-items" => "Obsah objednávky",
    "delivery-details" => "Doručovací údaje",
    "invoice-details" => "Fakturační údaje",
    "email" => "Email",
    "phone" => "Telefon",
    "thank-you-title" => "Děkujeme za objednávku a přejeme pěkný den.",
    "have-a-nice-day" => "Přejeme pěkný den!",
    "loonoy-team" => "Váš tým Loonoy",

    "new-order-title" => "Vaše objednávka na Loonoy.com",
    "new-order-text" => "Vaši objednávku jsme v pořádku obdrželi a nyní zpracováváme platbu. O jejím úspěšném provedení Vám brzy přijde potvrzovací e-mail. Pokud máte s dokončením platby jakýkoliv problém, dejte nám prosím vědět odpovědí na tento e-mail a pokusíme se Vám pomoci.",

    "sent-order-title" => "Vaše objednávka z Loonoy.com byla odeslána",
    "sent-order-text" => "Vaši objednávku č. :order_no jsme dnes odeslali Českou poštou, zásilku můžete sledovat pod tímto odkazem: <a href=':tracking_link'>:tracking_link</a>.",

    "canceled-order-title" => "Vaše objednávka z Loonoy.com byla stornována",
    "canceled-order-text" => "jen vám chceme dát vědět, že vaše objednávka č. :order_no byla právě stornována. Pokud jste o storno objednávky nezažádali, nebo nevíte proč se tak stalo, neváhejte se nás zeptat.",

    "subject-new" => "Loonoy | Objednávka č. :order_no",
    "subject-sent" => "Loonoy | Objednávka č. :order_no byla odeslána",
    "subject-canceled" => "Loonoy | Objednávka č. :order_no byla stornována",

    "failed-order-title" => "Výzva k platbě objednávky na Loonoy.com",
    "failed-order-text" => "velice děkujeme za Vaši objednávku číslo :order_no ze dne :order_date, ale bohužel nám zatím nepřišla platba. Můžeme vám s dokončením nějak pomoct? Pokud nedojde k zaplacení objednávky, systém ji za tři pracovní dny automaticky stornuje.",
    "failed-order-payment-links" => "Pro platbu vybranou platební metodou prosím použijte následující odkaz:",
    "failed-order-payment" => "Pokud máte potíže s dokončením platby pomocí vybrané platební metody, můžete objednávku zaplatit bankovním převodem:",
    "failed-order-payment-data" => "Do reference platby prosím zadejte číslo objednávky:",
    "failed-order-recipient" => "Příjemce",
    "failed-order-thank-you" => "Děkujeme za platbu",

    "subject-failed" => "Loonoy | Neobdrželi jsme platbu objednávky č. :order_no",
];
