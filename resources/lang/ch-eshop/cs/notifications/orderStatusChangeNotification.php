<?php

return [
    'email' => [
        'subject' => config('app.name') . ' Stav objednávky (:orderNo) byl změněn',
        'line1' => 'Stav vaší objednávky (:orderNo) byl změněn na :status.',
    ],
];
