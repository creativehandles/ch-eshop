<?php

return [
    'email' => [
        'subject' => config('app.name') . ' Upozornění na novou objednávku (:orderNo)',
        'line1' => 'Právě jste obdrželi novou objednávku. Níže najdete její detaily.',

        'Order date' => 'Datum objednávky',
        'Order no' => 'Číslo objednávky',
        'Status' => 'Status',
        'Payment method' => 'Platební metoda',
        'Billing address' => 'Fakturační adresa',
        'Delivery address' => 'Doručovací adresa',
        'Company' => 'Firma',
        'Item code' => 'Kód položky',
        'Sub total' => 'Mezisoučet',
        'Discount' => 'Sleva',
        'Delivery price' => 'Poštovné',
        'Payment price' => 'Poplatek za platbu',
        'Tax' => 'Daň',
        'Total' => 'Celkem (:currency)',
    ],
];
