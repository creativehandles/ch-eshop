<?php

return [
    'email' => [
        'subject' => config('app.name') . ' Potvrzení objednávky (:orderNo)',
        'greeting' => 'Dobrý den, :name',
        'line1' => 'Právě jsme obdrželi Vaši objednávku. Pro jistotu si zkontrolujte správnost údajů, které Vám posíláme níže. <br>Děkujeme za Váš nákup!<br>',

        'Order date' => 'Datum objednávky',
        'Order no' => 'Číslo objednávky',
        'Status' => 'Status',
        'Payment method' => 'Platební metoda',
        'Billing address' => 'Fakturační adresa',
        'Delivery address' => 'Doručovací adresa',
        'Company' => 'Firma',
        'Item code' => 'Kód položky',
        'Sub total' => 'Mezisoučet',
        'Discount' => 'Sleva',
        'Delivery price' => 'Poštovné',
        'Payment price' => 'Poplatek za platbu',
        'Tax' => 'Daň',
        'Total' => 'Celkem (:currency)',
    ],
];
