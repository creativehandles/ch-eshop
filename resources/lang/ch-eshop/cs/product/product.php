<?php

return [
    'create' => [
        'created' => 'Produkt vytvořen',
        'failed' => 'Produkt se nepodařilo vytvořit',
        'page_title' => 'Nový produkt',
    ],
    'edit' => [
        'updated' => 'Produkt upraven',
        'failed' => 'Produkt se nepodařilo upravit',
        'page_title' => 'Upravit produkt',
    ],

    'create_variant' => 'Variantní údaje',
    'title' => 'Název',
    'code' => 'Kód',
    'meta_title' => 'Meta titulek',
    'short_description' => 'Krátký popis',
    'description' => 'Popis',
    'meta_description' => 'Meta popis',
    'images' => 'Obrázky',
    'language' => 'Jazyk překladu',
    
    'variant' => [
        'visibility' => 'Viditelnost',
        'type' => 'Typ',
        'code' => 'Kód',
        'ean' => 'EAN',
        'color' => 'Barva',
        'unit_price_wo_tax' => 'Nákupní cena bez DPH',
        'selling_price_wo_tax' => 'Prodejní cena bez DPH',
        'availability' => 'Availability',
        'qty_available' => 'Dostupné množství',
        'delivery_in_days' => 'Doba dodání',
        'unit_price_w_tax' => 'Nákupní cena s DPH',
        'selling_price_w_tax' => 'Prodejní cena s DPH',
        'tax' => 'DPH',
        'in_stock' => 'Skladem',
        'is_visible' => 'Viditelné',
        'option_types' => ' Parametry',
        'delete' => 'Smazat variantu',
        'create' => 'Nová varianta',
    ],
    'tab' => [
        'headings' => [
            'basic_info' => 'Základní informace',
            'variants' => 'Varianty',
            'options' => 'Možnosti',
            'detail_info' => 'Detailní informace',
            'media' => 'Media',
            'seo' => 'SEO',
        ]
    ],
    'validations' => [
        'options are invalid' => 'Možnosti jsou chybné'
    ]
];
