<?php

return [
    'order' => [
        'index' => [
            'header' => 'Objednávky',
            'link_name' => 'Objednávky'
        ],
        'create' => [
            'header' => 'Nová objednávka',
            'link_name' => ' Nová objednávka'
        ],
        'edit' => [
            'header' => 'Úprava objednávky',
            'link_name' => 'Úprava objednávky'

        ]
    ],

    'product' => [
        'index' => [
            'header' => 'Produkty',
            'link_name' => 'Produkty'
        ],
        'create' => [
            'header' => 'Nový produkt',
            'link_name' => ' Nový produkt'
        ],
        'edit' => [
            'header' => 'Úprava produktu',
            'link_name' => 'Úprava produktu'
        ]
    ],
];
