<?php

return [
    'Fetching parcel code returned status' => 'Pokus o získání kódu zásilky vrátil status - :status.',
    'Fetching parcel code returned error' => 'Při načítání kódu zásilky se vrátil kód chyby :errorCode a zpráva :errorText.',

    'Get label data of parcels returned status' => 'Pokus o získání dat štítků vrátil status - :status.',
    'Get label data of parcels returned empty label data.' => 'Pokus o získání dat štítků vrátil prázdná data.',
];
