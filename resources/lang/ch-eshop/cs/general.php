<?php

return [
    'parcel' => [
        'post' => 'Odeslat zásilky',
        'label_download' => 'Stáhnout štítky',
    ],
    "countries" => [
        "Czech Republic" => "Česká republika",
        "Germany" => "Německo",
        "Austria" => "Rakousko",
        "United Kingdom" => "Velká Británie",
        "United States" => "Amerika",
    ],
    'czech_post' => 'Česká pošta',
    'stripe' => 'Stripe',
    'paypal' => 'PayPal',
    'CIN' => 'IČ',
    'VAT' => 'DIČ',
];
