<?php

return [
    'updated' => 'Objednávka aktualizována',
    'failed' => 'Objednávku nebylo možné aktualizovat',
    'page_title' => 'Upravit objednávku',
    'total' => 'Celkem',
    // 'create_variant' => 'Create Order Variant',

    'choose' => 'Vybrat',
    'name' => 'Jméno',
    'surname' => 'Příjmení',
    'language' => 'Zvolte jazyk',
    'country' => 'Zvolte zemi',
    'currency' => 'Zvolte měnu',
    'email' => 'Email',
    'phone' => 'Telefon',
    'company' => 'Je firma',

    'invoice_details' => 'Fakturační údaje',
    'billing_address_name' => 'Jméno',
    'billing_address_surname' => 'Příjmení',
    'billing_address_street' => 'Ulice',
    'billing_address_zip' => 'PSČ',
    'billing_address_city' => 'Město',
    'billing_address_country' => 'Země',
    'billing_address_phone' => 'Telefon',

    'delivery_details' => 'Doručovací údaje',
    'delivery_address_name' => 'Jméno',
    'delivery_address_surname' => 'Příjmení',
    'delivery_address_street' => 'Ulice',
    'delivery_address_zip' => 'PSČ',
    'delivery_address_city' => 'Město',
    'delivery_address_country' => 'Země',
    'delivery_address_phone' => 'Telefon',

    'admin_note' => 'Interní poznámky',
    'client_note' => 'Poznámka k objednávce',

    'order_item' => 'Produkty',
    'add_order_item' => 'Přidat produkt',
    'delivery_cost_w_tax'=>'Cena za dopravu',
    'exclude_tax'=>'bez DPH',
];