<?php

return [
    'order_no' => ' Číslo objednávky',
    'order_status' => 'Stav objednávky',
    'order_status_default' => 'Stav objednávky',
    'order_total_cost' => 'Payment price',

    'created' => 'Objednávka vytvořena, přiřaďte produkty',
    'failed' => 'Objednávku se nepodařilo vytvořit',
    'page_title' => 'Nová objednávka',
    'deleted' => 'Objednávka smazána',
    // 'create_variant' => 'Create Order Variant',

    'choose' => 'Vybrat',
    'name' => 'Jméno',
    'surname' => 'Příjmení',
    'language' => 'Zvolte jazyk',
    'country' => 'Zvolte zemi',
    'currency' => 'Měna objednávky',
    'email' => 'Email',
    'phone' => 'Telefon',
    'company' => 'Firemní zákazník',
    'cin' => 'IČO',
    'vat_no' => 'DIČ',
    'company_name' => 'Název firmy',
    'type' => 'Dopravní metoda',
    'payment_method' => 'Platební metoda',
    'payment_price' => 'Cena platby bez DPH',
    'delivery_cost_wo_tax' => 'Cena dopravy bez DPH',
    'vat_rate' => 'CS VAT rate',
    'items_tax' => 'DPH',
    'items_price_wo_tax' => 'Zboží bez DPH',
    'total' => 'Cena celkem',
    'product_list' => 'Seznam produktů',
    'add_item' => 'Přidat produkt',

    'invoice_details' => 'Fakturační údaje',
    'billing_address_name' => 'Jméno',
    'billing_address_surname' => 'Příjmení',
    'billing_address_street' => 'Ulice',
    'billing_address_zip' => 'PSČ',
    'billing_address_city' => 'Město',
    'billing_address_country' => 'Země',
    'billing_address_phone' => 'Telefon',

    'delivery_details' => 'Doručovací údaje',
    'delivery_address_name' => 'Jméno',
    'delivery_address_surname' => 'Příjmení',
    'delivery_address_street' => 'Ulice',
    'delivery_address_zip' => 'PSČ',
    'delivery_address_city' => 'Město',
    'delivery_address_country' => 'Země',
    'delivery_address_phone' => 'Telefon',

    'admin_note' => 'Interní poznámky',
    'client_note' => 'Poznámka k objednávce',

    'tab' => [
        'headings' => [
            'order_details' => 'Přehled objednávky',
            'customer_details' => 'Údaje o zákazníkovi',
            'notes' => 'Interní poznámky',
            'order_items' => 'Seznam produktů'
        ]
    ],

    'Order unable to send for delivery' => 'Zásilku :order_no nebylo možné odeslat',
    'Order was successfully sent for delivery' => 'Objednávka :order_no byla úspěšně odeslána přepravci.',
    'Order delivery status updated' => 'Status zásilky aktualizován',
    'Order unable to send for delivery due to' => 'Objednávku :order_no nebylo možné odeslat z důvodu :errors',
    'status error' => 'Chyba objednávky :order_no | :errors',
    'No Action' => 'Žádná akce',
    'Label Created for order' => 'Štítek vytvořen pro objedávku :order_no',
    'Parcels are still being processed. Please try again in a while' => 'Zásilky se zpracovávají. Zkuste to prosím znovu za chvíli.',
    'Please select at least one order.' => 'Vyberte prosím alespoň jednu objednávku.',
    'Czech post ID is empty in the order' => 'ID České Pošty u objednávky :orderNo je prázdné.',
    'Cannot create labels PDF.' => 'Nepodařilo se vytvořit PDF štítek',

    'Available quantity of the variant :variant is less than ordered quantity.' => 'Dostupné množství varianty :variant je menší než objednané množství.',
    'A variant doesn\'t have price set for the locale :locale.' => 'Varianta nemá nastavenou cenu pro jazyk :locale.',

    'Sub total' => 'Mezisoučet',
    'Discount' => 'Sleva',
    'Delivery price' => 'Poštovné',
    'Payment price' => 'Poplatek za platbu',
    'Tax' => 'Daň',
    'Total' => 'Celkem (:currency)',
];
