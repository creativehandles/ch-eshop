<?php

return [
    'deliveryMethods' => [
        'czech_post' => 'Česká pošta',
    ],

    'paymentMethods' => [
        'stripe' => 'Stripe',
        'paypal' => 'PayPal',
    ],
];
