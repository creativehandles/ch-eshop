<?php

return [
    "dear-customer" => "Sehr geehrter Kunde",
    "order-number" => "Bestellnummer",
    "order-date" => "Bestelldatum",
    "order-items" => "Inhalt des Auftrags",
    "delivery-details" => "Einzelheiten zur Lieferung",
    "invoice-details" => "Informationen zur Rechnungsstellung",
    "email" => "Email",
    "phone" => "Telefon",
    "thank-you-title" => "Wir danken Ihnen für Ihre Bestellung und wünschen Ihnen einen schönen Tag.",
    "have-a-nice-day" => "Einen schönen Tag noch!",
    "loonoy-team" => "Ihr Loonoy-Team",

    "new-order-title" => "Ihre Bestellung bei Loonoy.com",
    "new-order-text" => "Wir haben Ihre Bestellung sicher erhalten und sind dabei, die Zahlung zu bearbeiten. Wenn die Zahlung erfolgreich war, werden Sie in Kürze eine Bestätigung erhalten. Sollten Sie Probleme mit dem Abschluss der Zahlung haben, lassen Sie es uns bitte wissen, indem Sie auf diese E-Mail antworten, und wir werden Ihnen helfen.",

    "sent-order-title" => "Ihre Bestellung bei Loonoy.com wurde versandt",
    "sent-order-text" => "Ihre Bestellung :order_no wurde heute per Post verschickt, Sie können die Sendung unter diesem Link verfolgen: <a href=':tracking_link'>:tracking_link</a>.",

    "canceled-order-title" => "Ihre Bestellung bei Loonoy.com wurde storniert",
    "canceled-order-text" => "Wir möchten Ihnen nur mitteilen, dass Ihre Bestellung :order_no soeben storniert wurde. Wenn Sie keine Stornierung beantragt haben oder nicht wissen, warum dies geschehen ist, zögern Sie bitte nicht, uns zu fragen.",

    "subject-new" => "Loonoy | Bestellnr. :order_no",
    "subject-sent" => "Loonoy | Bestellung :order_no wurde gesendet",
    "subject-canceled" => "Loonoy | Bestellung :order_no wurde storniert",

    "failed-order-title" => "Einladung zur Bezahlung Ihrer Bestellung bei Loonoy.com",
    "failed-order-text" => "Vielen Dank für Ihre Bestellung :order_no von :order_date, aber leider haben wir noch keine Zahlung erhalten. Wenn Ihre Bestellung nicht bezahlt wird, wird sie nach drei Arbeitstagen automatisch storniert.",
    "failed-order-payment-links" => "Bitte benutzen Sie den folgenden Link, um mit der von Ihnen gewählten Zahlungsmethode zu bezahlen:",
    "failed-order-payment" => "Wenn Sie Schwierigkeiten haben, die Zahlung mit der von Ihnen gewählten Zahlungsmethode abzuschließen, können Sie Ihre Bestellung auch per Banküberweisung bezahlen:",
    "failed-order-payment-data" => "PBitte geben Sie Ihre Bestellnummer in der Zahlungsreferenz ein:",
    "failed-order-recipient" => "Begünstigter",
    "failed-order-thank-you" => "Vielen Dank für Ihre Zahlung",

    "subject-failed" => "Loonoy | Wir haben keine Zahlung für die Bestellung :order_no erhalten",
];
