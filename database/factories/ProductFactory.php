<?php

namespace Database\Factories;

use Creativehandles\ChEshop\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->paragraph(1),
            'meta_title' => $this->faker->paragraph(1),
            'short_description' => $this->faker->paragraph(1),
            'description' => $this->faker->paragraph(1),
            'meta_description' => $this->faker->paragraph(1),
            'url' => $this->faker->url,
            'code' => $this->faker->randomNumber(5)
        ];
    }
}
