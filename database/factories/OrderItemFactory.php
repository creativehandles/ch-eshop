<?php

namespace Database\Factories;

use Creativehandles\ChEshop\Models\Order;
use Creativehandles\ChEshop\Models\OrderItem;
use Creativehandles\ChEshop\Models\Status;
use Creativehandles\ChEshop\Models\Variant;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderItemFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = OrderItem::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $variant = Variant::factory()->create();
        return [
            'ch_eshop_order_id'=>Order::factory()->create()->id,
            'ch_eshop_variant_id'=>$variant->id,
            'code'=>$this->faker->uuid(),
            'ean'=>$this->faker->ean8(),
            'quantity'=>$this->faker->randomNumber(2),
            'discount_w_tax'=>$this->faker->randomNumber(2),
            'discount_wo_tax'=>$this->faker->randomNumber(2),
            'discount_wo_tax'=>$this->faker->randomNumber(2),
            'unit_price_w_tax'=>$this->faker->randomNumber(2),
            'unit_price_wo_tax'=>$this->faker->randomNumber(2),
            'selling_price_w_tax'=>$this->faker->randomNumber(2),
            'selling_price_wo_tax'=>$this->faker->randomNumber(2),
            'total_price_w_tax'=>$this->faker->randomNumber(2),
            'total_price_wo_tax'=>$this->faker->randomNumber(2),
            'tax'=>$this->faker->randomNumber(2),
            'object'=>json_encode($variant->product->with(['ch_eshop_attachments', 'variants'])->get()->toArray()),
        ];
    }
}
