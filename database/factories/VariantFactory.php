<?php

namespace Database\Factories;

use Creativehandles\ChEshop\Models\Product;
use Creativehandles\ChEshop\Models\Variant;
use Illuminate\Database\Eloquent\Factories\Factory;

class VariantFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Variant::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'type' => $this->faker->randomElement(['Large', 'Small', 'Pro', 'Smart', 'Light']),
            'ch_eshop_product_id' => Product::factory()->create()->id,
            'unit_price_wo_tax' => $this->faker->randomNumber(3),
            'selling_price_wo_tax' => $this->faker->randomNumber(3),
            'in_stock' => $this->faker->boolean(),
            'is_visible' => $this->faker->boolean(),
            'code' => $this->faker->randomNumber(3),
            'ean' => $this->faker->randomNumber(3),
            'qty_available' => $this->faker->randomNumber(2)
        ];
    }
}
