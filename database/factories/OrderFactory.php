<?php

namespace Database\Factories;

use Creativehandles\ChEshop\Models\Order;
use Creativehandles\ChEshop\Models\Status;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Order::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'ch_eshop_status_id' => $this->faker->randomElement([
                Status::NEW_ORDER,
                Status::IN_PROGRESS,
                Status::SENT,
                Status::DELIVERED,
                Status::CANCELED,
                Status::COMPLAINT,
                Status::PAID
            ]),
            'order_no' => $this->faker->ean8(),
            'invoice' => $this->faker->url(),
            'language' => $this->faker->randomElement(['en', 'cs']),
            'currency' => $this->faker->randomElement(['USD', 'CZK']),
            'country' => $this->faker->randomElement(['USA', 'Czech Republic']),
            'email' => $this->faker->email(),
            'phone' => $this->faker->e164PhoneNumber(),
            'company' => $this->faker->company(),
            'is_company' => $this->faker->boolean(),
            'delivery_type' => $this->faker->randomElement(['cash on delivery', 'credit', 'cash']),
            'payment_method' => $this->faker->randomElement(['paypal', 'stripe', 'gopay']),
            'delivery_cost_wo_tax' => $this->faker->randomNumber(2),
            'delivery_cost_w_tax' => $this->faker->randomNumber(2),
            'total_cost_wo_tax' => $this->faker->randomNumber(2),
            'total_cost_w_tax' => $this->faker->randomNumber(2),
            'total_tax' => $this->faker->randomNumber(2),
            'tax_rate' => $this->faker->randomNumber(2),
            'billing_address_name' => $this->faker->name(),
            'billing_address_surname' => $this->faker->name(),
            'billing_address_street' => $this->faker->streetName(),
            'billing_address_zip' => $this->faker->postcode(),
            'billing_address_city' => $this->faker->city(),
            'billing_address_country' => $this->faker->country(),
            'billing_address_phone' => $this->faker->e164PhoneNumber(),
            'delivery_address_name' => $this->faker->name(),
            'delivery_address_surname' => $this->faker->name(),
            'delivery_address_street' => $this->faker->streetName(),
            'delivery_address_zip' => $this->faker->postcode(),
            'delivery_address_city' => $this->faker->city(),
            'delivery_address_country' => $this->faker->country(),
            'delivery_address_phone' => $this->faker->e164PhoneNumber(),
            'client_note' => $this->faker->paragraph(1),
            'admin_note' => $this->faker->paragraph(1),
        ];
    }
}
