<?php

namespace Database\Factories;

use Creativehandles\ChEshop\Models\Status;
use Illuminate\Database\Eloquent\Factories\Factory;

class StatusFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Status::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return $this->faker->randomElement([
            [
                'title' => 'Pending',
                'description' => 'When Order not confirmed',
                'color' => '#488548',
                'on_change_notify_type' => 'email',
                'notification' => '/email/path',
            ],
            [
                'title' => 'Canceled',
                'description' => 'Order canceled by admin',
                'color' => '#ff5871',
                'on_change_notify_type' => 'email',
                'notification' => '/email/path',
            ],
            [
                'title' => 'Completed',
                'description' => 'Order completely Finished',
                'color' => '#ff58cc',
                'on_change_notify_type' => 'email',
                'notification' => '/email/path',
            ]
        ]);
    }
}
