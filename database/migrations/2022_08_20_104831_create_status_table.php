<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatusTable extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('ch_eshop_statuses')) {
            Schema::create('ch_eshop_statuses',function(Blueprint $table){
                $table->id();
                $table->longText('title')->nullable();
                $table->longText('description')->nullable();
                $table->string('color')->default('#FF5733');
                $table->string('on_change_notify_type')->default('email');
                $table->longText('notification')->default('State Changed');
                $table->timestamps();
            });
        }
    }    
    
    public function down()
    {
        Schema::dropIfExists('ch_eshop_statuses');
    }
}
