<?php

use Creativehandles\ChEshop\Models\Status;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PopulateStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $json = file_get_contents(__DIR__.'/../../storage/database/seeds/orderStatuses.json');
        $statuses = json_decode($json, true);

        // TODO: improve the seeding thus truncating is not required.
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Status::truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        foreach ($statuses as $status) {
            Status::updateOrCreate(
                ['title' => $status['title']],
                $status
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
