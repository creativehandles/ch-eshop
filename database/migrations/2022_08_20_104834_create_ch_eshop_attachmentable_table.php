<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChEshopAttachmentableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('ch_eshop_attachmentables')) {
        Schema::create('ch_eshop_attachmentables', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('ch_eshop_attachment_id');
            $table->unsignedBigInteger('ch_eshop_attachmentable_id');
            $table->string('ch_eshop_attachmentable_type');
            $table->timestamps();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ch_eshop_attachmentables');
    }
}
