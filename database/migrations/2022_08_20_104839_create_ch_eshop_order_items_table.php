<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChEshopOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('ch_eshop_order_items')) {
        Schema::create('ch_eshop_order_items', function (Blueprint $table) {
            $table->id();
            $table->foreignId('ch_eshop_order_id')->constrained();
            $table->foreignId('ch_eshop_variant_id')->constrained();
            $table->string('code')->nullable();
            $table->string('ean')->nullable();
            $table->double('quantity',15,2)->nullable()->default(0.0);
            $table->double('discount_w_tax',15,2)->nullable()->default(0.0);
            $table->double('discount_wo_tax',15,2)->nullable()->default(0.0);
            $table->double('unit_price_w_tax',15,2)->nullable()->default(0.0);
            $table->double('unit_price_wo_tax',15,2)->nullable()->default(0.0);
            $table->double('selling_price_w_tax',15,2)->nullable()->default(0.0);
            $table->double('selling_price_wo_tax',15,2)->nullable()->default(0.0);
            $table->double('total_price_w_tax',15,2)->nullable()->default(0.0);
            $table->double('total_price_wo_tax',15,2)->nullable()->default(0.0);
            $table->double('tax',15,2)->nullable()->default(0.0);
            $table->json('object')->nullable();
            $table->softDeletes();
            $table->timestamps();
		
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ch_eshop_order_items');
    }
}
