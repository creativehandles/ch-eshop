<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFlexibeeOrderReferenceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flexibee_order_references', function (Blueprint $table) {
            $table->id();
            $table->integer("order_id")->unique();
            $table->integer("flexibee_id")->nullable()->default(NULL);
            $table->boolean("status")->default(false);
            $table->text("document_url")->nullable()->default(NULL);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flexibee_order_references');
    }
}
