<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChEshopOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('ch_eshop_orders')) {
        Schema::create('ch_eshop_orders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('ch_eshop_status_id')->constrained();
            $table->string('order_no')->nullable();
            $table->longText('invoice')->nullable();
            $table->string('language')->nullable();
            $table->string('currency')->nullable();
            $table->string('country')->nullable();
            $table->boolean('is_company')->default(false);
            $table->string('company')->nullable();
            $table->string('cin')->nullable();
            $table->string('vat_no')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('delivery_type')->nullable(); // cash on delivery, cash, credit
            $table->string('payment_method')->nullable(); //stripe,cash,..
            $table->double('payment_price',15,2)->nullable()->default(0.0); // mostly card fee like 5USD
            $table->double('delivery_cost_wo_tax',15,2)->nullable()->default(0.0);
            $table->double('delivery_cost_w_tax',15,2)->nullable()->default(0.0);
            $table->double('total_cost_wo_tax',15,2)->nullable()->default(0.0);
            $table->double('total_cost_w_tax',15,2)->nullable()->default(0.0);
            $table->double('total_tax',15,2)->nullable()->default(0.0);
            $table->double('tax_rate',15,2)->nullable()->default(0.0);
            $table->double('total_pay',15,2)->nullable()->default(0.0);
            
            $table->string('billing_address_name')->nullable();
            $table->string('billing_address_surname')->nullable();
            $table->string('billing_address_street')->nullable();
            $table->string('billing_address_zip')->nullable();
            $table->string('billing_address_city')->nullable();
            $table->string('billing_address_country')->nullable();
            $table->string('billing_address_phone')->nullable();
            
            $table->string('delivery_address_name')->nullable();
            $table->string('delivery_address_surname')->nullable();
            $table->string('delivery_address_street')->nullable();
            $table->string('delivery_address_zip')->nullable();
            $table->string('delivery_address_city')->nullable();
            $table->string('delivery_address_country')->nullable();
            $table->string('delivery_address_phone')->nullable();

            $table->string('client_note')->nullable();
            $table->string('admin_note')->nullable();
            
            $table->string('czech_post_id')->nullable();
            $table->string('printing_data')->nullable();
            $table->longText('delivery_data')->nullable();
            $table->longText('payment_object')->nullable();
            
            $table->softDeletes();
            $table->timestamps();
		
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ch_eshop_orders');
    }
}
