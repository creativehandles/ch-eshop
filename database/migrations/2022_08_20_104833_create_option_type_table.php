<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOptionTypeTable extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('ch_eshop_option_types')) {
        Schema::create('ch_eshop_option_types',function(Blueprint $table){
            $table->id();
            $table->string('name');
            $table->string('unit')->nullable();
            $table->timestamps();
        });
        }
    }    
    
    public function down()
    {
        Schema::dropIfExists('ch_eshop_option_types');
    }
}
