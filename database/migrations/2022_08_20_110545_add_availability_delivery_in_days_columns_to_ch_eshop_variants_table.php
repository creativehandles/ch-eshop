<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAvailabilityDeliveryInDaysColumnsToChEshopVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ch_eshop_variants', function (Blueprint $table) {
            $table->text('availability')->nullable()->default(null)->after('qty_available');
            $table->unsignedInteger('delivery_in_days')->nullable()->default(null)->after('availability');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ch_eshop_variants', function (Blueprint $table) {
            $table->dropColumn(['availability', 'delivery_in_days']);
        });
    }
}
