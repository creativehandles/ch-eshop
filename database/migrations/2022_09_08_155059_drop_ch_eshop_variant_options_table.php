<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropChEshopVariantOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('ch_eshop_variant_options');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (! Schema::hasTable('ch_eshop_variant_options')) {
            Schema::create('ch_eshop_variant_options', function (Blueprint $table) {
                $table->id();
                $table->foreignId('ch_eshop_variant_id')->constrained();
                $table->foreignId('ch_eshop_option_type_id')->constrained();
                $table->string('value')->nullable();
                $table->timestamps();
            });
        }
    }
}
