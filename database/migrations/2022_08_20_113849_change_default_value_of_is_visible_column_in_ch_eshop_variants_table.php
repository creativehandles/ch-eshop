<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeDefaultValueOfIsVisibleColumnInChEshopVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ch_eshop_variants', function (Blueprint $table) {
            $table->boolean('is_visible')->default(false)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ch_eshop_variants', function (Blueprint $table) {
            $table->boolean('is_visible')->default(true)->change();
        });
    }
}
