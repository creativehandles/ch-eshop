<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChEshopProductOptionValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ch_eshop_product_option_values', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('ch_eshop_product_id')->nullable()->default(null);
            $table->unsignedBigInteger('ch_eshop_product_option_id')->nullable()->default(null);
            $table->longText('value')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ch_eshop_product_option_values');
    }
}
