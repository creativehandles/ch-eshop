<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPaymentPriceWTaxColumnToChEshopOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ch_eshop_orders', function (Blueprint $table) {
            $table->double('payment_price_w_tax',15,2)->nullable()->default(0.0)->after('payment_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ch_eshop_orders', function (Blueprint $table) {
            $table->dropColumn('payment_price_w_tax');
        });
    }
}
