<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColorColumnToChEshopVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ch_eshop_variants', function (Blueprint $table) {
            $table->longText('color')->nullable()->default(null)->after('ean');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ch_eshop_variants', function (Blueprint $table) {
            $table->dropColumn('color');
        });
    }
}
