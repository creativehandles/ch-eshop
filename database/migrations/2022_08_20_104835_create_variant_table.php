<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVariantTable extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('ch_eshop_variants')) {
        Schema::create('ch_eshop_variants',function(Blueprint $table){
            $table->id();
            $table->foreignId('ch_eshop_product_id')->constrained();
            $table->text('type')->nullable();
            $table->longText('unit_price_wo_tax',15,2)->default(0);
            $table->longText('unit_price_w_tax',15,2)->default(0);
            $table->longText('selling_price_wo_tax',15,2)->default(0);
            $table->longText('selling_price_w_tax',15,2)->default(0);
            $table->boolean('in_stock')->default(true);
            $table->boolean('is_visible')->default(true);
            $table->text('code')->nullable();
            $table->text('ean')->nullable();
            $table->double('qty_available',15,2)->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
        }
    }    
    
    public function down()
    {
        Schema::dropIfExists('ch_eshop_variants');
    }
}