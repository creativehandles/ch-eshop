<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameChEshopOptionTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('ch_eshop_option_types')) {
            Schema::rename('ch_eshop_option_types', 'ch_eshop_product_options');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('ch_eshop_product_options')) {
            Schema::rename('ch_eshop_product_options', 'ch_eshop_option_types');
        }
    }
}
