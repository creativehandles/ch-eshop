<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTable extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('ch_eshop_products')) {
        Schema::create('ch_eshop_products',function(Blueprint $table){
            $table->id();
            $table->longText('title')->nullable();
            $table->longText('meta_title')->nullable();
            $table->longText('short_description')->nullable();
            $table->longText('description')->nullable();
            $table->longText('meta_description')->nullable();
            $table->longText('url')->nullable();
            $table->string('code')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
        }
    }    
    
    public function down()
    {
        Schema::dropIfExists('ch_eshop_products');
    }
}
