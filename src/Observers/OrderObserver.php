<?php

namespace Creativehandles\ChEshop\Observers;

use Creativehandles\ChEshop\Helpers\RandomID;
use Creativehandles\ChEshop\Models\Order;

class OrderObserver
{
    /**
     * Handle the Order "created" event.
     *
     * @param  \App\Models\Creativehandles\ChEshop\Models\Order  $order
     * @return void
     */
    public function created(Order $order)
    {
        $order->update([
            'order_no' => RandomID::unique(config('ch-eshop.invoice_prefix'))
        ]);
    }

    /**
     * Handle the Order "updated" event.
     *
     * @param  \App\Models\Creativehandles\ChEshop\Models\Order  $order
     * @return void
     */
    public function updated(Order $order)
    {
        //
    }

    /**
     * Handle the Order "deleted" event.
     *
     * @param  \App\Models\Creativehandles\ChEshop\Models\Order  $order
     * @return void
     */
    public function deleted(Order $order)
    {
        //
    }

    /**
     * Handle the Order "restored" event.
     *
     * @param  \App\Models\Creativehandles\ChEshop\Models\Order  $order
     * @return void
     */
    public function restored(Order $order)
    {
        //
    }

    /**
     * Handle the Order "force deleted" event.
     *
     * @param  \App\Models\Creativehandles\ChEshop\Models\Order  $order
     * @return void
     */
    public function forceDeleted(Order $order)
    {
        //
    }
}
