<?php

namespace Creativehandles\ChEshop\Repositories;

use Creativehandles\ChEshop\Http\Resources\ProductResource;
use App\Repositories\BaseEloquentRepository;
use Creativehandles\ChEshop\Models\ChEshopAttachment;
use Creativehandles\ChEshop\Models\Product;
use Creativehandles\ChEshop\Models\ProductOption;
use Creativehandles\ChEshop\Models\Variant;
use Creativehandles\ChEshop\Services\LocalesService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Throwable;

class ProductRepository extends BaseEloquentRepository
{
    const RELATIONS = ['optionValues.option', 'variants.attachments'];

    const IMAGE_ATTACHMENT = 'image';
    const DOCUMENT_ATTACHMENT = 'document';
    const UNKNOWN_ATTACHMENT = 'unknown';

    const PRODUCT_OPTION_TYPE_BOOLEAN = 'boolean';

    private LocalesService $localesService;

    public function __construct(LocalesService $localesService)
    {
        parent::__construct();

        $this->localesService = $localesService;
    }

    public function getModel()
    {
        return new Product();
    }

    public function getResource()
    {
        return ProductResource::class;
    }

    public function allVariant()
    {
        return Variant::all();
    }

    public function getAllVariantOrderItemData()
    {
        return Variant::with(['product'])->get();
    }

    public function updateDetails($data, Product $product)
    {
        // extract the options
        $productOptions = Arr::pull($data, 'options');

        // update product
        $product->update($data['product']);

        // set product options

        if (is_array($productOptions)) {
            foreach ($productOptions as $optionId => $value) {
                $product->optionValues()->updateOrCreate(
                    [
                        'ch_eshop_product_id' => $product->id,
                        'ch_eshop_product_option_id' => $optionId,
                    ],
                    [
                        'ch_eshop_product_id' => $product->id,
                        'ch_eshop_product_option_id' => $optionId,
                        'value' => $value
                    ]
                );
            }
        }

        // create new variant and its options
        if (isset($data['variant']['type'])) {
            $this->makeVariant($product, $data['variant']);
        }

        // update variant details
        foreach ($product->variants as $variant) {
            if (isset($data['variant_update'][$variant->id])) {
                $variantData = $data['variant_update'][$variant->id];
                // pull out attachments
                $attachments = Arr::pull($variantData, 'attachments');
                // calculate tax inclusive prices
                $locale = app()->getLocale();
                $variantData['is_visible'] = isset($variantData['is_visible']) ? $variantData['is_visible'] : false;
                $variantData['unit_price_w_tax'] = $this->localesService->getTaxInclusivePrice($locale, $variantData['unit_price_wo_tax']);
                $variantData['selling_price_w_tax'] = $this->localesService->getTaxInclusivePrice($locale, $variantData['selling_price_wo_tax']);

                $variant->update($variantData);
                $this->saveAttachments($variant, $attachments);
            }
        }

        return $product->refresh();
    }

    /**
     * Make a product
     *
     * @param array $data
     * @return Product
     */
    public function make(array $data)
    {
        // extract the options
        $productOptions = Arr::pull($data, 'options');

        // create product
        $product = $this->model::create($data['product']);

        // set product options
        if (is_array($productOptions)) {
            foreach ($productOptions as $optionId => $value) {
                $product->optionValues()->create([
                    'ch_eshop_product_option_id' => $optionId,
                    'value' => $value
                ]);
            }
        }

        // make product variant
        $this->makeVariant($product, $data['variant']);

        return $product;
    }

    public function makeVariant(Product $product, array $data)
    {
        // pull out attachments
        $attachments = Arr::pull($data, 'attachments');
        // calculate tax inclusive prices
        $locale = app()->getLocale();
        $data['is_visible'] = isset($data['is_visible']) ? $data['is_visible'] : false;
        $data['unit_price_w_tax'] = $this->localesService->getTaxInclusivePrice($locale, $data['unit_price_wo_tax']);
        $data['selling_price_w_tax'] = $this->localesService->getTaxInclusivePrice($locale, $data['selling_price_wo_tax']);
        $variant = $product->variants()->create($data);

        // save attachments
        if ($attachments) {
            $this->saveAttachments($variant, $attachments);
        }

        return $variant;
    }

    public function getVariant($search, $field = 'id')
    {
        return Variant::where($field, $search)->get();
    }

    public function findVariants($search, $field = 'id')
    {
        return Variant::where($field, $search)->get();
    }

    public function saveAttachments(Model $model, $files)
    {
        if (is_array($files)) {
            foreach ($files as $file) {
                $this->saveAttachments($model, $file);
            }
        }

        if ($files instanceof UploadedFile) {
            $file = $files;
            $filename = $file->getClientOriginalName();
            $path = self::getAttachmentPath($model);
            $filePath = $file->storeAs($path, $filename, config('ch-eshop.files.disk'));
            $type = $this->getAttachmentType($file->getClientOriginalExtension());

            $model->attachments()->create([
                'type' => $type,
                'value' => $filePath
            ]);
        }
    }

    public function getAttachmentType($extension): string
    {
        switch ($extension) {
            case 'jpeg' || 'png' || 'jpg':
                return self::IMAGE_ATTACHMENT;
                break;

            case 'doc' || 'text' || 'pdf':
                return self::DOCUMENT_ATTACHMENT;
                break;

            default:
                return self::UNKNOWN_ATTACHMENT;
                break;
        }
    }

    public static function getAttachmentPath(Model $model)
    {
        if ($model instanceof Variant) {
            return sprintf(
                'public/%s/products/%d/variants/%s/attachments',
                config('ch-eshop.files.storagePath'),
                $model->product->id,
                $model->id
            );
        }

        if ($model instanceof Product) {
            return sprintf(
                'public/%s/products/%d/attachments',
                config('ch-eshop.files.storagePath'),
                $model->product->id
            );
        }

        return sprintf(
            'public/%s/attachments',
            config('ch-eshop.files.storagePath')
        );
    }

    public function deleteVariant($variant)
    {
        try {
            if ($variant instanceof Variant) {
                $variant->delete();
            } else {
                Variant::find($variant)->delete();
            }
            return true;
        } catch (Throwable $th) {
            Log::error($th);
            return false;
        }
    }

    public function deleteProduct($product)
    {
        try {
            if ($product instanceof Product) {
                $product->delete();
            } else {
                $this->model::find($product)->delete();
            }
            return true;
        } catch (Throwable $th) {
            throw $th;
        }
    }

    public function removeAttachment(ChEshopAttachment $attachment, Model $parent)
    {
        $parent->attachments($attachment)->detach();
        $attachment->delete();
    }

    public function getAllProductOptions()
    {
        return ProductOption::all();
    }
}
