<?php

namespace Creativehandles\ChEshop\Repositories;

use Creativehandles\ChEshop\Http\Resources\ProductOptionResource;
use App\Repositories\BaseEloquentRepository;
use Creativehandles\ChEshop\Models\ProductOption;

class ProductOptionsRepository extends BaseEloquentRepository
{
    public function getModel()
    {
        return new ProductOption();
    }

    public function getResource()
    {
        return ProductOptionResource::class;
    }
}
