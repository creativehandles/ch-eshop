<?php

namespace Creativehandles\ChEshop\Repositories;

use Creativehandles\ChEshop\Models\FlexibeeOrderReference;
use App\Repositories\BaseEloquentRepository;

class FlexibeeOrderReferenceRepository extends BaseEloquentRepository
{


    public function getModel()
    {
        return new FlexibeeOrderReference();
    }

   
}
