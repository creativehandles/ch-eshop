<?php

namespace Creativehandles\ChEshop\Repositories;

use App\Repositories\BaseEloquentRepository;
use Creativehandles\ChEshop\Http\Resources\OrderResource;
use Creativehandles\ChEshop\Models\Order;

class OrderRepository extends BaseEloquentRepository
{

    public function getModel()
    {
        return new Order();
    }

    public function getResource()
    {
        return OrderResource::class;
    }
}
