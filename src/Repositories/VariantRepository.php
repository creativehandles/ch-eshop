<?php

namespace Creativehandles\ChEshop\Repositories;

use App\Repositories\BaseEloquentRepository;
use Creativehandles\ChEshop\Http\Resources\VariantResource;
use Creativehandles\ChEshop\Models\Variant;

class VariantRepository extends BaseEloquentRepository
{

    public function getModel()
    {
        return new Variant();
    }

    public function getResource()
    {
        return VariantResource::class;
    }
}
