<?php

namespace Creativehandles\ChEshop\Repositories;

use App\Repositories\BaseEloquentRepository;
use Creativehandles\ChEshop\Http\Resources\StatusResource;
use Creativehandles\ChEshop\Models\Status;

class StatusRepository extends BaseEloquentRepository
{

    public function getModel()
    {
        return new Status();
    }

    public function getResource()
    {
        return StatusResource::class;
    }
}
