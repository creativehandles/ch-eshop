<?php

namespace Creativehandles\ChEshop\Helpers;

class RandomID
{
    public static function unique($prefix=null)
    {
        return uniqid($prefix);
    }
}
