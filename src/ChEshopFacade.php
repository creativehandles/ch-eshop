<?php

namespace Creativehandles\ChEshop;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Creativehandles\ChEshop\Skeleton\SkeletonClass
 */
class ChEshopFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'ch-eshop';
    }
}
