<?php

namespace Creativehandles\ChEshop\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class ProductOptionValue extends Model
{
    use HasTranslations;
    
    protected $table = 'ch_eshop_product_option_values';
    
    protected $fillable = [
        'ch_eshop_product_id',
        'ch_eshop_product_option_id',
        'value'
    ];

    public $translatable = ['value'];
    
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
    
    public function option()
    {
        return $this->belongsTo(ProductOption::class, 'ch_eshop_product_option_id');
    }
}
