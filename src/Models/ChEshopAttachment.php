<?php

namespace Creativehandles\ChEshop\Models;

use Illuminate\Database\Eloquent\Model;

class ChEshopAttachment extends Model
{
    protected $fillable = [
        'type',
        'value'
    ];
    
    public function variants()
    {
        return $this->morphedByMany(Variant::class, 'ch_eshop_attachmentable');
    }
}
