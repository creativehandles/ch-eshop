<?php

namespace Creativehandles\ChEshop\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class ProductOption extends Model
{
    use HasTranslations;
    
    protected $table = 'ch_eshop_product_options';
    
    protected $fillable = [
        'name',
        'unit',
        'type'
    ];
    
    public $translatable = ['name'];
    
    public function productOptionValues()
    {
        return $this->hasMany(ProductOptionValue::class);
    }
}
