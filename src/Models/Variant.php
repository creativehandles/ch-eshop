<?php

namespace Creativehandles\ChEshop\Models;

use Database\Factories\VariantFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class Variant extends Model
{
    use HasTranslations, HasFactory, SoftDeletes;

    protected $table = 'ch_eshop_variants';
    protected $fillable = [
        'type',
        'ch_eshop_product_id',
        'unit_price_wo_tax',
        'unit_price_w_tax',
        'selling_price_wo_tax',
        'selling_price_w_tax',
        'in_stock',
        'is_visible',
        'code',
        'ean',
        'color',
        'qty_available',
        'availability',
        'delivery_in_days'
    ];
    public $translatable = [
        'type',
        'color',
        'unit_price_wo_tax',
        'unit_price_w_tax',
        'selling_price_wo_tax',
        'selling_price_w_tax',
        'availability'
    ];

    protected static function newFactory()
    {
        return VariantFactory::new();
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'ch_eshop_product_id');
    }

    public function attachments()
    {
        return $this->morphToMany(ChEshopAttachment::class, 'ch_eshop_attachmentable');
    }
}
