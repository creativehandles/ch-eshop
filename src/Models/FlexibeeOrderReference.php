<?php

namespace Creativehandles\ChEshop\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FlexibeeOrderReference extends Model
{
    use HasFactory;
    protected $guarded = [];
}
