<?php

namespace Creativehandles\ChEshop\Models;

use Database\Factories\OrderItemFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderItem extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'ch_eshop_order_items';

    protected $fillable = [
        'ch_eshop_order_id',
        'ch_eshop_variant_id',
        'code',
        'ean',
        'quantity',
        'discount_w_tax',
        'discount_wo_tax',
        'unit_price_w_tax',
        'unit_price_wo_tax',
        'selling_price_w_tax',
        'selling_price_wo_tax',
        'total_price_w_tax',
        'total_price_wo_tax',
        'tax',
        'object',
        'flexibee_id',
    ];

    protected static function newFactory()
    {
        return OrderItemFactory::new();
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'ch_eshop_order_id');
    }
    
    public function variant()
    {
        return $this->belongsTo(Variant::class, 'ch_eshop_variant_id');
    }
}
