<?php

namespace Creativehandles\ChEshop\Models;

use Database\Factories\ProductFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class Product extends Model
{
    use HasTranslations, HasFactory, SoftDeletes;

    protected $table = 'ch_eshop_products';
    
    public $translatable = [
        'title',
        'meta_title',
        'short_description',
        'description',
        'meta_description',
        'url'
    ];

    protected $fillable = [
        'title',
        'meta_title',
        'short_description',
        'description',
        'meta_description',
        'url',
        'code'
    ];

    protected static function newFactory()
    {
        return ProductFactory::new();
    }

    public function optionValues()
    {
        return $this->hasMany(ProductOptionValue::class, 'ch_eshop_product_id', 'id');
    }
    
    public function variants()
    {
        return $this->hasMany(Variant::class, 'ch_eshop_product_id', 'id');
    }
}
