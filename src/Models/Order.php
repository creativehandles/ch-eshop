<?php

namespace Creativehandles\ChEshop\Models;

use Creativehandles\ChEshop\Helpers\RandomID;
use Database\Factories\OrderFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class Order extends Model
{
    use HasFactory, SoftDeletes, HasTranslations;

    protected $table = 'ch_eshop_orders';

    protected $fillable = [
        'ch_eshop_status_id',
        'order_no',
        'invoice',
        'language',
        'currency',
        'email',
        'phone',
        'company',
        'cin',
        'vat_no',
        'country',
        'is_company',
        'delivery_type',
        'payment_method',
        'payment_price',
        'payment_price_w_tax',
        'delivery_cost_wo_tax',
        'delivery_cost_w_tax',
        'total_cost_wo_tax',
        'total_cost_w_tax',
        'total_tax',
        'tax_rate',
        'billing_address_name',
        'billing_address_surname',
        'billing_address_street',
        'billing_address_zip',
        'billing_address_city',
        'billing_address_country',
        'billing_address_phone',
        'delivery_address_name',
        'delivery_address_surname',
        'delivery_address_street',
        'delivery_address_zip',
        'delivery_address_city',
        'delivery_address_country',
        'delivery_address_phone',
        'client_note',
        'admin_note',
        'total_pay',
        'payment_object',
        'printing_data',
        'delivery_data',
        'czech_post_id',
    ];

    protected $translatable = ['delivery_data'];

    protected static function newFactory()
    {
        return OrderFactory::new();
    }

    public function status()
    {
        return $this->belongsTo(Status::class, 'ch_eshop_status_id');
    }

    public function items()
    {
        return $this->hasMany(OrderItem::class, 'ch_eshop_order_id');
    }

    public function getBillingAddressFullNameAttribute()
    {
        return trim($this->billing_address_name . ' ' . $this->billing_address_surname);
    }

    public function getHtmlBillingAddressAttribute()
    {
        $billingAddress = trim($this->billing_address_name . ' ' . $this->billing_address_surname);

        if (!empty($this->billing_address_street)) {
            $billingAddress .= ',<br>' . $this->billing_address_street;
        }

        if (!empty($this->billing_address_city)) {
            $billingAddress .= ',<br>' . $this->billing_address_city;
        }

        if (!empty($this->billing_address_zip)) {
            $billingAddress .= ', ' . $this->billing_address_zip;
        }

        if (!empty($this->billing_address_country)) {
            $billingAddress .= ',<br>' . $this->billing_address_country . '.';
        }

        if (!empty($this->billing_address_phone)) {
            $billingAddress .= '<br>' . $this->billing_address_phone;
        }

        if (!empty($this->email)) {
            $billingAddress .= '<br>' . $this->email;
        }

        return $billingAddress;
    }

    public function getHtmlDeliveryAddressAttribute()
    {
        $deliveryAddress = trim($this->delivery_address_name . ' ' . $this->delivery_address_surname);

        if (!empty($this->delivery_address_street)) {
            $deliveryAddress .= ',<br>' . $this->delivery_address_street;
        }

        if (!empty($this->delivery_address_city)) {
            $deliveryAddress .= ',<br>' . $this->delivery_address_city;
        }

        if (!empty($this->delivery_address_zip)) {
            $deliveryAddress .= ', ' . $this->delivery_address_zip;
        }

        if (!empty($this->delivery_address_country)) {
            $deliveryAddress .= ',<br>' . $this->delivery_address_country . '.';
        }

        if (!empty($this->delivery_address_phone)) {
            $deliveryAddress .= '<br>' . $this->delivery_address_phone;
        }

        return $deliveryAddress;
    }

    public function getHtmlCompanyDetailsAttribute()
    {
        return sprintf(
            '%s<br>%s: %s<br>%s: %s',
            $this->company,
            __('ch-eshop::general.CIN'),
            $this->cin,
            __('ch-eshop::general.VAT'),
            $this->vat_no
        );
    }

    public function flexiRecord()
    {
        return $this->hasOne(FlexibeeOrderReference::class, 'order_id', 'id');
    }

    //check DB if we have any reference regarding ABRA record
    public function isAbraOrderExist()
    {
        if (!$this->flexiRecord) {
            return false;
        }

        return (bool)$this->flexiRecord->status;
    }
}
