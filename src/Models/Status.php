<?php

namespace Creativehandles\ChEshop\Models;

use Database\Factories\StatusFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Status extends Model
{
    use HasTranslations, HasFactory;

    // TODO: check what's up with ID numbers instead of unique strings.
    const NEW_ORDER = 1;
    const IN_PROGRESS = 2;
    const SENT = 3;
    const DELIVERED = 4;
    const CANCELED = 5;
    const COMPLAINT = 6;
    const PAID = 7;
    const FAILED = 8;

    const SYSTEM_STATUSES = [
        1 => 'NEW_ORDER',
        2 => 'IN_PROGRESS',
        3 => 'SENT',
        4 => 'DELIVERED',
        5 => 'CANCELED',
        6 => 'COMPLAINT',
        7 => 'PAID',
        8 => 'FAILED',
    ];

    protected $table = 'ch_eshop_statuses';

    public $translatable = ['title', 'description', 'notification'];

    protected $fillable = [
        'title',
        'description',
        'color',
        'on_change_notify_type',
        'notification',
    ];

    protected static function newFactory()
    {
        return StatusFactory::new();
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'ch_eshop_status_id');
    }
}
