<?php

namespace Creativehandles\ChEshop\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderAddItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'item.ch_eshop_variant_id'=>'required|exists:ch_eshop_variants,id',
            'item.quantity' => 'required|min:0|numeric',
            // 'item.discount' => 'sometimes|min:0|numeric',
        ];
    }
}
