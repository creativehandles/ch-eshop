<?php

namespace Creativehandles\ChEshop\Http\Requests;

use Creativehandles\ChEshop\Models\ProductOption;
use Illuminate\Foundation\Http\FormRequest;

class ProductUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product.*' => 'required',

            // product options
            'options' => [
                'sometimes',
                'nullable',
                'array',
                function($attribute, $value, $fail) {
                    // get the product option IDs
                    $ids = array_keys($value);
                    // query to check if any given product option ID is not valid
                    $productOptionsCount = ProductOption::whereIn('id', $ids)->count();

                    if ($productOptionsCount !== count($ids))
                        return $fail(__('ch-eshop::product/product.validations.options are invalid'));
                }
            ],

            // new variant
            'variant.is_visible' => 'sometimes|exclude_if:variant.type,null|boolean',
            'variant.type' => 'sometimes|nullable|string',
            'variant.unit_price_wo_tax' => 'exclude_if:variant.type,null|numeric',
            'variant.selling_price_wo_tax' => 'exclude_if:variant.type,null|numeric',
            'variant.code' => 'exclude_if:variant.type,null|string',
            'variant.ean' => 'exclude_if:variant.type,null|string',
            'variant.color' => 'exclude_if:variant.type,null|string',
            'variant.qty_available' => 'exclude_if:variant.type,null|numeric',
            'variant.availability' => 'exclude_if:variant.type,null|string',
            'variant.delivery_in_days' => 'exclude_if:variant.type,null|numeric',
            'variant.attachments' => 'nullable|array',
            'variant.attachments.*' => 'file|mimes:jpg,png,jpeg',

            // update existing variant
            'variant_update.*.is_visible' => 'sometimes|boolean',
            'variant_update.*.type' => 'required|string',
            'variant_update.*.unit_price_wo_tax' => 'required|numeric',
            'variant_update.*.selling_price_wo_tax' => 'required|numeric',
            'variant_update.*.code' => 'required|string',
            'variant_update.*.ean' => 'required|string',
            'variant_update.*.color' => 'required|string',
            'variant_update.*.qty_available' => 'required|numeric',
            'variant_update.*.availability' => 'required|string',
            'variant_update.*.delivery_in_days' => 'required|numeric',
            'variant_update.*.attachments' => 'nullable|array',
            'variant_update.*.attachments.*' => 'file|mimes:jpg,png,jpeg',
        ];
    }
}
