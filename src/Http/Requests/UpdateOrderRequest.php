<?php

namespace Creativehandles\ChEshop\Http\Requests;

use Creativehandles\ChEshop\Services\LocalesService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param LocalesService $localesService
     * @return array
     */
    public function rules(LocalesService $localesService)
    {
        return [
            "language" => [
                "required",
                "string",
                Rule::in($localesService->getLocaleCodes())
            ],
            "delivery_type" => "required|string",
            "delivery_cost_wo_tax" => "required|numeric",
            "payment_method" => "required|string",
            "payment_price" => "required|numeric",
            "client_note" => "sometimes|nullable|string",
            "is_company" => "sometimes|boolean",
            "company" => "required_if:is_company,true|nullable|string",
            "cin" => "required_if:is_company,true|nullable|string",
            "vat_no" => "required_if:is_company,true|nullable|string",
            "billing_address_name" => "required|string",
            "billing_address_surname" => "required|string",
            "billing_address_street" => "required|string",
            "billing_address_zip" => "required|string",
            "billing_address_city" => "required|string",
            "billing_address_country" => "required|string",
            "billing_address_phone" => "required|string",
            "email" => "required|email",
            "delivery_address_name" => "sometimes|nullable|string",
            "delivery_address_surname" => "sometimes|nullable|string",
            "delivery_address_street" => "sometimes|nullable|string",
            "delivery_address_zip" => "sometimes|nullable|string",
            "delivery_address_city" => "sometimes|nullable|string",
            "delivery_address_country" => "sometimes|nullable|string",
            "delivery_address_phone" => "sometimes|nullable|string",
            "admin_note" => "sometimes|nullable|string",
        ];
    }
}
