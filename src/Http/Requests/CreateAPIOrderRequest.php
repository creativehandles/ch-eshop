<?php

namespace Creativehandles\ChEshop\Http\Requests;

use Creativehandles\ChEshop\Services\LocalesService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * @bodyParam language string required Locale
 * @bodyParam email string required Email
 * @bodyParam is_company boolean required Is a company order?
 * @bodyParam company string Company name
 * @bodyParam cin string Company registration no.
 * @bodyParam vat_no string Company VAT no.
 * @bodyParam delivery_type string required Delivery type
 * @bodyParam payment_method string required Payment method
 * @bodyParam billing_address_name string required
 * @bodyParam billing_address_surname string required
 * @bodyParam billing_address_street string required
 * @bodyParam billing_address_city string required
 * @bodyParam billing_address_zip number required
 * @bodyParam billing_address_country string required
 * @bodyParam billing_address_phone string required
 * @bodyParam delivery_address_name string
 * @bodyParam delivery_address_surname string
 * @bodyParam delivery_address_street string
 * @bodyParam delivery_address_city string
 * @bodyParam delivery_address_zip string
 * @bodyParam delivery_address_country string
 * @bodyParam delivery_address_phone number
 * @bodyParam client_note string
 * @bodyParam items object[] required List of items
 * @bodyParam items[].ch_eshop_variant_id int required
 * @bodyParam items[].quantity int required
 */
class CreateAPIOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param LocalesService $localesService
     * @return array
     */
    public function rules(LocalesService $localesService)
    {
        return [
            "language" => [
                "required",
                "string",
                Rule::in($localesService->getLocaleCodes())
            ],
            "email" => "required|email",
            "is_company" => "sometimes|boolean",
            "company" => "required_if:is_company,true|nullable|string",
            "cin" => "required_if:is_company,true|nullable|string",
            "vat_no" => "required_if:is_company,true|nullable|string",
            "delivery_type" => "required|string",
            "payment_method" => "required|string",
            "billing_address_name" => "required|string",
            "billing_address_surname" => "required|string",
            "billing_address_street" => "required|string",
            "billing_address_city" => "required|string",
            "billing_address_zip" => "required|string",
            "billing_address_country" => "required|string",
            "billing_address_phone" => "required|string",
            "delivery_address_name" => "sometimes|nullable|string",
            "delivery_address_surname" => "sometimes|nullable|string",
            "delivery_address_street" => "sometimes|nullable|string",
            "delivery_address_city" => "sometimes|nullable|string",
            "delivery_address_zip" => "sometimes|nullable|string",
            "delivery_address_country" => "sometimes|nullable|string",
            "delivery_address_phone" => "sometimes|nullable|string",
            "client_note" => "sometimes|nullable|string",
            "items.*" => "required|array",
            "items.*.ch_eshop_variant_id" => "required|exists:ch_eshop_variants,id",
            "items.*.quantity" => "required|numeric|min:1"
        ];
    }
}
