<?php

namespace Creativehandles\ChEshop\Http\Requests;

use Creativehandles\ChEshop\Models\ProductOption;
use Illuminate\Foundation\Http\FormRequest;

class ProductCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // product
            'product.title' => 'required',
            'product.meta_title' => 'required',
            'product.short_description' => 'required',
            'product.description' => 'required',
            'product.meta_description' => 'required',

            // variant
            'variant.is_visible' => 'sometimes|boolean',
            'variant.type' => 'required|string',
            'variant.unit_price_wo_tax' => 'required|numeric',
            'variant.selling_price_wo_tax' => 'required|numeric',
            'variant.code' => 'required|string',
            'variant.ean' => 'required|string',
            'variant.color' => 'required|string',
            'variant.qty_available' => 'required|numeric',
            'variant.availability' => 'required|string',
            'variant.delivery_in_days' => 'required|numeric',
            'variant.attachments' => 'nullable|array',
            'variant.attachments.*' => 'file|mimes:jpg,png,jpeg',

            // product options
            'options' => [
                'sometimes',
                'nullable',
                'array',
                function($attribute, $value, $fail) {
                    // get the product option IDs
                    $ids = array_keys($value);
                    // query to check if any given product option ID is not valid
                    $productOptionsCount = ProductOption::whereIn('id', $ids)->count();

                    if ($productOptionsCount !== count($ids))
                        return $fail(__('ch-eshop::product/product.validations.options are invalid'));
                }
            ],
        ];
    }
}