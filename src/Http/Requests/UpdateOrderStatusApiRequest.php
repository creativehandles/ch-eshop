<?php

namespace Creativehandles\ChEshop\Http\Requests;

use Creativehandles\ChEshop\Models\Status;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * @bodyParam status_id int required ID of the order status
 *
 * <b>Accepted order status IDs</b><br>
 * NEW_ORDER = 1 <br>
 * IN_PROGRESS = 2 <br>
 * SENT = 3 <br>
 * DELIVERED = 4 <br>
 * CANCELED = 5 <br>
 * COMPLAINT = 6 <br>
 * PAID = 7 <br>
 * FAILED = 8
 */
class UpdateOrderStatusApiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status_id' => [
                'required',
                'integer',
                Rule::in(array_keys(Status::SYSTEM_STATUSES))
            ]
        ];
    }
}
