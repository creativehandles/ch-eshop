<?php

namespace Creativehandles\ChEshop\Http\Livewire;

use Livewire\Component;

class OrderPrice extends Component
{
    public $order;
    
    protected function getListeners(): array
    {
        return array_merge(
            parent::getListeners(),
            ['itemAdded' => '$refresh']
        );
    }

    public function render()
    {
        return view('ch-eshop::livewire.order-prices');
    }
}
