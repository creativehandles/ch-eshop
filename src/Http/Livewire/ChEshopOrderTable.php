<?php

namespace Creativehandles\ChEshop\Http\Livewire;

use Carbon\Carbon;
use Creativehandles\ChEshop\Delivery\CzechPost;
use Creativehandles\ChEshop\Models\Order;
use Creativehandles\ChEshop\Models\Status;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use PowerComponents\LivewirePowerGrid\Button;
use PowerComponents\LivewirePowerGrid\Column;
use PowerComponents\LivewirePowerGrid\PowerGrid;
use PowerComponents\LivewirePowerGrid\PowerGridEloquent;
use PowerComponents\LivewirePowerGrid\PowerGridComponent;
use PowerComponents\LivewirePowerGrid\Traits\ActionButton;
use Throwable;

final class ChEshopOrderTable extends PowerGridComponent
{
    use ActionButton;

    /*
    |--------------------------------------------------------------------------
    |  Features Setup
    |--------------------------------------------------------------------------
    | Setup Table's general features
    |
    */

    protected function getListeners()
    {
        return array_merge(
            parent::getListeners(),
            [
                'sendParcels',
                'downloadLabels',
            ]
        );
    }

    public function sendParcels(): void
    {
        $orders = Order::find($this->checkboxValues);
        $messages = [];
        try {
            if (!empty($orders)) {
                foreach ($orders as $order) {
                    $parcelCode = null;
                    $errors = null;

                    if (empty($order->czech_post_id)) {
                        $response = (new CzechPost())->sendParcels($this->prepareOrderForDelivery($order));
                        if ($response['status'] == 202) {
                            $order->update([
                                'czech_post_id' => $response['data']['idTransaction']
                            ]);
                            $messages[] = [
                                'type' => 'success',
                                'body' => __('ch-eshop::order/create.Order was successfully sent for delivery', ['order_no' => $order->order_no])
                            ];
                        } else {
                            $messages[] = [
                                'type' => 'error',
                                'body' => __('ch-eshop::order/create.Order unable to send for delivery', ['order_no' => $order->order_no])
                            ];
                        }
                    }

                    $order->refresh();

                    if (!empty($order->czech_post_id) && empty(trim($order->printing_data))) {
                        $response = (new CzechPost())->getIdTransaction($order->czech_post_id);

                        if ($response['status'] == 200 && isset($response['data']['ResultSendParcelsList'])) {

                            $parcelCode = array_get($response, 'data.ResultSendParcelsList.0.parcelCode');
                            $statusResEN = (new CzechPost())->parcelsStatuses([$parcelCode], 'EN');
                            $statusResCS = (new CzechPost())->parcelsStatuses([$parcelCode]);
                            $order->update([
                                'printing_data' => $parcelCode,
                                'delivery_data' => ['en' => $statusResEN, 'cs' => $statusResCS]
                            ]);
                            $messages[] = [
                                'type' => 'success',
                                'body' => __('ch-eshop::order/create.Order delivery status updated', ['order_no' => $order->order_no])
                            ];
                        } else if ($response['status'] == 202) {
                            $messages[] = [
                                'type' => 'error',
                                'body' => __('ch-eshop::order/create.Parcels are still being processed. Please try again in a while')
                            ];
                        } else {

                            foreach (Arr::get($response, 'data.StatusResponseList', []) as $value) {
                                $errors = (!empty($errors)) ? $errors . ', ' . $value['responseText'] : $value['responseText'];
                            }

                            $messages[] = [
                                'type' => 'error',
                                'body' => __('ch-eshop::order/create.Order unable to send for delivery due to', ['order_no' => $order->order_no, 'errors' => $errors])
                            ];
                        }
                    } else if (!empty($order->printing_data)) {

                        $statusResEN = (new CzechPost())->parcelsStatuses([$order->printing_data], 'EN');
                        $statusResCS = (new CzechPost())->parcelsStatuses([$order->printing_data]);

                        if ($statusResCS['status'] > 399 || $statusResEN['status'] > 399) {
                            $messages[] = [
                                'type' => 'error',
                                'body' => __('ch-eshop::order/create.status error', ['order_no' => $order->order_no, 'errors' => $statusResEN['error_message']])
                            ];
                        } else {
                            $order->update([
                                'delivery_data' => ['en' => $statusResEN, 'cs' => $statusResCS]
                            ]);
                            $messages[] = [
                                'type' => 'success',
                                'body' => __('ch-eshop::order/create.Order delivery status updated', ['order_no' => $order->order_no])
                            ];
                        }
                    } else {
                        $messages[] = [
                            'type' => 'success',
                            'body' => __('ch-eshop::order/create.No Action')
                        ];
                    }
                }
            }
        } catch (Throwable $th) {
            Log::error($th->getMessage(), $th->getTrace());

            $messages[] = [
                'type' => 'error',
                'body' => $th->getMessage()
            ];
        }

        $this->emit('bulk-messages', $messages);
        $this->fillData();
    }

    private function prepareOrderForDelivery(Order $order)
    {
        $payload = [
            'parcelHeader' => [
                'transmissionDate' => Carbon::now()->format('Y-m-d'),
                'customerID' => 'M13584',
                'postCode' => '60011',
                'locationNumber' => (int) config('ch-eshop.czech_post.loonoy_location_id'),
                'goodToAccept' => true,
            ],
            'parcelDataList' => [
                0 => [
                    'parcelParams' => [
                        'recordID' => (string) $order->id, // only strings accepted
                        'prefixParcelCode' => 'DR',
                        'weight' => '10.00', // hard-coded because the package is around 10 kg as of now
                        'insuredValue' => 15000,
                        // 'amount' => 1500, // commented because package is cash on delivery
                        'currency' => 'CZK',
                        'notePrint' => $order->order_no,
                    ],
                    'parcelAddress' => [
                        'recordID' => "$order->id", // only string accepted
                        'subject' => $order->is_company ? 'P' : 'F', // if customer is company 'P' else if individual 'F'.
                        'address' => [
                            'street' => $order->delivery_address_street, // houseNumber and sequenceNumber are included in the street address in the order form.
                            'city' => $order->delivery_address_city,
                            'zipCode' => $order->delivery_address_zip,
                            'isoCountry' => 'CZ',
                        ],
                        'mobilNumber' => $order->delivery_address_phone,
                        'emailAddress' => $order->email,
                    ],
                    'parcelServices' => [
                        0 => '7',
                        1 => '45',
                        2 => 'XL',
                    ],
                ],
            ],
        ];

        if ($order->is_company) {
            Arr::set(
                $payload,
                'parcelDataList.0.parcelAddress.company',
                sprintf('%s - %s %s', $order->company, $order->delivery_address_name, $order->delivery_address_surname)
            );
        } else {
            Arr::set($payload, 'parcelDataList.0.parcelAddress.firstName', $order->delivery_address_name);
            Arr::set($payload, 'parcelDataList.0.parcelAddress.surname', $order->delivery_address_surname);
        }

        return $payload;
    }

    public function downloadLabels()
    {
        try {
            $orders = Order::find($this->checkboxValues);

            if (empty($orders)) {
                $this->emit('bulk-messages', [
                    [
                        'type' => 'error',
                        'body' => __('ch-eshop::order/create.Please select at least one order.')
                    ]
                ]);

                return;
            }

            $messages = [];
            $parcelCodes = [];

            foreach ($orders as $order) {
                if (empty($order->printing_data)) {
                    if (empty($order->czech_post_id)) {
                        $messages[] = [
                            'type' => 'error',
                            'body' => __(
                                'ch-eshop::order/create.Czech post ID is empty in the order',
                                [
                                    'orderNo' => $order->order_no,
                                ]
                            )
                        ];

                        continue;
                    }

                    $parcelCode = (new CzechPost())->getParcelCode($order->czech_post_id);
                    $order->update(['printing_data' => $parcelCode]);
                }

                $parcelCodes[] = $order->printing_data;
            }

            /**
             * If parcel codes are empty, just emit the messages and return.
             * Error messages on why parcel codes are empty are already set in the messages array.
             */
            if (empty($parcelCodes)) {
                $this->emit('bulk-messages', $messages);
                return;
            }

            $labelData = (new CzechPost())->getLabelDataOfParcels($parcelCodes);
            $fileName = sprintf('labels-%s.pdf', date('Ymd-His'));
            $filePath = 'ch-eshop/labels/' . $fileName;

            if (!Storage::disk(config('ch-eshop.files.disk'))->put($filePath, $labelData)) {
                $messages[] = [
                    'type' => 'error',
                    'body' => __(
                        'ch-eshop::order/create.Cannot create labels PDF.',
                        [
                            'orderNo' => $order->order_no,
                        ]
                    )
                ];
            }

            $this->emit('bulk-messages', $messages);

            return Storage::disk(config('ch-eshop.files.disk'))->download($filePath);
        } catch (Throwable $th) {
            Log::debug($th->getMessage(), $th->getTrace());

            $messages[] = [
                'type' => 'error',
                'body' => $th->getMessage()
            ];

            $this->emit('bulk-messages', $messages);

            return;
        }
    }

    public function printingData(Order $order)
    {
        return [
            "printingHeader" => [
                "customerID" => '12', //max length 6
                "contractNumber" => "07485485655",
                "idForm" => 101,
                "shiftHorizontal" => 2,
                "shiftVertical" => 2,
                "position" => 1
            ],
            "printingData" => [$order->printing_data]
        ];
    }

    public function setUp(): void
    {
        $this
            ->showCheckBox()
            ->showRecordCount('full')
            ->showPerPage();
    }

    /*
    |--------------------------------------------------------------------------
    |  Datasource
    |--------------------------------------------------------------------------
    | Provides data to your Table using a Model or Collection
    |
    */

    /**
     * PowerGrid datasource.
     *
     * @return  \Illuminate\Database\Eloquent\Builder<\App\Models\User>|null
     */
    public function datasource(): ?Builder
    {
        return Order::query()->join('ch_eshop_statuses', function ($order) {
            $order->on('ch_eshop_orders.ch_eshop_status_id', '=', 'ch_eshop_statuses.id');
        })->select([
            'ch_eshop_orders.*',
            DB::raw("ch_eshop_orders.created_at as order_date"),
            // DB::raw("CONCAT(ch_eshop_orders.billing_address_name,' ',ch_eshop_orders.billing_address_surname) as user_info"),
            DB::raw("JSON_UNQUOTE(JSON_EXTRACT(ch_eshop_statuses.title, '$." . app()->getLocale() . "')) as status_title")
        ]);
    }

    /*
    |--------------------------------------------------------------------------
    |  Relationship Search
    |--------------------------------------------------------------------------
    | Configure here relationships to be used by the Search and Table Filters.
    |
    */

    /**
     * Relationship search.
     *
     * @return array<string, array<int, string>>
     */
    public function relationSearch(): array
    {
        return [];
    }

    private function statuses()
    {
        return Status::all();
    }

    /*
    |--------------------------------------------------------------------------
    |  Add Column
    |--------------------------------------------------------------------------
    | Make Datasource fields available to be used as columns.
    | You can pass a closure to transform/modify the data.
    |
    */
    public function addColumns(): ?PowerGridEloquent
    {
        return PowerGrid::eloquent()
            ->addColumn('order_no')
            ->addColumn('order_date', function ($order) {
                return $order->created_at->format('d.m.Y H:i');
            })
            ->addColumn('billing_address_name', function ($order) {
                return $order->billing_address_full_name;
            })
            ->addColumn('delivery_type', function ($order) {
                $str = "<p>" . $order->delivery_type;

                if (isset(($order->delivery_data)['data']['detail'][0]['parcelStatuses'])) {
                    $data = ($order->delivery_data)['data']['detail'][0]['parcelStatuses'];
                    $str .= "<br><small>" . Arr::last($data)['text'] . "</small>";
                }

                if (!empty($order->printing_data)) {
                    $str .= "<br>[{$order->printing_data}]";
                }

                return $str . "</p>";
            })
            ->addColumn('payment_method')
            ->addColumn('status_title', function ($item) {
                $color = ($this->statuses()->find($item->ch_eshop_status_id)) ? 'style="color:' . $this->statuses()->find($item->ch_eshop_status_id)->color . '"' : null;
                $update_link = route('admin.ch-eshop.order.update', ['order' => $item->id]);
                $select = '<select data-link="' . $update_link . '" ' . $color . ' name="ch_eshop_status_id" class="power_grid form-control mb-1 shadow-none statusSelect">
                            <option>Select Status</option>';
                foreach ($this->statuses() as $status) {
                    $textColor = 'style="color:' . $status->color . '"';
                    if ($item->ch_eshop_status_id == $status->id) {
                        $select = $select . '<option selected ' . $textColor . ' value="' . $status->id . '">' . $status->title . '</option>';
                    } else {
                        $select = $select . '<option ' . $textColor . ' value="' . $status->id . '">' . $status->title . '</option>';
                    }
                }
                return $select . '</select>';
            })
            ->addColumn('total_cost_w_tax', function ($order) {
                return $order->total_cost_w_tax . ' ' . $order->currency;
            })
            ->addColumn('abra_flexi', function ($order) {
               $actions = "<a class='btn btn-sm btn-warning mb-1' href=" . route('admin.ch-eshop.order.update-afb-record', ['order' => $order->id]) . ">" . __('ch-eshop::table.button.update-afb') . "</a><br>";
                               //if record not found, then show create button
                if (!$order->isAbraOrderExist()) {
                    $actions = "<a class='btn btn-sm btn-warning mb-1' href=" . route('admin.ch-eshop.order.create-afb-record', ['order' => $order->id]) . ">" . __('ch-eshop::table.button.create-afb') . "</a><br>";
                }

                //if record exist then show the button
                if ($order->isAbraOrderExist()) {
                    $actions .= "<a class='btn btn-sm btn-warning  mb-1' href=" . route('admin.ch-eshop.order.download-invoice', ['order' => $order->id]) . ">" . __('ch-eshop::table.button.download-invoice') . "</a><br>";
                }

                return $actions;
            });
    }

    /*
    |--------------------------------------------------------------------------
    |  Include Columns
    |--------------------------------------------------------------------------
    | Include the columns added columns, making them visible on the Table.
    | Each column can be configured with properties, filters, actions...
    |
    */

    /**
     * PowerGrid Columns.
     *
     * @return array<int, Column>
     */
    public function columns(): array
    {
        return [

            Column::add()
                ->title(__('ch-eshop::table.order.order_ref'))
                ->field('order_no')
                ->sortable()
                ->searchable()
                ->makeInputText('order_no'),

            Column::add()
                ->title(__('ch-eshop::table.order.order_date'))
                ->field('order_date', 'ch_eshop_orders.created_at')
                ->sortable()
                ->makeInputDatePicker('ch_eshop_orders.created_at'),

            Column::add()
                ->title(__('ch-eshop::table.order.user_info'))
                ->field('billing_address_name')
                ->sortable()
                ->searchable()
                ->makeInputText('billing_address_name'),

            Column::add()
                ->title(__('ch-eshop::table.order.delivery'))
                ->field('delivery_type'),

            Column::add()
                ->title(__('ch-eshop::table.order.payment'))
                ->field('payment_method')
                ->sortable()
                ->makeInputText('payment_method')
                ->searchable(),

            Column::add()
                ->title(__('ch-eshop::table.order.state'))
                ->field('status_title')
                ->makeInputText('title')

                // ->field('status_title', 'ch_eshop_statuses.title')
                // ->makeInputMultiSelect(Status::all(), 'title', 'status_title')

                ->sortable()
                ->searchable(),

            Column::add()
                ->title(__('ch-eshop::table.order.total'))
                ->field('total_cost_w_tax')
                ->sortable()
                ->searchable()
                ->makeInputText(),

            Column::add()
                ->title('ABRA Flexi')
                ->field('abra_flexi')
                ->sortable()
                ->searchable()
                ->makeInputText(),

        ];
    }

    public function header(): array
    {
        return [
            Button::add('post-parcels')
                ->caption(__('ch-eshop::general.parcel.post'))
                ->class('action btn btn-secondary text-white')
                ->emit('sendParcels', []),

            Button::add('download-parcel-labels')
                ->caption(__('ch-eshop::general.parcel.label_download'))
                ->class('action btn btn-secondary text-white')
                ->emit('downloadLabels', [])
        ];
    }

    /*
    |--------------------------------------------------------------------------
    | Actions Method
    |--------------------------------------------------------------------------
    | Enable the method below only if the Routes below are defined in your app.
    |
    */

    /**
     * PowerGrid Product Action Buttons.
     *
     * @return array<int, \PowerComponents\LivewirePowerGrid\Button>
     */
    public function actions(): array
    {
        return [
            Button::add('edit')
                ->caption(__('ch-eshop::table.button.edit'))
                ->class('btn btn-info btn-sm')
                ->target('')
                ->route('admin.ch-eshop.order.edit', ['order' => 'id']),

            Button::add('destroy')
                ->target('')
                ->caption(__('ch-eshop::table.button.delete'))
                ->class('btn btn-danger btn-sm')
                ->route('admin.ch-eshop.order.destroy', ['order' => 'id'])
                ->method('delete')
        ];
    }

    /*
    |--------------------------------------------------------------------------
    | Edit Method
    |--------------------------------------------------------------------------
    | Enable the method below to use editOnClick() or toggleable() methods.
    | Data must be validated and treated (see "Update Data" in PowerGrid doc).
    |
    */

    /**
     * PowerGrid Product Update.
     *
     * @param array<string,string> $data
     * @return bool
     * @throws Exception
     */
    public function update(array $data): bool
    {
        try {
            $updated = Order::query()->findOrFail($data['id'])
                ->update([
                    $data['field'] => $data['value'],
                ]);
        } catch (QueryException $exception) {
            $updated = false;
        }

        $this->fillData();

        return $updated;
    }
}
