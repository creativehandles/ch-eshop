<?php

namespace Creativehandles\ChEshop\Http\Livewire;

use Creativehandles\ChEshop\Models\Order;
use Creativehandles\ChEshop\Models\Status;
use Creativehandles\ChEshop\Services\OrderService;
use Creativehandles\ChEshop\Services\StatusService;
use Livewire\Component;

class ChangeOrderState extends Component
{
    public $order;
    public $data;
    public $default = 'Select State';
    public $label = 'Label';

    protected $orderService;
    protected $statusService;
    
    protected function getListeners(): array
    {
        return array_merge(
            parent::getListeners(),
            ['order_status_changed' => '$refresh']
        );
    }

    public function mount($order)
    {
        $this->data = $this->systemStatus();
        $this->order = $order;
        $this->selected = $this->order->ch_eshop_status_id; // to default select
    }

    public function boot(Order $order, OrderService $orderService, StatusService $statusService)
    {
        $this->order = $order;
        $this->orderService = $orderService;
        $this->statusService = $statusService;
    }

    public function render()
    {
        return view('ch-eshop::livewire.select');
    }

    public function systemStatus()
    {
        $data = [];
        foreach ($this->statusService->getSystemStatuses() as $status) {
            $data[$status->id] = $status->title;
        }
        return $data;
    }

    public function changeEvent(int $value)
    {
        if (!empty($value) && $this->order->ch_eshop_status_id !== $value && $this->orderService->changeOrderState(Status::find($value), $this->order)) {
            session()->flash('success', __('ch-eshop::order/edit.updated'));
            $this->emitSelf('order_status_changed');
        }
    }
}
