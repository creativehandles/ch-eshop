<?php

namespace Creativehandles\ChEshop\Http\Livewire;

use Creativehandles\ChEshop\Models\Variant;
use Creativehandles\ChEshop\Services\ProductService;
use Livewire\Component;

class DeleteVariant extends Component
{
    public $variant;

    private $productService;

    public function boot(Variant $variant, ProductService $productService)
    {
        $this->variant = $variant;
        $this->productService = $productService;
    }

    public function render()
    {
        return '<input type="button" class="btn btn-danger" wire:click="delete" value="'.__('ch-eshop::product/product.variant.delete').'">';
    }

    public function delete()
    {
        try {
            if($this->productService->deleteVariant($this->variant)){
                session()->flash('success', __('ch-eshop::product/product.edit.updated'));
            }
            return redirect(route('admin.ch-eshop.product.edit',['product' => $this->variant->ch_eshop_product_id]));
        } catch (\Throwable $th) {
            session()->flash('error', __('ch-eshop::product/product.edit.failed'));
            return redirect(route('admin.ch-eshop.product.edit',['product' => $this->variant->ch_eshop_product_id]));
        }
    }
}
