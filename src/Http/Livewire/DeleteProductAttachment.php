<?php

namespace Creativehandles\ChEshop\Http\Livewire;

use Creativehandles\ChEshop\Models\ChEshopAttachment;
use Creativehandles\ChEshop\Services\ProductService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;

class DeleteProductAttachment extends Component
{
    private ProductService $productService;
    public ChEshopAttachment $attachment;
    public Model $parent;

    public function boot(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function mount(ChEshopAttachment $attachment, Model $parent)
    {
        $this->attachment = $attachment;
        $this->parent = $parent;
    }

    public function render()
    {
        return sprintf(
            '<li class="list-group-item d-flex justify-content-between align-items-center" style="padding: 0.75rem;">
                <img style="max-width: 100px;max-height: 50px;" src="%s" class="img-thumbnail">
                <button class="btn btn-primary btn-sm" wire:click="delete">X</button>
            </li>',
            Storage::url($this->attachment->value)
        );
    }

    public function delete()
    {
        $this->productService->removeAttachment($this->attachment, $this->parent);
    }
}
