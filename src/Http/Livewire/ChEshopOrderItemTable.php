<?php

namespace Creativehandles\ChEshop\Http\Livewire;

use Creativehandles\ChEshop\Models\Order;
use Creativehandles\ChEshop\Models\OrderItem;
use Creativehandles\ChEshop\Services\OrderService;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use PowerComponents\LivewirePowerGrid\Button;
use PowerComponents\LivewirePowerGrid\Column;
use PowerComponents\LivewirePowerGrid\PowerGrid;
use PowerComponents\LivewirePowerGrid\PowerGridEloquent;
use PowerComponents\LivewirePowerGrid\PowerGridComponent;
use PowerComponents\LivewirePowerGrid\Traits\ActionButton;

final class ChEshopOrderItemTable extends PowerGridComponent
{
    use ActionButton;

    //Messages informing success/error data is updated.
    public bool $showUpdateMessages = false;

    public $order;
    public $hidePerPage = true;
    protected $orderService;

    public function boot(Order $order, OrderService $orderService)
    {
        $this->order = $order;
        $this->orderService = $orderService;
    }

    protected function getListeners(): array
    {
        return array_merge(
            parent::getListeners(),
            ['itemAdded' => '$refresh']
        );
    }

    /*
    |--------------------------------------------------------------------------
    |  Features Setup
    |--------------------------------------------------------------------------
    | Setup Table's general features
    |
    */
    public function setUp(): void
    {
        $this
            ->showPerPage(100);
    }

    /*
    |--------------------------------------------------------------------------
    |  Datasource
    |--------------------------------------------------------------------------
    | Provides data to your Table using a Model or Collection
    |
    */

    /**
     * PowerGrid datasource.
     *
     * @return  \Illuminate\Database\Eloquent\Builder<\App\Models\User>|null
     */
    public function datasource(): ?Builder
    {
        return OrderItem::query()->where('ch_eshop_order_id', $this->order->id);
    }

    /*
    |--------------------------------------------------------------------------
    |  Relationship Search
    |--------------------------------------------------------------------------
    | Configure here relationships to be used by the Search and Table Filters.
    |
    */

    /**
     * Relationship search.
     *
     * @return array<string, array<int, string>>
     */
    public function relationSearch(): array
    {
        return [];
    }

    /*
    |--------------------------------------------------------------------------
    |  Add Column
    |--------------------------------------------------------------------------
    | Make Datasource fields available to be used as columns.
    | You can pass a closure to transform/modify the data.
    |
    */
    public function addColumns(): ?PowerGridEloquent
    {
        $locale = app()->getLocale();
        return PowerGrid::eloquent()
            ->addColumn('product', function ($item) use ($locale) {
                return Arr::get(json_decode($item->object, true) ?? [], "title.$locale");
            })
            ->addColumn('ean')
            ->addColumn('quantity')
            ->addColumn('selling_price_wo_tax', fn ($orderItem) => $orderItem->selling_price_wo_tax.' '.$orderItem->order->currency)
            ->addColumn('total_price_w_tax', fn ($orderItem) => $orderItem->total_price_w_tax.' '.$orderItem->order->currency);
    }

    /*
    |--------------------------------------------------------------------------
    |  Include Columns
    |--------------------------------------------------------------------------
    | Include the columns added columns, making them visible on the Table.
    | Each column can be configured with properties, filters, actions...
    |
    */

    /**
     * PowerGrid Columns.
     *
     * @return array<int, Column>
     */
    public function columns(): array
    {
        return [
            Column::add()
                ->title(__('ch-eshop::table.order-item.product'))
                ->field('product'),

            Column::add()
                ->title(__('ch-eshop::table.order-item.ean'))
                ->field('ean'),

            Column::add()
                ->title(__('ch-eshop::table.order-item.qty'))
                ->field('quantity')
                ->editOnClick(true),

            Column::add()
                ->title(__('ch-eshop::table.order-item.selling_price_wo_tax'))
                ->field('selling_price_wo_tax'),

            Column::add()
                ->title(__('ch-eshop::table.order-item.total'))
                ->field('total_price_w_tax'),
        ];
    }

    /**
     * PowerGrid Product Action Buttons.
     *
     * @return array<int, \PowerComponents\LivewirePowerGrid\Button>
     */


    public function actions(): array
    {
        return [
            Button::add('destroy')
                ->target('_parent')
                ->caption(__('ch-eshop::table.button.delete'))
                ->class('btn btn-danger')
                ->route('admin.ch-eshop.order.delete.orderItem', ['order' => $this->order->id, 'orderItem' => 'id'])
                ->method('delete')
        ];
    }

    public function inputTextChanged(array $data): void
    {
        if ($data['field'] === 'quantity') {

            $itemObject = OrderItem::find($data['id']);

            $total_price_update = [

                //WO tax total
                [
                    'id' => $data['id'],
                    'value' => $data['value'] * $itemObject->selling_price_wo_tax,
                    'field' => 'total_price_wo_tax'
                ],

                //W tax total
                [
                    'id' => $data['id'],
                    'value' => $data['value'] * $itemObject->selling_price_w_tax,
                    'field' => 'total_price_w_tax'
                ]
            ];

            $this->update($total_price_update[0]);
            $this->update($total_price_update[1]);
        }

        $update = $this->update($data);

        $this->fillData();

        if (!$this->showUpdateMessages) {
            return;
        }

        if (!$update) {
            session()->flash('error', $this->updateMessages('error', $data['field']));

            return;
        }
        $this->orderService->generateOrderPrices($this->order);
        session()->flash('success', $this->updateMessages('success', $data['field']));
    }

    /*
    |--------------------------------------------------------------------------
    | Edit Method
    |--------------------------------------------------------------------------
    | Enable the method below to use editOnClick() or toggleable() methods.
    | Data must be validated and treated (see "Update Data" in PowerGrid doc).
    |
    */

    /**
     * PowerGrid Product Update.
     *
     * @param array<string,string> $data
     */


    public function update(array $data): bool
    {
        try {
            $updated = $this->orderService->updateOrderItem($this->order, [
                'id' => $data['id'],
                $data['field'] => $data['value']
            ]);
        } catch (QueryException $exception) {
            $updated = false;
        }
        return $updated;
    }

    public function updateMessages(string $status = 'error', string $field = '_default_message'): string
    {
        $updateMessages = [
            'success'   => [
                '_default_message' => __('Data has been updated successfully!'),
                //'custom_field'   => __('Custom Field updated successfully!'),
            ],
            'error' => [
                '_default_message' => __('Error updating the data.'),
                //'custom_field'   => __('Error updating custom field.'),
            ]
        ];

        $message = ($updateMessages[$status][$field] ?? $updateMessages[$status]['_default_message']);

        return (is_string($message)) ? $message : 'Error!';
    }

    public function addItem(array $data): void
    {
        $this->dispatchBrowserEvent('addItem', $data);
    }
}
