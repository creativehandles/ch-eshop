<?php

namespace Creativehandles\ChEshop\Http\Livewire;

use Creativehandles\ChEshop\Models\Order;
use Creativehandles\ChEshop\Services\OrderService;
use Livewire\Component;
use Throwable;

class AddOrderItem extends Component
{
    public Order $order;
    protected OrderService $orderService;

    public $orderItems;
    public $quantity;
    public $variant_id;

    public function mount($order, $orderItems)
    {
        $this->order = $order;
        $this->orderItems = $orderItems;
    }

    public function boot(Order $order, OrderService $orderService)
    {
        $this->order = $order;
        $this->orderService = $orderService;
    }

    public function render()
    {
        return view('ch-eshop::livewire.add-order-item');
    }

    /**
     * @throws Throwable
     */
    public function addItem()
    {
        try {
            $data = $this->validate([
                'quantity' => 'required|numeric',
                'variant_id' => 'required|integer',
            ]);

            $this->orderService->addBulkItem(
                [
                    [
                        'ch_eshop_variant_id' => $data['variant_id'],
                        'quantity' => $data['quantity'],
                    ]
                ],
                $this->order
            );

            $this->quantity = null;
            $this->variant_id = null;

            $this->emit('itemAdded');
        } catch (Throwable $th) {
            session()->flash('error', $th->getMessage());

            throw $th;
        }
    }
}
