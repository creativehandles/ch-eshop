<?php

namespace Creativehandles\ChEshop\Http\Resources;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use JsonSerializable;

class VariantResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array|Arrayable|JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'is_visible' => (boolean) $this->is_visible,
            'type' => $this->type,
            'code' => $this->code,
            'ean' => $this->ean,
            'color' => $this->color,
            'selling_price_wo_tax' => (double) $this->selling_price_wo_tax,
            'selling_price_w_tax' => (double) $this->selling_price_w_tax,
            'availability' => $this->availability,
            'qty_available' => $this->qty_available,
            'delivery_in_days' => $this->delivery_in_days,
            'attachments' => AttachmentResource::collection($this->whenLoaded('attachments')),
        ];
    }
}
