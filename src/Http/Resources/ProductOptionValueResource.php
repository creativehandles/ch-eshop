<?php

namespace Creativehandles\ChEshop\Http\Resources;

use Creativehandles\ChEshop\Repositories\ProductRepository;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use JsonSerializable;

class ProductOptionValueResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array|Arrayable|JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'option_id' => $this->ch_eshop_product_option_id,
            'value' => $this->getTypeCastedValue(),
            'option' => new ProductOptionResource($this->whenLoaded('option')),
        ];
    }

    private function getTypeCastedValue()
    {
        if ($this->whenLoaded('option', true, false)) {
            switch ($this->option->type) {
                case ProductRepository::PRODUCT_OPTION_TYPE_BOOLEAN:
                    return (boolean) $this->value;
                    break;

                default:
                    return $this->value;
                    break;
            }
        }

        return $this->value;
    }
}
