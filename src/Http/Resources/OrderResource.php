<?php

namespace Creativehandles\ChEshop\Http\Resources;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use JsonSerializable;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array|Arrayable|JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'ch_eshop_status_id' => $this->ch_eshop_status_id,
            'order_no' => $this->order_no,
            'invoice' => $this->invoice,
            'language' => $this->language,
            'currency' => $this->currency,
            'email' => $this->email,
            'phone' => $this->phone,
            'is_company' => $this->is_company,
            'company' => $this->company,
            'cin' => $this->cin,
            'vat_no' => $this->vat_no,
            'delivery_type' => $this->delivery_type,
            'delivery_cost_wo_tax' => $this->delivery_cost_wo_tax,
            'delivery_cost_w_tax' => $this->delivery_cost_w_tax,
            'payment_method' => $this->payment_method,
            'payment_price' => $this->payment_price,
            'payment_price_w_tax' => $this->payment_price_w_tax,
            'total_cost_wo_tax' => $this->total_cost_wo_tax,
            'total_cost_w_tax' => $this->total_cost_w_tax,
            'total_tax' => $this->total_tax,
            'total_pay' => $this->total_pay,
            'tax_rate' => $this->tax_rate,
            'billing_address_name' => $this->billing_address_name,
            'billing_address_surname' => $this->billing_address_surname,
            'billing_address_street' => $this->billing_address_street,
            'billing_address_city' => $this->billing_address_city,
            'billing_address_zip' => $this->billing_address_zip,
            'billing_address_country' => $this->billing_address_country,
            'billing_address_phone' => $this->billing_address_phone,
            'delivery_address_name' => $this->delivery_address_name,
            'delivery_address_surname' => $this->delivery_address_surname,
            'delivery_address_street' => $this->delivery_address_street,
            'delivery_address_city' => $this->delivery_address_city,
            'delivery_address_zip' => $this->delivery_address_zip,
            'delivery_address_country' => $this->delivery_address_country,
            'delivery_address_phone' => $this->delivery_address_phone,
            'client_note' => $this->client_note,
        ];
    }
}
