<?php

namespace Creativehandles\ChEshop\Http\Controllers\PluginsControllers;

use App\Http\Controllers\Controller;
use Creativehandles\ChEshop\Delivery\CzechPost;

class EShopController extends Controller
{
    public function createLocation()
    {
        $payload = [
            "locationName" => "Loonoy",
            "locationNameAddOn" => "",
            "senderAddress" => [
                "companyName" => "Loonoy s.r.o.",
                "aditionAddress" => "",
                "street" => "Jiřího z Poděbrad",
                "houseNumber" => "",
                "sequenceNumber" => "860",
                "cityPart" => "",
                "city" => "Hrušovany u Brna",
                "zipCode" => "66462",
                "isoCountry" => "CZ",
                "subIsoCountry" => "",
                "addressCode" => 0,
            ],
            "codAddress" => [
                "companyName" => "Loonoy s.r.o.",
                "aditionAddress" => "",
                "street" => "Jiřího z Poděbrad",
                "houseNumber" => "",
                "sequenceNumber" => "860",
                "cityPart" => "",
                "city" => "Hrušovany u Brna",
                "zipCode" => "66462",
                "isoCountry" => "CZ",
                "subIsoCountry" => "",
                "addressCode" => 0
            ],
            "codBank" => [
                // "prefixAccount" => "",
                "account" => "2201873595",
                "bank" => "2010",
            ],
            "senderContacts" => [
                "mobilNumber" => "775937972",
                "phoneNumber" => "",
                "emailAddress" => "info@loonoy.com",
            ]
        ];

        $czechPost = (new CzechPost())->createLocation($payload);
    }


    public function getLocationList(){
        return (new CzechPost())->getLocationList();
    }

    public function getSingleLocationByName(){
        return (new CzechPost())->getSingleLocationByName();
    }

    public function response($message, $status, $data = null, $with_input = true, $redirection = null)
    {
        // set message
        $type = "success";

        if ($status > 399) {
            $type = "error";
        }

        // generate Appropriate Response
        if (request()->ajax()) {
            return response()->json(['message' => $message, 'data' => $data ?? null], $status);
        } else {
            // create redirection
            if ($redirection) {
                $redirection = redirect($redirection);
            } else {
                $redirection = redirect()->back();
            }

            $redirection = $redirection->with([$type => $message, $data]);

            // merge input
            if ($with_input) {
                return $redirection->withInput();
            }

            return $redirection;
        }
    }

    public function index()
    {
        return view('ch-eshop::index');
    }
}
