<?php

namespace Creativehandles\ChEshop\Http\Controllers\PluginsControllers;

use Creativehandles\ChEshop\Http\Requests\ProductCreateRequest;
use Creativehandles\ChEshop\Http\Requests\ProductUpdateRequest;
use Creativehandles\ChEshop\Models\ChEshopAttachment;
use Creativehandles\ChEshop\Models\Product;
use Creativehandles\ChEshop\Models\Variant;
use Creativehandles\ChEshop\Repositories\ProductRepository;
use Creativehandles\ChEshop\Services\LocalesService;
use Creativehandles\ChEshop\Services\ProductService;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Log;
use Throwable;

class ProductController extends EShopController
{
    private ProductService $productService;
    private ProductRepository $productRepository;
    private LocalesService $localesService;

    private static array $productCreatePageTabs;

    const BASE_VIEW_PATH = 'products/';

    public function __construct(
        ProductService $productService,
        ProductRepository $productRepository,
        LocalesService $localesService
    ) {
        $this->productRepository = $productRepository;
        $this->productService = $productService;
        $this->localesService = $localesService;

        self::$productCreatePageTabs = [
            [
                'active' => 'active',
                'body' => '#basicInfo',
                'icon' => 'fa fa-info-circle',
                'title' => __('ch-eshop::product/product.tab.headings.basic_info')
            ],
            [
                'body' => '#options',
                'icon' => 'fa fa-bars',
                'title' => __('ch-eshop::product/product.tab.headings.options')
            ],
            [
                'body' => '#variants',
                'icon' => 'fa fa-cubes',
                'title' => __('ch-eshop::product/product.tab.headings.variants')
            ],
            [
                'body' => '#seo',
                'icon' => 'fa fa-line-chart ',
                'title' => __('ch-eshop::product/product.tab.headings.seo')
            ],
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        return $this->productService->render(self::BASE_VIEW_PATH . 'index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        return $this->productService->render(
            self::BASE_VIEW_PATH . 'create',
            [
                'productOptions' => $this->productRepository->getAllProductOptions(),
                'headings' => self::$productCreatePageTabs,
                'action' => route('admin.ch-eshop.product.store'),
                'locales' => [
                    'selectData' => collect(config('laravellocalization.supportedLocales'))
                        ->map(function ($item) {
                            return $item['name'];
                        })
                        ->toArray(),
                    'configData' => $this->localesService->getLocaleConfigurations(),
                ]
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductCreateRequest $request
     * @return JsonResponse|RedirectResponse|Redirector
     */
    public function store(ProductCreateRequest $request)
    {
        try {
            $current_locale = app()->getLocale();
            app()->setLocale($request->language);

            $product = $this->productService->storeProduct($request);

            app()->setLocale($current_locale);

            return $this->response(__('ch-eshop::product/product.create.created'), 201, ['product' => $product], false);
        } catch (Throwable $th) {
            Log::debug($th->getMessage(), $th->getTrace());
            return $this->response(__('ch-eshop::product/product.create.failed'), 500, ['developer_message' => $th->getMessage()]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Factory|View
     */
    public function edit($id)
    {
        $translationLocale = request()->get('translation_locale', app()->getLocale());
        $product = $this->productService->getProduct($id, ['optionValues.option', 'variants.attachments']);
        $product->setLocale($translationLocale);

        return $this->productService->render(
            self::BASE_VIEW_PATH . 'create',
            [
                'product' => $product,
                'productOptions' => $this->productRepository->getAllProductOptions(),
                'headings' => self::$productCreatePageTabs,
                'action' => route('admin.ch-eshop.product.update', ['product' => $id, 'translation_locale' => $translationLocale]),
                'locales' => [
                    'selectData' => collect(config('laravellocalization.supportedLocales'))
                        ->map(function ($item) {
                            return $item['name'];
                        })
                        ->toArray(),
                    'configData' => $this->localesService->getLocaleConfigurations(),
                ]
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProductUpdateRequest $request
     * @param Product $product
     * @return JsonResponse|RedirectResponse|Redirector
     */
    public function update(ProductUpdateRequest $request, Product $product)
    {
        try {
            $current_locale = app()->getLocale();
            app()->setLocale($request->language);

            $product = $this->productService->updateProduct($request, $product);

            app()->setLocale($current_locale);

            return $this->response(__('ch-eshop::product/product.edit.updated'), 200, ['product' => $product], false);
        } catch (Throwable $th) {
            Log::debug($th->getMessage(), $th->getTrace());

            return $this->response(__('ch-eshop::product/product.edit.failed'), 500, ['developer_message' => $th->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Product $product
     * @return JsonResponse|RedirectResponse|Redirector
     */
    public function destroy(Product $product)
    {
        try {
            $this->productService->deleteProduct($product);
            return $this->response(__('ch-eshop::product/product.edit.updated'), 200, null, false);
        } catch (Throwable $th) {
            return $this->response(__('ch-eshop::product/product.edit.failed'), 500, ['developer_message' => $th->getMessage()]);
        }
    }

    public function deleteVariant(Variant $variant)
    {
        try {
            $this->productService->deleteVariant($variant);
            return $this->response(__('ch-eshop::product/product.edit.updated'), 200, null, false);
        } catch (Throwable $th) {
            return $this->response(__('ch-eshop::product/product.edit.failed'), 500, ['developer_message' => $th->getMessage()]);
        }
    }
}
