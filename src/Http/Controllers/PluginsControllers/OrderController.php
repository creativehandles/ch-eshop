<?php

namespace Creativehandles\ChEshop\Http\Controllers\PluginsControllers;

use Creativehandles\ChEshop\Http\Requests\CreateOrderRequest;
use Creativehandles\ChEshop\Http\Requests\OrderAddItemRequest;
use Creativehandles\ChEshop\Http\Requests\UpdateOrderRequest;
use Creativehandles\ChEshop\Models\Order;
use Creativehandles\ChEshop\Models\OrderItem;
use Creativehandles\ChEshop\Models\Status;
use Creativehandles\ChEshop\Services\DeliveryMethodsService;
use Creativehandles\ChEshop\Services\LocalesService;
use Creativehandles\ChEshop\Services\OrderService;
use Creativehandles\ChEshop\Services\PaymentMethodsService;
use Creativehandles\ChEshop\Services\ProductService;
use Creativehandles\ChEshop\Services\StatusService;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Log;
use Throwable;

class OrderController extends EShopController
{
    private OrderService $orderService;
    private ProductService $productService;
    private StatusService $statusService;
    private LocalesService $localeService;
    private DeliveryMethodsService $deliveryMethodsService;
    private PaymentMethodsService $paymentMethodsService;

    const BASE_VIEW_PATH = 'orders/';

    public function __construct(
        OrderService $orderService,
        ProductService $productService,
        StatusService $statusService,
        LocalesService $localeService,
        DeliveryMethodsService $deliveryMethodsService,
        PaymentMethodsService $paymentMethodsService
    ) {
        $this->orderService = $orderService;
        $this->productService = $productService;
        $this->statusService = $statusService;
        $this->localeService = $localeService;
        $this->deliveryMethodsService = $deliveryMethodsService;
        $this->paymentMethodsService = $paymentMethodsService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        return $this->orderService->render(self::BASE_VIEW_PATH . 'index', [
            'statuses' => $this->statusService->getSystemStatuses()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        $localeData = $this->localeService->getLocaleConfigurations();
        $deliveryMethods = $this->deliveryMethodsService->getDeliveryMethodConfigurations();
        $paymentMethods = $this->paymentMethodsService->getPaymentMethodConfigurations();
        $countryData = collect($localeData)
            ->map(function ($item) {
                return $item['country'] . ' - ' . $item['localeCode'];
            })->toArray();
        $deliveryMethodsSelectData = collect($deliveryMethods)
            ->map(function ($item) {
                return __('ch-eshop::config.deliveryMethods.' . $item['delivery_method']);
            })->toArray();
        $paymentMethodsSelectData = collect($paymentMethods)
            ->map(function ($item) {
                return __('ch-eshop::config.paymentMethods.' . $item['payment_method']);
            })->toArray();

        return $this->orderService->render(self::BASE_VIEW_PATH . 'create', [
            'localeData' => $localeData,
            'deliveryMethods' => $deliveryMethods,
            'paymentMethods' => $paymentMethods,
            'countryData' => $countryData,
            'deliveryMethodsSelectData' => $deliveryMethodsSelectData,
            'paymentMethodsSelectData' => $paymentMethodsSelectData,
            'headings' => [
                [
                    'active' => 'active',
                    'body' => '#order_details',
                    'icon' => 'fa fa-info-circle',
                    'title' => __('ch-eshop::order/create.tab.headings.order_details')
                ],
                [
                    'body' => '#customer_details',
                    'icon' => 'fa fa-file-text-o',
                    'title' => __('ch-eshop::order/create.tab.headings.customer_details')
                ],
                [
                    'body' => '#notes',
                    'icon' => 'fa fa-picture-o',
                    'title' => __('ch-eshop::order/create.tab.headings.notes')
                ],
            ],
            'action' => route('admin.ch-eshop.order.store'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateOrderRequest $request
     * @return JsonResponse|RedirectResponse|Redirector
     */
    public function store(CreateOrderRequest $request)
    {
        try {
            $data = $request->validated();
            $order = $this->orderService->store($data);

            return $this->response(
                __('ch-eshop::order/create.created'),
                201,
                ['order' => $order],
                false,
                route('admin.ch-eshop.order.edit', ['order' => $order->id])
            );
        } catch (Throwable $th) {
            return $this->response(__('ch-eshop::order/create.failed'), 500, ['developer_message' => $th->getMessage()]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Order  $order
     * @return Factory|View
     */
    public function edit(Order $order)
    {
        $localeData = $this->localeService->getLocaleConfigurations();
        $deliveryMethods = $this->deliveryMethodsService->getDeliveryMethodConfigurations();
        $paymentMethods = $this->paymentMethodsService->getPaymentMethodConfigurations();
        $countryData = collect($localeData)
            ->map(function ($item) {
                return $item['country'] . ' - ' . $item['localeCode'];
            })->toArray();
        $deliveryMethodsSelectData = collect($deliveryMethods)
            ->map(function ($item) {
                return __('ch-eshop::config.deliveryMethods.' . $item['delivery_method']);
            })->toArray();
        $paymentMethodsSelectData = collect($paymentMethods)
            ->map(function ($item) {
                return __('ch-eshop::config.paymentMethods.' . $item['payment_method']);
            })->toArray();
        $orderItems = [];

        foreach ($this->productService->getAllVariantOrderItemData() as $key => $value) {
            if (isset($value['product'])) {
                $orderItems[$value['id']] = $value['product']['title'] . '(' . $value['type'] . ') - ' . $value['code'];
            }
        }

        return $this->orderService->render(self::BASE_VIEW_PATH . 'create', [
            'localeData' => $localeData,
            'deliveryMethods' => $deliveryMethods,
            'paymentMethods' => $paymentMethods,
            'countryData' => $countryData,
            'deliveryMethodsSelectData' => $deliveryMethodsSelectData,
            'paymentMethodsSelectData' => $paymentMethodsSelectData,
            'order' => $order,
            'orderItems' => $orderItems,
            'headings' => [
                [
                    'active' => 'active',
                    'body' => '#order_details',
                    'icon' => 'fa fa-info-circle',
                    'title' => __('ch-eshop::order/create.tab.headings.order_details')
                ],
                [
                    'body' => '#customer_details',
                    'icon' => 'fa fa-file-text-o',
                    'title' => __('ch-eshop::order/create.tab.headings.customer_details')
                ],
                [
                    'body' => '#notes',
                    'icon' => 'fa fa-picture-o',
                    'title' => __('ch-eshop::order/create.tab.headings.notes')
                ],
                [
                    'body' => '#orderItems',
                    'icon' => 'fa fa-list',
                    'title' => __('ch-eshop::order/create.tab.headings.order_items')
                ],
            ],
            'action' => route('admin.ch-eshop.order.update', ['order' => $order->id]),
            'modify' => true,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateOrderRequest $request
     * @param Order $order
     * @return JsonResponse|RedirectResponse|Redirector
     */
    public function update(UpdateOrderRequest $request, Order $order)
    {
        try {
            $order = $this->orderService->update($request, $order);

            return $this->response(__('ch-eshop::order/edit.updated'), 200, ['order' => $order], false);
        } catch (Throwable $th) {
            Log::error($th->getMessage(), $th->getTrace());
            return $this->response(__('ch-eshop::order/edit.failed'), 500, ['developer_message' => $th->getMessage()]);
        }
    }

    public function changeOrderState(Order $order, Status $status)
    {
        try {
            $order = $this->orderService->changeOrderState($status, $order);

            return $this->response(__('ch-eshop::order/edit.updated'), 200, ['order' => $order], false);
        } catch (Throwable $th) {
            return $this->response(__('ch-eshop::order/edit.failed'), 500, ['developer_message' => $th->getMessage()]);
        }
    }


    public function addOrderItem(OrderAddItemRequest $request, Order $order)
    {
        try {
            $data = $request->validated();
            $order = $this->orderService->addBulkItem([$data['item']], $order);

            return $this->response(__('ch-eshop::order/edit.updated'), 200, ['order' => $order], false);
        } catch (Throwable $th) {
            return $this->response(__('ch-eshop::order/edit.failed'), 500, ['developer_message' => $th->getMessage()]);
        }
    }

    public function removeOrderItem(Order $order, OrderItem $orderItem)
    {
        try {
            $this->orderService->removeItemToOrder($order, $orderItem);

            return $this->response(__('ch-eshop::order/edit.updated'), 200, null, false);
        } catch (Throwable $th) {
            return $this->response(__('ch-eshop::order/edit.failed'), 500, ['developer_message' => $th->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Order $order
     * @return Response
     */
    public function destroy(Order $order)
    {
        try {
            $this->orderService->delete($order);
            return $this->response(__('ch-eshop::order/edit.updated'), 200, null, false);
        } catch (Throwable $th) {
            return $this->response(__('ch-eshop::order/edit.failed'), 500, ['developer_message' => $th->getMessage()]);
        }
    }

    
    public function createAfbRecord(Order $order)
    {
        try {
            $response = $this->orderService->createNewOrderInFlexibee($order);

            if (\Arr::get($response, 'winstrom.success')) {
                return $this->response(__('ch-eshop::table.abf.success'), 200,  $response, false);
            }
            //for the errors we dont have option to translate the texts. errors are directly from ABF end
            return $this->response(str_replace("\n", " ", \Arr::get($response, 'winstrom.results.0.errors.0.message')), 500, ['developer_message' => $response]);
        } catch (Throwable $th) {
            \Log::error($th);
            return $this->response(__('ch-eshop::table.exceptions.error-occured'), 500, ['developer_message' => $th->getMessage()]);
        }
    }

    public function downloadInvoice(Order $order)
    {

        try {
            $path = $this->orderService->downloadInvoice($order);
           
            return response()->download($path);
        } catch (Throwable $th) {
            \Log::error($th);
            return $this->response(__('ch-eshop::table.exceptions.filenotfound'), 500, ['developer_message' => $th->getMessage()]);
        }
    }
   
    
    public function updateAfbRecord(Order $order){
        try {
            $response = $this->orderService->updateOrderInFlexibee($order);

            if (\Arr::get($response, 'winstrom.success')) {
                return $this->response(__('ch-eshop::table.abf.success'), 200,  $response, false);
            }
            //for the errors we dont have option to translate the texts. errors are directly from ABF end
            return $this->response(str_replace("\n", " ", \Arr::get($response, 'winstrom.results.0.errors.0.message')), 500, ['developer_message' => $response]);
        } catch (Throwable $th) {
            \Log::error($th);
            return $this->response(__('ch-eshop::table.exceptions.error-occured'), 500, ['developer_message' => $th->getMessage()]);
        }
    }
}
