<?php

namespace Creativehandles\ChEshop\Http\Controllers\APIControllers\PluginsControllers\ChEShop;

use Creativehandles\ChEshop\Http\Requests\CreateAPIOrderRequest;
use App\Services\Response\ExternalApiResponse;
use Creativehandles\ChEshop\Http\Requests\UpdateOrderStatusApiRequest;
use Creativehandles\ChEshop\Http\Resources\OrderResource;
use Creativehandles\ChEshop\Repositories\OrderRepository;
use Creativehandles\ChEshop\Repositories\StatusRepository;
use Creativehandles\ChEshop\Services\OrderService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Throwable;

/**
 * @group Orders
 *
 * API endpoints for orders
 */
class OrderAPIController extends BaseApiController
{
    private OrderService $orderService;
    private OrderRepository $orderRepository;
    private StatusRepository $statusRepository;

    public function __construct(
        ExternalApiResponse $apiResponse,
        OrderService $orderService,
        OrderRepository $orderRepository,
        StatusRepository $statusRepository
    ) {
        parent::__construct($apiResponse);

        $this->orderService = $orderService;
        $this->orderRepository = $orderRepository;
        $this->statusRepository = $statusRepository;
    }

    /**
     * Create an order
     *
     * @responseFile 200 vendor/creativehandles/ch-eshop/storage/app/api/order/store.json
     * @responseFile 422 vendor/creativehandles/ch-eshop/storage/app/api/responses/422.json
     * @responseFile 500 vendor/creativehandles/ch-eshop/storage/app/api/responses/500.json
     *
     * @param CreateAPIOrderRequest $request
     * @return JsonResponse
     */
    public function store(CreateAPIOrderRequest $request)
    {
        try {
            $data = $request->validated();

            return DB::transaction(function() use ($data) {
                $order = $this->orderService->store($data);

                $this->orderService->addBulkItem($data['items'], $order);

                $this->orderService->triggerNewOrderNotifications($order);
                
                $this->orderService->createNewOrderInFlexibee($order);

                return $this->apiResponse->success(new OrderResource($order), 201);
            });
        } catch (Throwable $th) {
            return $this->apiResponse->failed($th->getMessage());
        }
    }

    /**
     * Update order status
     *
     * @responseFile 200 vendor/creativehandles/ch-eshop/storage/app/api/responses/200.json
     * @responseFile 422 vendor/creativehandles/ch-eshop/storage/app/api/responses/422.json
     * @responseFile 500 vendor/creativehandles/ch-eshop/storage/app/api/responses/500.json
     *
     * @param UpdateOrderStatusApiRequest $request
     * @param $orderId
     * @return JsonResponse
     */
    public function updateOrderStatus(UpdateOrderStatusApiRequest $request, $orderId)
    {
        try {
            $order = $this->orderRepository->byId($orderId);
            $data = $request->validated();
            $statusId = $data['status_id'];

            if ($order->ch_eshop_status_id == $statusId) {
                return $this->apiResponse->success();
            }

            $status = $this->statusRepository->byId($statusId);
            $order = $this->orderService->changeOrderState($status, $order);

            return $this->apiResponse->success();
        } catch (ModelNotFoundException $ex) {
            return $this->apiResponse->failed($ex->getMessage(), 404);
        } catch (Throwable $th) {
            return $this->apiResponse->failed($th->getMessage());
        }
    }
}
