<?php

namespace Creativehandles\ChEshop\Http\Controllers\APIControllers\PluginsControllers\ChEShop;

use App\Services\Response\ExternalApiResponse;
use Creativehandles\ChEshop\Http\Resources\ProductOptionResource;
use Creativehandles\ChEshop\Repositories\ProductOptionsRepository;
use Illuminate\Http\JsonResponse;
use Throwable;

/**
 * @group Product options
 *
 * API endpoints for product options
 */
class ProductOptionsController extends BaseApiController
{
    protected ProductOptionsRepository $productOptionsRepository;

    public function __construct(ExternalApiResponse $apiResponse, ProductOptionsRepository $productOptionsRepository)
    {
        parent::__construct($apiResponse);

        $this->productOptionsRepository = $productOptionsRepository;
    }

    /**
     * Get all product options
     *
     * @return JsonResponse
     * @responseFile 200 vendor/creativehandles/ch-eshop/storage/app/api/productOptions/index.json
     */
    public function index()
    {
        try {
            $productOptions = $this->productOptionsRepository->all();

            return $this->apiResponse->success(ProductOptionResource::collection($productOptions), 200);
        } catch (Throwable $th) {
            return $this->apiResponse->failed($th->getMessage());
        }
    }
}
