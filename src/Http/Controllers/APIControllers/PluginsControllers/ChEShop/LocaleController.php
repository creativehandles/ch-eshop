<?php

namespace Creativehandles\ChEshop\Http\Controllers\APIControllers\PluginsControllers\ChEShop;

use App\Services\Response\ExternalApiResponse;
use Creativehandles\ChEshop\Services\LocalesService;

/**
 * @group Locales
 *
 * API endpoints for locales.
 */
class LocaleController extends BaseApiController
{
    public LocalesService $localesService;

    public function __construct(ExternalApiResponse $apiResponse, LocalesService $localesService)
    {
        parent::__construct($apiResponse);

        $this->localesService = $localesService;
    }

    /**
     * Get locale configurations
     *
     * @responseFile 200 vendor/creativehandles/ch-eshop/storage/app/api/locales/index.json
     * @responseFile 500 vendor/creativehandles/ch-eshop/storage/app/api/responses/500.json
     */
    public function index()
    {
        $data = $this->localesService->getLocaleConfigurations();

        return $this->apiResponse->success(collect($data)->values());
    }
}
