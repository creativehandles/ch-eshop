<?php

namespace Creativehandles\ChEshop\Http\Controllers\APIControllers\PluginsControllers\ChEShop;

use App\Services\Response\ExternalApiResponse;
use Creativehandles\ChEshop\Services\DeliveryMethodsService;

/**
 * @group Delivery methods
 *
 * API endpoints for delivery methods.
 */
class DeliveryMethodsController extends BaseApiController
{
    public DeliveryMethodsService $deliveryMethodsService;

    public function __construct(ExternalApiResponse $apiResponse, DeliveryMethodsService $deliveryMethodsService)
    {
        parent::__construct($apiResponse);

        $this->deliveryMethodsService = $deliveryMethodsService;
    }

    /**
     * Get delivery methods
     *
     * @responseFile 200 vendor/creativehandles/ch-eshop/storage/app/api/deliveryMethods/index.json
     * @responseFile 500 vendor/creativehandles/ch-eshop/storage/app/api/responses/500.json
     */
    public function index()
    {
        $data = $this->deliveryMethodsService->getDeliveryMethodConfigurations();

        return $this->apiResponse->success(collect($data)->values());
    }
}
