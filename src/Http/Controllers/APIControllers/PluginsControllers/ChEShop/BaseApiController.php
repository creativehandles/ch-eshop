<?php

namespace Creativehandles\ChEshop\Http\Controllers\APIControllers\PluginsControllers\ChEShop;

use App\Http\Controllers\Controller;
use App\Services\Response\ExternalApiResponse;

class BaseApiController extends Controller
{
    protected ExternalApiResponse $apiResponse;
    protected string $reqLocale;

    public function __construct(ExternalApiResponse $apiResponse)
    {
        $this->reqLocale = request()->headers->get('Accept-Language', app()->getLocale());

        app()->setLocale($this->reqLocale);

        $this->apiResponse = $apiResponse;
    }
}
