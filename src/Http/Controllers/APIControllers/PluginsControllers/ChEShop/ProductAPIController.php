<?php

namespace Creativehandles\ChEshop\Http\Controllers\APIControllers\PluginsControllers\ChEShop;

use App\Services\Response\ExternalApiResponse;
use Creativehandles\ChEshop\Http\Resources\ProductResource;
use Creativehandles\ChEshop\Models\Product;
use Creativehandles\ChEshop\Repositories\ProductRepository;
use Creativehandles\ChEshop\Services\ProductService;
use Illuminate\Http\JsonResponse;
use Throwable;

/**
 * @group Products
 *
 * API endpoints for products
 */
class ProductAPIController extends BaseApiController
{
    protected ProductService $productService;

    public function __construct(ExternalApiResponse $apiResponse, ProductService $productService)
    {
        parent::__construct($apiResponse);

        $this->productService = $productService;
    }

    /**
     * Get all products
     *
     * @return JsonResponse
     * @responseFile 200 vendor/creativehandles/ch-eshop/storage/app/api/product/index.json
     */
    public function index()
    {
        try {
            $products = $this->productService->getAllProduct();

            return $this->apiResponse->success(ProductResource::collection($products), 200);
        } catch (Throwable $th) {
            return $this->apiResponse->failed($th->getMessage());
        }
    }

    /**
     * Get a product
     *
     * @param Product $product
     * @return JsonResponse
     *
     * @responseFile 200 vendor/creativehandles/ch-eshop/storage/app/api/product/show.json
     * @responseFile 404 vendor/creativehandles/ch-eshop/storage/app/api/product/404.json
     */
    public function show(Product $product)
    {
        try {
            $product->loadMissing(ProductRepository::RELATIONS);

            return $this->apiResponse->success(new ProductResource($product), 200);
        } catch (Throwable $th) {
            return $this->apiResponse->failed($th->getMessage());
        }
    }
}
