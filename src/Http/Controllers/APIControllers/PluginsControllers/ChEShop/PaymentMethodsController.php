<?php

namespace Creativehandles\ChEshop\Http\Controllers\APIControllers\PluginsControllers\ChEShop;

use App\Services\Response\ExternalApiResponse;
use Creativehandles\ChEshop\Services\LocalesService;
use Creativehandles\ChEshop\Services\PaymentMethodsService;

/**
 * @group Payment methods
 *
 * API endpoints for payment methods.
 */
class PaymentMethodsController extends BaseApiController
{
    public PaymentMethodsService $paymentMethodsService;

    public function __construct(ExternalApiResponse $apiResponse, PaymentMethodsService $paymentMethodsService)
    {
        parent::__construct($apiResponse);

        $this->paymentMethodsService = $paymentMethodsService;
    }

    /**
     * Get payment method configurations
     *
     * @responseFile 200 vendor/creativehandles/ch-eshop/storage/app/api/paymentMethods/index.json
     * @responseFile 500 vendor/creativehandles/ch-eshop/storage/app/api/responses/500.json
     */
    public function index()
    {
        $data = $this->paymentMethodsService->getPaymentMethodConfigurations();

        return $this->apiResponse->success(collect($data)->values());
    }
}
