<?php

namespace Creativehandles\ChEshop\Notifications;

use Creativehandles\ChEshop\Models\Order;
use Creativehandles\ChEshop\Services\LocalesService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class NewOrderAdminNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public Order $order;
    public LocalesService $localeService;

    /**
     * Create a new notification instance.
     *
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
        $this->localeService = app(LocalesService::class);
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {

        return (new MailMessage)
        ->subject(__('ch-eshop::emails.subject-new',['order_no'=>$this->order->order_no]))
        //instructed to use the same template untill we get a proper one
        ->markdown('ch-eshop::emails.newOrderNotification', ['order' => $this->order, 'localeService' => $this->localeService]);

    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
