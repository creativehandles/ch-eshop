<?php

namespace Creativehandles\ChEshop\Notifications;

use Creativehandles\ChEshop\Models\Order;
use Creativehandles\ChEshop\Models\Status;
use Creativehandles\ChEshop\Services\LocalesService;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class OrderStatusChangeNotification extends Notification
{
    use Queueable;

    public Order $order;
    public LocalesService $localeService;

    /**
     * Create a new notification instance.
     *
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
        $this->localeService = app(LocalesService::class);
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        //got instuctions to prepare just cancel and sent statuses
        switch ($this->order->status->id) {
            case Status::CANCELED:
                $markdown = "canceledOrderCustomerNotification";
                $subject = __('ch-eshop::emails.subject-canceled', ['order_no' => $this->order->order_no]);
                break;
            case Status::SENT:
                $markdown = "sentOrderCustomerNotification";
                $subject = __('ch-eshop::emails.subject-sent', ['order_no' => $this->order->order_no]);
                break;
            case Status::FAILED:
                $markdown = "failedOrderCustomerNotification";
                $subject = __('ch-eshop::emails.subject-failed', ['order_no' => $this->order->order_no]);
                break;
            default:
                $markdown =  false;
        }

        if (!$markdown) {
            return;
        }

        return (new MailMessage)->subject($subject)
            ->markdown('ch-eshop::emails.' . $markdown, ['order' => $this->order, 'localeService' => $this->localeService]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
