<?php

namespace Creativehandles\ChEshop;

use Creativehandles\ChEshop\Console\Commands\BuildEshopCommand;
use Creativehandles\ChEshop\Console\Commands\UpdateStocksCommand;
use Creativehandles\ChEshop\Http\Livewire\AddOrderItem;
use Creativehandles\ChEshop\Http\Livewire\ChangeOrderState;
use Creativehandles\ChEshop\Http\Livewire\ChEshopOrderItemTable;
use Creativehandles\ChEshop\Http\Livewire\ChEshopOrderTable;
use Creativehandles\ChEshop\Http\Livewire\ChEshopProductTable;
use Creativehandles\ChEshop\Http\Livewire\DeleteProductAttachment;
use Creativehandles\ChEshop\Http\Livewire\DeleteVariant;
use Creativehandles\ChEshop\Http\Livewire\OrderPrice;
use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Support\ServiceProvider;
use Livewire\Livewire;

class ChEshopServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        /*
         * Optional methods to load your package assets
         */
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang/ch-eshop', 'ch-eshop');
        /**
         * Cannot load overridden livewire-powergrid lang files from within the package,
         * since the original package, LivewirePowerGrid is loading it's lang files
         * into the same namespace.
         * Should publish the overridden lang files instead.
         */
//        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang/livewire-powergrid', 'livewire-powergrid');
        $this->loadViewsFrom(__DIR__ . '/../resources/views/ch-eshop', 'ch-eshop');
        $this->loadViewsFrom(__DIR__ . '/../resources/views/livewire-powergrid', 'livewire-powergrid');
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->loadRoutesFrom(__DIR__ . '/../routes/api.php');
        $this->loadRoutesFrom(__DIR__ . '/../routes/plugins.php');

        // registering package commands.
        if ($this->app->runningInConsole()) {
            $this->commands([BuildEshopCommand::class]);
            $this->commands([UpdateStocksCommand::class]);
        }

        // manually register livewire components
        Livewire::component('add-order-item', AddOrderItem::class);
        Livewire::component('change-order-state', ChangeOrderState::class);
        Livewire::component('ch-eshop-order-item-table', ChEshopOrderItemTable::class);
        Livewire::component('ch-eshop-order-table', ChEshopOrderTable::class);
        Livewire::component('ch-eshop-product-table', ChEshopProductTable::class);
        Livewire::component('delete-product-attachment', DeleteProductAttachment::class);
        Livewire::component('delete-variant', DeleteVariant::class);
        Livewire::component('order-price', OrderPrice::class);

        if (class_exists(Breadcrumbs::class)) {
            require __DIR__ . '/../routes/breadcrumbs.php';
        }

        $this->publishes(
            [__DIR__ . '/../config/config.php' => config_path('ch-eshop.php')],
            'ch-eshop-config'
        );

        // publishing the translation files.
        $this->publishes(
            [__DIR__ . '/../resources/lang/ch-eshop' => resource_path('lang/vendor/ch-eshop')],
            'ch-eshop-lang'
        );

        $this->publishes(
            [__DIR__ . '/../resources/lang/livewire-powergrid' => resource_path('lang/vendor/livewire-powergrid')],
            'ch-eshop-livewire-powergrid-lang'
        );

        // publishing the views.
        $this->publishes(
            [__DIR__ . '/../resources/views/ch-eshop' => resource_path('views/vendor/ch-eshop')],
            'ch-eshop-views'
        );

        $this->publishes(
            [__DIR__ . '/../resources/views/livewire-powergrid' => resource_path('views/vendor/livewire-powergrid')],
            'ch-eshop-livewire-powergrid-views'
        );
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__ . '/../config/config.php', 'ch-eshop');
        $this->mergeConfigFrom(__DIR__ . '/../config/livewire-powergrid.php', 'livewire-powergrid');
    }
}
