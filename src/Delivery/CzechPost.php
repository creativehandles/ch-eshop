<?php

namespace Creativehandles\ChEshop\Delivery;

use Creativehandles\ChEshop\Models\Order;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Throwable;

class CzechPost
{
    protected $api_token;
    protected $secret_key;
    protected $nonce;
    protected $auth_timestamp;
    protected $payload_hash;
    protected $is_post = false;
    protected $url;
    protected $payload;
    protected $endpoint;
    protected $idcck;

    protected $headers;
    protected $signature;

    protected $response;
    protected $status_code;


    public function __construct()
    {
        $this->api_token = config('ch-eshop.czech_post.api_token');
        $this->secret_key = config('ch-eshop.czech_post.secret_key');
        $this->nonce = config('ch-eshop.czech_post.nonce');
        $this->auth_timestamp = config('ch-eshop.czech_post.auth_timestamp');
        $this->url =  config('ch-eshop.czech_post.url');
        $this->idcck =  config('ch-eshop.czech_post.idcck');
    }

    /**
     * Generate API Request Auth Signature and Hash
     */
    private function signature(): void
    {
        $auth_content = ";" . $this->auth_timestamp . ";" . $this->nonce;

        if ($this->is_post) {
            $this->payload_hash = hash('sha256', $this->payload);
            $auth_content = hash('sha256', $this->payload) . $auth_content;
        }

        $hash = hash_hmac('sha256', $auth_content, $this->secret_key, true); //hash
        $this->signature = base64_encode($hash);
    }

    /**
     * Set Required Request headers
     */
    private function headers(): void
    {
        $headers =  [
            "Content-Type: application/json;charset=UTF-8",
            "Api-Token: {$this->api_token}",
            "Authorization: CP-HMAC-SHA256 nonce=\"{$this->nonce}\" signature=\"{$this->signature}\"",
            "Authorization-Timestamp: {$this->auth_timestamp}"
        ];

        if ($this->is_post) {
            $headers[] = "Authorization-content-SHA256: $this->payload_hash";
        }

        $this->headers = $headers;
    }

    /**
     * Check Packages Statuses
     */
    public function parcelsStatuses(array $packageIds, $language = 'CZ')
    {
        $this->endpoint = "parcelStatus";
        return $this->call([
            'parcelIds' => $packageIds,
            'language' => $language
        ]);
    }

    /**
     * parcels statuses
     */
    public function parcelsStatusesOverview($from, $to)
    {
        $this->endpoint = "sendParcels/stats?dateFrom=$from&dateTo=$to";
        return $this->call();
    }

    /**
     * Send new parcel
     */
    public function sendParcels(array $payload)
    {
        if (config('ch-eshop.czech_post.in_test')) {
            $payload = json_decode(config('ch-eshop.czech_post.test_parcel_data'), true);
        }

        $this->endpoint = 'sendParcels';
        return $this->call($payload);
    }

    /**
     * Parcel send
     */
    public function getIdTransaction($parcelID)
    {
        $this->endpoint = 'sendParcels/idTransaction/' . $parcelID;
        return $this->call();
    }

    /**
     * Parcel history
     */
    public function getParcelHistory($idTransaction)
    {
        $this->endpoint = "parcelDataHistory/parcelID/$idTransaction";
        return $this->call();
    }

    /**
     * Send new parcel
     * @params https://www.ceskaposta.cz/en/napi/b2b#parcelPrinting
     */
    public function parcelLabels(array $payload)
    {
        $this->endpoint = 'parcelPrinting';
        return $this->call($payload);
    }

    /**
     * Set Request Payload
     */
    private function processPayload(array $payload)
    {
        $this->payload = json_encode($payload);
    }

    /**
     * Make API Call
     */
    private function call($with_payload = null)
    {
        try {
            if ($with_payload) {
                $this->is_post = true;
                $this->processPayload($with_payload);
            }

            $this->signature();
            $this->headers();

            $apiRequest = curl_init();
            curl_setopt($apiRequest, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($apiRequest, CURLOPT_HEADER, FALSE);
            curl_setopt($apiRequest, CURLOPT_HTTPHEADER, $this->headers);
            curl_setopt($apiRequest, CURLOPT_URL, $this->url . $this->endpoint);
            curl_setopt($apiRequest, CURLOPT_SSL_VERIFYPEER, false); // Only for testing!

            if ($this->is_post) {
                curl_setopt($apiRequest, CURLOPT_POST, true);
                curl_setopt($apiRequest, CURLOPT_POSTFIELDS, $this->payload);
            }

            $apiResponse = json_decode(curl_exec($apiRequest), true);
            $apiResponseCode = curl_getinfo($apiRequest, CURLINFO_HTTP_CODE);

            Log::debug('Api request', [
                'url' => $this->url . $this->endpoint,
                'payload' => $with_payload
            ]);
            Log::debug('Api response', [
                'url' => $this->url . $this->endpoint,
                'payload' => $apiResponse
            ]);

            curl_close($apiRequest);

            $this->response = $apiResponse;
            $this->status_code = $apiResponseCode;
        } catch (Throwable $th) {
            Log::error($th->getMessage(), $th->getTrace());

            $this->error = $th->getMessage();
            $this->status_code = $th->getCode();
        }

        return [
            'data' => $this->response,
            'status' => $this->status_code,
            'error_message' => isset(($this->response)[0]['message']) ? ($this->response)[0]['message'] : null,
        ];
    }

    // https://b2b-test.postaonline.cz:444/restservices/ZSKService/v1/location/idContract/{idContract}
    // location APIs
    public function createLocation( array $payload)
    {
        $this->endpoint = "location/idContract/$this->idcck";

        return $this->call($payload);
    }

    public function getSingleLocationByName($name = "Loonoy"){
        $locationArray = $this->getLocationList();
        $locationCollection = collect($locationArray['data']);

        return $locationCollection->where('location.locationName',$name)->first();
    }

    public function getLocationList()
    {
        $this->endpoint = "location/idContract/$this->idcck";

        return $this->call();
    }

    /**
     * Get parcel code
     * @param string $czechPostId
     * @return string
     * @throws Exception
     */
    public function getParcelCode(string $czechPostId)
    {
        $response = $this->getIdTransaction($czechPostId);
        $responseStatus = Arr::get($response, 'status');

        if ($responseStatus != 200) {
            throw new Exception(__(
                'ch-eshop::czech-post.Fetching parcel code returned status',
                [
                    'status' => $responseStatus,
                ]
            ));
        }

        $parcelCode = trim(Arr::get($response, 'data.ResultSendParcelsList.0.parcelCode'));

        if (empty($parcelCode)) {
            throw new Exception(__(
                'ch-eshop::czech-post.Fetching parcel code returned error',
                [
                    'errorCode' => Arr::get($response, 'data.ResultSendParcelsList.0.parcelStateResponse.0.responseCode'),
                    'errorText' => Arr::get($response, 'data.ResultSendParcelsList.0.parcelStateResponse.0.responseText'),
                ]
            ));
        }

        return $parcelCode;
    }

    /**
     * Get label data of parcels
     * @param string|array $parcelCodes
     * @return string
     * @throws Exception
     */
    public function getLabelDataOfParcels($parcelCodes)
    {
        if (!is_array($parcelCodes)) {
            $parcelCodes = [$parcelCodes];
        }

        $payload = [
            "printingHeader" => [
                "customerID" => '12', // max length 6
                "contractNumber" => "07485485655",
                "idForm" => 101,
                "shiftHorizontal" => 2,
                "shiftVertical" => 2,
                "position" => 1
            ],
            "printingData" => $parcelCodes
        ];
        $response = $this->parcelLabels($payload);
        $responseStatus = Arr::get($response, 'status');

        if ($responseStatus != 200) {
            throw new Exception(__(
                'ch-eshop::czech-post.Get label data of parcels returned status',
                [
                    'status' => $responseStatus,
                ]
            ));
        }

        $encodedLabelData = Arr::get($response, 'data.printingDataResult');
        $labelData = base64_decode($encodedLabelData, true);

        if (empty($labelData)) {
            throw new Exception(__('ch-eshop::czech-post.Get label data of parcels returned empty label data.'));
        }

        return $labelData;
    }
}
