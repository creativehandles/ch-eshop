<?php

namespace Creativehandles\ChEshop\Console\Commands;

use Creativehandles\ChEshop\Models\ProductOption;
use Creativehandles\ChEshop\Models\Status;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Composer;
use Illuminate\Support\Facades\DB;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class BuildEshopCommand extends Command
{
    protected $composer;
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ch-eshop:build';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will publish the necessary assets and prepare the environment';

    /**
     * Create a new command instance.
     *
     * @param Composer $composer
     */
    public function __construct(Composer $composer)
    {
        parent::__construct();

        $this->composer = $composer;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Publishing the package');
        $this->call(
            'vendor:publish',
            [
                '--provider' => 'Creativehandles\ChEshop\ChEshopServiceProvider',
                '--force' => true
            ]
        );

        $this->info('Regenerating the Composer autoloader files');
        $this->composer->dumpAutoloads();

        $this->info('Migrating the database');
        $this->call('migrate');

        $this->info('Seeding the database');
        $this->seedAdminMenus();
        $this->seedProductOptions();
        $this->seedOrderStatuses();

        $this->info('This package require following packages please read their documentation and do the necessary configuration');
        $this->info('Livewire: https://laravel-livewire.com/docs/2.x/quickstart');
        $this->info('Powergrid: https://livewire-powergrid.com/#/get-started/install');

//        $this->info('Publishing Livewire Powergrid resources');
//        Artisan::call('vendor:publish --tag=livewire-powergrid-config');
//        Artisan::call('vendor:publish --tag=livewire-powergrid-views');
//        Artisan::call('vendor:publish --tag=livewire-powergrid-lang');

        $this->info('Published the necessary assets and prepared the environment');
    }

    public function seedAdminMenus()
    {
        $json = file_get_contents(__DIR__.'/../../../storage/database/seeds/adminMenuItems.json');
        $menuItems = json_decode($json, true);

        foreach(LaravelLocalization::getSupportedLanguagesKeys() as $locale) {
            $menu = DB::table('admin_menus')->where(['name' => 'Sidebar-' . $locale])->first();

            if ($menu) {
                foreach ($menuItems as $menuItem) {
                    $menuItem = array_merge($menuItem, [
                        'label' => __(Arr::get($menuItem, 'label', ''), [], $locale),
                        'link' => Arr::get($menuItem, 'link', ''),
                        'menu' => $menu->id,
                        'role_id' => 2
                    ]);

                    DB::table('admin_menu_items')->updateOrInsert(
                        ['link' => $menuItem['link'], 'menu' => $menuItem['menu']],
                        $menuItem
                    );
                }
            }
        }
    }

    public function seedProductOptions()
    {
        $json = file_get_contents(__DIR__.'/../../../storage/database/seeds/productOptions.json');
        $options = json_decode($json, true);

        /**
         * I freaking don't like truncating and re-populating the table.
         * But the freaking updateOrCreate method is not working with the "name" column.
         * :(
         *
         * @todo improve the seeding thus truncating is not required.
         */
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        ProductOption::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        foreach ($options as $option) {
            ProductOption::updateOrCreate(
                ['name' => $option['name']],
                $option
            );
        }
    }

    public function seedOrderStatuses()
    {
        $json = file_get_contents(__DIR__.'/../../../storage/database/seeds/orderStatuses.json');
        $statuses = json_decode($json, true);

        // TODO: improve the seeding thus truncating is not required.
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Status::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        foreach ($statuses as $status) {
            Status::updateOrCreate(
                ['title' => $status['title']],
                $status
            );
        }
    }
}
