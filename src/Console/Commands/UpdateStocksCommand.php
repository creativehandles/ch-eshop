<?php

namespace Creativehandles\ChEshop\Console\Commands;

use Creativehandles\ChEshop\Services\FlexiBee\FlexiBeeService;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class UpdateStocksCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ch-eshop:update-stock-details {--code= : Add item code here to fetch only that item}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update each and every items\' status by fetching details from ABRA Flexi and update ESHOP';


    public function __construct()
    {
        parent::__construct();
    }


    public function handle(FlexiBeeService $flexiBeeService)
    {

        $code = $this->option('code', null);

        $this->info('Connecting to FlexiBee');

        //if single item, 
        if ($code) {
            $response = $flexiBeeService->getStockDetailsOfItem(strtoupper($code));
        } else {
            $response = $flexiBeeService->getStockDetails();
        }
        
        $this->info('Fetched data from FlexiBee');

        $stock = Arr::get($response, 'winstrom.skladova-karta', []);

        if (!count($stock)) {
            return $this->error('Empty response. Cannot proceed');
        }

        foreach ($stock as $item) {
            $status = $flexiBeeService->processStockDetails($item);

            if (!$status) {
                $this->error("Error occured when stock updating for :" . $item['cenik']);
                continue;
            }

            $this->info("Stocks updated for code:" . $item['cenik']);
        }
    }
}
