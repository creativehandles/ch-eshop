<?php

namespace Creativehandles\ChEshop\Services;

use Creativehandles\ChEshop\Models\Order;
use Creativehandles\ChEshop\Models\OrderItem;
use Creativehandles\ChEshop\Models\Status;
use Creativehandles\ChEshop\Notifications\NewOrderAdminNotification;
use Creativehandles\ChEshop\Notifications\NewOrderCustomerNotification;
use Creativehandles\ChEshop\Repositories\OrderRepository;
use Creativehandles\ChEshop\Repositories\ProductRepository;
use Creativehandles\ChEshop\Repositories\VariantRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use Creativehandles\ChEshop\Notifications\OrderStatusChangeNotification;
use Creativehandles\ChEshop\Services\FlexiBee\FlexiBeeService;
use Exception;
use Throwable;

class OrderService extends BaseEshopService
{
    private VariantRepository $variantRepository;
    private ProductRepository $productRepository;
    private OrderRepository $orderRepository;
    private DeliveryMethodsService $deliveryMethodsService;
    private PaymentMethodsService $paymentMethodsService;
    private LocalesService $localesService;
    private FlexiBeeService $flexiBeeService;

    public function __construct(
        VariantRepository $variantRepository,
        ProductRepository $productRepository,
        OrderRepository $orderRepository,
        DeliveryMethodsService $deliveryMethodsService,
        PaymentMethodsService $paymentMethodsService,
        LocalesService $localesService,
        FlexiBeeService $flexiBeeService
    ) {
        $this->variantRepository = $variantRepository;
        $this->productRepository = $productRepository;
        $this->orderRepository = $orderRepository;
        $this->deliveryMethodsService = $deliveryMethodsService;
        $this->paymentMethodsService = $paymentMethodsService;
        $this->localesService = $localesService;
        $this->flexiBeeService = $flexiBeeService;
    }

    public function store($data)
    {
        if ($data instanceof Request) {
            $data = $data->validated();
        }

        $localeConfiguration = $this->localesService->getLocaleConfiguration($data['language']);
        $deliveryPriceWoTax = !empty($data['delivery_cost_wo_tax']) ? $data['delivery_cost_wo_tax'] : null;
        $paymentPriceWoTax = !empty($data['payment_price']) ? $data['payment_price'] : null;
        $deliveryPrices = $this->deliveryMethodsService->getPricesForDeliveryMethod($data['delivery_type'], $data['language'], $deliveryPriceWoTax);
        $paymentPrices = $this->paymentMethodsService->getPricesForPaymentMethod($data['payment_method'], $data['language'], $paymentPriceWoTax);
        $data['ch_eshop_status_id'] = Status::NEW_ORDER;
        $data['currency'] = Arr::get($localeConfiguration, 'currency.code');
        $data['country'] = Arr::get($localeConfiguration, 'country');
        $data['is_company'] = isset($data['is_company']) ? $data['is_company'] : false;
        $data['delivery_cost_wo_tax'] = $deliveryPrices['price_wo_tax'] ?? 0;
        $data['delivery_cost_w_tax'] = $deliveryPrices['price_w_tax'] ?? 0;
        $data['payment_price'] = $paymentPrices['price_wo_tax'] ?? 0;
        $data['payment_price_w_tax'] = $paymentPrices['price_w_tax'] ?? 0;
        $data['order_no'] = $this->generateNextOrderNo();

        return $this->orderRepository->create($data);
    }

    public function update($data, Order $order)
    {
        if ($data instanceof Request) {
            $data = $data->validated();
        }

        $deliveryPriceWoTax = !empty($data['delivery_cost_wo_tax']) ? $data['delivery_cost_wo_tax'] : null;
        $paymentPriceWoTax = !empty($data['payment_price']) ? $data['payment_price'] : null;
        $deliveryPrices = $this->deliveryMethodsService->getPricesForDeliveryMethod($data['delivery_type'], $data['language'], $deliveryPriceWoTax);
        $paymentPrices = $this->paymentMethodsService->getPricesForPaymentMethod($data['payment_method'], $data['language'], $paymentPriceWoTax);
        $data['is_company'] = isset($data['is_company']) ? $data['is_company'] : false;
        $data['delivery_cost_wo_tax'] = $deliveryPrices['price_wo_tax'] ?? 0;
        $data['delivery_cost_w_tax'] = $deliveryPrices['price_w_tax'] ?? 0;
        $data['payment_price'] = $paymentPrices['price_wo_tax'] ?? 0;
        $data['payment_price_w_tax'] = $paymentPrices['price_w_tax'] ?? 0;

        $order->update($data);
        $this->generateOrderPrices($order);

        return $order->refresh();
    }

    public function changeOrderState(Status $status, Order $order)
    {
        try {
            $order->ch_eshop_status_id = $status->id;
            $order->saveOrFail();

            $this->triggerOrderStatusChangeNotification($order);

            return $order;
        } catch (Throwable $th) {
            throw $th;
        }
    }

    public function triggerOrderStatusChangeNotification(Order $order)
    {
        if (!in_array(
            $order->ch_eshop_status_id,
            [Status::CANCELED, Status::SENT, Status::FAILED])
        ) {
            return;
        }

        $notification = new OrderStatusChangeNotification($order);
        $notification->locale($order->language);

        Notification::route('mail', $order->email)
            ->notify($notification);
    }

    public function triggerNewOrderNotifications(Order $order)
    {
        $customerNotification = new NewOrderCustomerNotification($order);
        $customerNotification->locale($order->language);

        $adminNotification = new NewOrderAdminNotification($order);
        $adminNotification->locale(config('app.admin_locale'));

        Notification::route('mail', $order->email)
            ->notify($customerNotification);
        Notification::route('mail', config('ch-eshop.notify_emails'))
            ->notify($adminNotification);
    }

    public function updateOrderItem(Order $order, array $data)
    {
        try {

            $current_locale = app()->getLocale();

            app()->setLocale($order->language);
            $res = $order->items()->where('id', $data['id'])->update(collect($data)->except(['id'])->toArray());
            $this->generateOrderPrices($order);
            app()->setLocale($current_locale);

            return $res;
        } catch (Throwable $th) {
            Log::error($th->getMessage());
            throw $th;
        }
    }

    /**
     * @param array $data
     * @param Order $order
     * @return boolean|Order
     * @throws Throwable
     */
    private function addItemToOrder(array $data, Order $order)
    {
        $current_locale = app()->getLocale();
        app()->setLocale($order->language ?? $current_locale);

        try {
            $variant = $this->variantRepository->find($data['ch_eshop_variant_id']);
            $product = $variant->product;

            if ($variant->qty_available < $data['quantity']) {
                throw new Exception(__(
                    'ch-eshop::order/create.Available quantity of the variant :variant is less than ordered quantity.',
                    ['variant' => $variant->code]
                ));
            }

            if (empty($variant->unit_price_wo_tax)) {
                throw new Exception(__(
                    'ch-eshop::order/create.A variant doesn\'t have price set for the locale :locale.',
                    ['locale' => $order->language]
                ));
            }

            $totalPriceWOTax = (float) $this->localesService->multiply($order->language, $variant->selling_price_wo_tax, $data['quantity'], true);
            $totalPriceWTax = (float) $this->localesService->multiply($order->language, $variant->selling_price_w_tax, $data['quantity'], true);

            $itemData = [
                'ch_eshop_variant_id' => $variant->id,
                'code' => $variant->code,
                'ean' => $variant->ean,
                'quantity' => $data['quantity'],
                'unit_price_wo_tax' => $variant->unit_price_wo_tax,
                'unit_price_w_tax' => $variant->unit_price_w_tax,
                'selling_price_wo_tax' => $variant->selling_price_wo_tax,
                'selling_price_w_tax' => $variant->selling_price_w_tax,
                'total_price_wo_tax' => $totalPriceWOTax,
                'total_price_w_tax' => $totalPriceWTax,
                'tax' => $totalPriceWTax - $totalPriceWOTax,
                'object' => $product->loadMissing('variants'),
            ];

            $order->items()->create($itemData);
            $variant->decrement('qty_available', $data['quantity']);

            return $order->refresh();
        } catch (Throwable $th) {
            throw $th;
        } finally {
            app()->setLocale($current_locale);
        }
    }

    public function addBulkItem(array $array, Order $order)
    {
        return DB::transaction(function () use ($array, $order) {
            foreach ($array as $item) {
                $this->addItemToOrder($item, $order);
            }

            $this->generateOrderPrices($order);

            return $order->refresh();
        });
    }

    public function removeItemToOrder(Order $order, OrderItem $orderItem)
    {
        try {

            $current_locale = app()->getLocale();
            app()->setLocale($order->language);

            $quantity = $orderItem->quantity;
            $orderItem->variant->increment('qty_available', $quantity);

            if (!$orderItem->delete()) {
                return true;
            }

            $this->generateOrderPrices($order);
            app()->setLocale($current_locale);

            return true;
        } catch (Throwable $th) {
            Log::error($th);
            throw $th;
        }
    }

    public function generateOrderPrices(Order $order)
    {
        $sums = $order->items()->select([
            DB::raw('SUM(total_price_wo_tax) as total_price_wo_tax'),
            DB::raw('SUM(tax) as tax'),
            DB::raw('SUM(total_price_w_tax) as total_price_w_tax'),
        ])->first();

        return $order->update([
            'total_cost_wo_tax' => $sums->total_price_wo_tax,
            'total_tax' => $sums->tax + ($order->delivery_cost_w_tax - $order->delivery_cost_wo_tax) + ($order->payment_price_w_tax - $order->payment_price),
            'total_cost_w_tax' => $sums->total_price_w_tax,
            'total_pay' => $sums->total_price_w_tax + $order->delivery_cost_w_tax + $order->payment_price_w_tax,
        ]);
    }

    public function delete($order)
    {
        try {
            if ($order instanceof Order) {
                return $order->delete();
            }
            return $this->orderRepository->deleteById($order);
        } catch (Throwable $th) {
            throw $th;
        }
    }

    /**
     * Generate the next order number
     *
     * @return string
     */
    public function generateNextOrderNo(): string
    {
        $lastOrder = $this->orderRepository->getSingle([], ['id' => 'DESC'], [], true);
        $firstOrderNo = config('ch-eshop.invoice_prefix') . '00001';

        if ($lastOrder === null) {
            return $firstOrderNo;
        }

        $numericLastOrderNo = str_replace(config('ch-eshop.invoice_prefix'), '', $lastOrder->order_no);

        if (!is_numeric($numericLastOrderNo)) {
            return $firstOrderNo;
        }

        return sprintf('%s%05d', config('ch-eshop.invoice_prefix'), ++$numericLastOrderNo);
    }

    public function createNewOrderInFlexibee(Order $order)
    {
        return $this->flexiBeeService->placeOrderInFlexiBee($order);
    }

    public function downloadInvoice(Order $order)
    {
        return $this->flexiBeeService->downloadPdf($order, $order->language, $order->id);
    }

    public function updateOrderInFlexibee(Order $order)
    {
        return $this->flexiBeeService->updateOrderInFlexiBee($order);
    }
}
