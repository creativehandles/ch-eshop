<?php

namespace Creativehandles\ChEshop\Services;

use Illuminate\Support\Arr;

class DeliveryMethodsService
{
    private array $deliveryMethodConfigurations;
    private LocalesService $localesService;

    public function __construct(LocalesService $localesService)
    {
        $json = file_get_contents(__DIR__.'/../../storage/app/config/delivery_methods.json');
        $this->deliveryMethodConfigurations = json_decode($json, true);

        $this->localesService = $localesService;
    }

    /**
     * Get delivery methods
     *
     * @return array [['delivery_method', 'prices': [['locale_code', 'tax_rate', 'price_wo_tax', 'price_w_tax']]]
     */
    public function getDeliveryMethodConfigurations()
    {
        return collect($this->deliveryMethodConfigurations)
            ->map(function ($deliveryMethod) {
                return [
                    'delivery_method' => Arr::get($deliveryMethod, 'delivery_method'),
                    'prices' => collect(Arr::get($deliveryMethod, 'prices', []))
                        ->map(function ($price) {
                            return [
                                'locale_code' => Arr::get($price, 'locale_code', ''),
                                'tax_rate' => Arr::get($price, 'tax_rate', 0),
                                'price_wo_tax' => Arr::get($price, 'price_wo_tax', 0),
                                'price_w_tax' => Arr::get($price, 'price_w_tax', 0),
                            ];
                        })
                        ->toArray(),
                ];
            })
            ->toArray();
    }

    /**
     * Get price for a delivery method
     *
     * @param string $deliveryMethod
     * @param string $locale
     * @param string|null $deliveryPriceWoTax
     * @return array ['tax_rate', 'price_wo_tax', 'price_w_tax']
     */
    public function getPricesForDeliveryMethod(string $deliveryMethod, string $locale, string $deliveryPriceWoTax = null)
    {
        $prices = Arr::get($this->getDeliveryMethodConfigurations(), $deliveryMethod . '.prices.' . $locale);

        if (empty($prices)) {
            return null;
        }

        $taxRate = Arr::get($prices, 'tax_rate');
        $priceWoTax = Arr::get($prices, 'price_wo_tax');
        $priceWithTax = Arr::get($prices, 'price_w_tax');

        if ($deliveryPriceWoTax !== null) {
            $priceWoTax = $deliveryPriceWoTax;
            $priceWithTax = $this->localesService->getTaxInclusivePrice($locale, $priceWoTax, $taxRate);
        }

        return [
            'tax_rate' => (float) $taxRate,
            'price_wo_tax' => (float) $priceWoTax,
            'price_w_tax' => (float) $priceWithTax,
        ];
    }
}