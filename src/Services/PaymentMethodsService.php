<?php

namespace Creativehandles\ChEshop\Services;

use Illuminate\Support\Arr;

class PaymentMethodsService
{
    private array $paymentMethodConfigurations;
    private LocalesService $localesService;

    public function __construct(LocalesService $localesService)
    {
        $json = file_get_contents(__DIR__.'/../../storage/app/config/paymentMethods.json');
        $this->paymentMethodConfigurations = json_decode($json, true);

        $this->localesService = $localesService;
    }

    /**
     * Get payment method configurations
     *
     * @return array [['payment_method', 'prices': [['locale_code', 'tax_rate', 'price_wo_tax', 'price_w_tax']]]
     */
    public function getPaymentMethodConfigurations()
    {
        return collect($this->paymentMethodConfigurations)
            ->map(function ($paymentMethod) {
                return [
                    'payment_method' => Arr::get($paymentMethod, 'payment_method'),
                    'prices' => collect(Arr::get($paymentMethod, 'prices', []))
                        ->map(function ($price) {
                            return [
                                'locale_code' => Arr::get($price, 'locale_code', ''),
                                'tax_rate' => Arr::get($price, 'tax_rate', 0),
                                'price_wo_tax' => Arr::get($price, 'price_wo_tax', 0),
                                'price_w_tax' => Arr::get($price, 'price_w_tax', 0),
                            ];
                        })
                        ->toArray(),
                ];
            })
            ->toArray();
    }

    /**
     * Get price for a payment method
     *
     * @param string $paymentMethod
     * @param string $locale
     * @param string|null $paymentPriceWoTax
     * @return array ['tax_rate', 'price_wo_tax', 'price_w_tax']
     */
    public function getPricesForPaymentMethod(string $paymentMethod, string $locale, string $paymentPriceWoTax = null)
    {
        $prices = Arr::get($this->getPaymentMethodConfigurations(), $paymentMethod . '.prices.' . $locale);

        if (empty($prices)) {
            return null;
        }

        $taxRate = Arr::get($prices, 'tax_rate');
        $priceWoTax = Arr::get($prices, 'price_wo_tax');
        $priceWithTax = Arr::get($prices, 'price_w_tax');

        if ($paymentPriceWoTax !== null) {
            $priceWoTax = $paymentPriceWoTax;
            $priceWithTax = $this->localesService->getTaxInclusivePrice($locale, $priceWoTax, $taxRate);
        }

        return [
            'tax_rate' => (float) $taxRate,
            'price_wo_tax' => (float) $priceWoTax,
            'price_w_tax' => (float) $priceWithTax,
        ];
    }
}