<?php

namespace Creativehandles\ChEshop\Services;


use Creativehandles\ChEshop\Repositories\StatusRepository;

class StatusService extends BaseEshopService
{
    protected $statusRepository;
    
    public function __construct(StatusRepository $statusRepository)
    {
        $this->statusRepository = $statusRepository;
    }
    
   public function getSystemStatuses()
   {
       return $this->statusRepository->all();
   }
}
