<?php

namespace Creativehandles\ChEshop\Services\FlexiBee;

use Creativehandles\ChEshop\Repositories\FlexibeeOrderReferenceRepository;
use Creativehandles\ChEshop\Services\FlexiBee\FlexiBeeClient;
use Carbon\Carbon;
use Creativehandles\ChEshop\Models\Order;
use Creativehandles\ChEshop\Repositories\VariantRepository;
use Creativehandles\ChEshop\Services\LocalesService;
use Exception;
use Illuminate\Support\Arr;

class FlexiBeeService
{

    protected $flexiBeeClient;
    protected $flexibeeOrderReferenceRepository;
    protected $variantRepository;

    public function __construct(
        FlexiBeeClient $flexiBeeClient,
        FlexibeeOrderReferenceRepository $flexibeeOrderReferenceRepository,
        VariantRepository $variantRepository
    ) {
        $this->flexiBeeClient = $flexiBeeClient;
        $this->flexibeeOrderReferenceRepository = $flexibeeOrderReferenceRepository;
        $this->variantRepository = $variantRepository;
    }


    /**
     * get all the items in flexibee
     */
    public function getStockDetails()
    {
        return $this->flexiBeeClient->processRequest("GET", "skladova-karta.json?detail=full", [], "json");
    }

    /**
     * get single stock detail in flexibee
     */
    public function getStockDetailsOfItem(string $code)
    {
        return $this->flexiBeeClient->processRequest("GET", "skladova-karta/(cenik='code:$code').json?detail=full", [], "json");
    }

    /**
     * get an order in flexibee
     */
    public function getOrderByOrderCode(string $orderCode, string $detail = null)
    {
        return $this->flexiBeeClient->processRequest("GET", "objednavka-prijata/(kod='$orderCode').json?detail=$detail", [], "json");
    }

    /**
     * create an order in flexibee
     */
    public function createOrder(array $orderData)
    {
        $payload = ['json' => $orderData];

        return $this->flexiBeeClient->processRequest("POST", "objednavka-prijata.json?detail=full", $payload, "json");
    }

    /**
     * create an order in flexibee
     */
    public function updateOrder(array $orderData)
    {
        $payload = ['json' => $orderData];

        return $this->flexiBeeClient->processRequest("PUT", "objednavka-prijata.json?detail=full", $payload, "json");
    }

    /**
     * synchronize quantity with flexibee
     */
    public function processStockDetails(array $stock)
    {
        //get item code
        $itemCode = str_replace("code:", "", Arr::get($stock, 'cenik'));

        if (!$itemCode) {
            \Log::error("Empty Item code. Cannot process", $stock);
            return false;
        }
        //get variant by code
        $varient =  $this->variantRepository->findBy('code', $itemCode);

        if (!$varient) {
            \Log::error("Cannot find varient for code $itemCode ", $stock);
            return false;
        }

        // update quantity
        return $this->variantRepository->update($varient, [
            'qty_available' => Arr::get($stock, 'stavMjSPozadavky')
        ]);
    }

    /**
     * load configuration file from the ch-ehsop package
     */
    public function getLocaleConfigurations(string $locale)
    {
        $configs = app(LocalesService::class)->getLocaleConfigurations();
        return Arr::get($configs, $locale, []);
    }

    /**
     * get country tax rate. im using eshop package's configuration files for this.
     */
    public function getCountryTaxRate($locale)
    {
        $localeConfig = $this->getLocaleConfigurations($locale);
        return $localeConfig['tax']['rate'] ?? 0.0;
    }

    /**
     * get country tax rate. im using eshop package's configuration files for this.
     */
    public function getCurrencyCode($locale)
    {
        $localeConfig = $this->getLocaleConfigurations($locale);
        return $localeConfig['currency']['code'] ?? "CZK";
    }

    /**
     * get ABRA's country names otherwise it wont support
     */
    public function getAfbCountry($locale)
    {
        $localeConfig = $this->getLocaleConfigurations($locale);
        return $localeConfig['afb_country'] ?? "Česká republika";
    }

    /**
     * got instructions to use Delivery type as POST 
     */
    public function getDeliveryType($deliveryType)
    {
        switch ($deliveryType) {
            case "czech_post":
            case "Česká pošta":
            case "Czech post":
                return "POST";
            default:
                return $deliveryType;
        }
    }

    /**
     * payment type codes according to ABRA
     */
    public function getPaymentCode($payementType)
    {
        switch ($payementType) {
            case "stripe":
                return "code:STRIPE";
            case "paypal":
                return "code:PAYPAL";
            default:
                return "code:NESPECIFIKOVANO";
        }
    }

    /**
     * download invoice files
     */
    public function downloadPdf(Order $order, $lang)
    {

        $reference = $this->flexibeeOrderReferenceRepository->findBy('order_id', $order->id);

        if (!$reference || !$reference->flexibee_id) {
            throw new \Exception(__("File not found"));
        }

        switch ($lang) {
            case "en-US":
            case "en-GB":
                $lang =  "en";
                break;
            default:
                $lang = $lang;
        }
        
        $reportId = $reference->flexibee_id;

        $filePath = storage_path("$order->order_no.pdf");

        $this->flexiBeeClient->processRequest('GET', "objednavka-prijata/" . $reportId . ".pdf?report-lang=$lang", [
            'sink' => $filePath
        ]);

        return $filePath;
    }

    /**
     * create new order record in ABRA
     */
    public function placeOrderInFlexiBee(Order $order)
    {
        //first check if the order is exist, if so update the reference table. 
        $flexiOrder = $this->getOrderByOrderCode($order->order_no, 'custom:kod,polozkyDokladu(kod,mnozMj)');

        if ($id = Arr::get($flexiOrder, 'winstrom.objednavka-prijata.0.id', null)) {

            $item = [
                'order_id' => $order->id,
                'flexibee_id' => $id,
                'status' => true,
            ];
            // we are going to save a reference record
            $this->flexibeeOrderReferenceRepository->updateOrCreate($item, 'order_id');

            return ['winstrom' => ['success' => true]];
        }
        
        //create order in ABRA
        $response = $this->createOrder($this->createPayload($order));

        //keep a reference record
        $this->storeReference($response, $order);

        return $response;
    }

    protected function updateOrderItemsTable(Order $order)
    {
        //first check if the otder is exist, if so update the reference table. 
        $flexiOrder = $this->getOrderByOrderCode($order->order_no, 'custom:kod,polozkyDokladu(kod,mnozMj)');

        if ($id = Arr::get($flexiOrder, 'winstrom.objednavka-prijata.0.id', null)) {

            $items = Arr::get($flexiOrder, 'winstrom.objednavka-prijata.0.polozkyDokladu', []);

            //update each item's flexi bee record ID. this will be used to identify when updating orders 
            foreach ($items as $item) {
                $order->refresh()->items()
                    ->where('code', $item['kod'])
                    ->update(['flexibee_id' => $item['id']]);
            }
        }
    }

    public function updateOrderInFlexiBee(Order $order)
    {
        //first check if the orer is exist, if so update the reference table. 
        //this time we need just custom fields
        $flexiOrder = $this->getOrderByOrderCode($order->order_no, 'custom:kod,polozkyDokladu(kod,mnozMj)');
        //update items table
        $this->updateOrderItemsTable($order);

        $id = Arr::get($flexiOrder, 'winstrom.objednavka-prijata.0.id', null);

        if (!$id) {
            \Log::error("Cannot find ABRA record for order : " . $order->order_no);
            throw new \Exception(__('ch-eshop::table.exceptions.no-abf-record'));
        }

        //update order meta data
        $updatePayload = $this->updatePayload($order, Arr::get($flexiOrder, 'winstrom.objednavka-prijata.0'));

        $response =  $this->updateOrder($updatePayload);

        $this->storeReference($response, $order);

        return $response;
    }

    private function storeReference(array $response, $order)
    {
        //if the order created successfullly
        $status = (bool) Arr::get($response, 'winstrom.success', false);

        if (!$status) {
            return false;
        }

        $item = [
            'order_id' => $order->id,
            'flexibee_id' => Arr::get($response, 'winstrom.results.0.id', null),
            'status' => (bool) $status,
            'document_url' => Arr::get($response, 'winstrom.results.0.ref', null)
        ];
        //then we are going to save a ref record
        $this->flexibeeOrderReferenceRepository->updateOrCreate($item, 'order_id');
    }


    public function getOrderItemsPayload(Order $order, $includeId = false, $deleteIds = [])
    {
        $items = $order->items;
        $payload = [];
        $taxRate = $this->getCountryTaxRate($order->language);

        foreach ($items as $item) {
            $data = [
                "kod" => $item->code,
                "eanKod" => $item->ean,
                "nazev" => json_decode($item->object)->title->{$order->language} ?? "",
                // got instructions to keep followings as it is
                "typCenyDphK" => "typCeny.bezDph",
                "typCenyDphK@showAs" => "bez DPH",
                "typSzbDphK@showAs" => "základní",
                "mnozMj" => $item->quantity,
                "cenaMj" => $item->selling_price_wo_tax,
                "sumCelkem" => $item->total_price_w_tax,
                "szbDph" => $taxRate,
                "sumZkl" => $item->total_price_wo_tax,
                "sumDph" => $item->tax,
            ];

            if ($includeId && $item->flexibee_id) {
                $data['id'] = $item->flexibee_id;
            }
            $payload[] = $data;
        }

        //delete items
        foreach ($deleteIds as $id) {
            $data['id'] = $id;
            $data['@action'] = "delete";
            $payload[] = $data;
        }


        return $payload;
    }

    /**
     *
     * prepare create order payload.
     *
     * reference: https://demo.flexibee.eu/c/demo/objednavka-prijata/properties
     *
     */
    public function createPayload(Order $order)
    {
        $payload = [
            "winstrom" => [
                "@version" => "1.0",
                "objednavka-prijata" => [
                    $this->getMetaData($order),
                ]
            ]
        ];

        return $payload;
    }

    /** 
     * prepare order meta data
     */
    protected function getMetaData(Order $order, bool $updateRecord = false, bool $withItemIds = false, array $flexiOrder = [])
    {
        $item =  [
            'kod' => $order->order_no,
            "zamekK" => "zamek.otevreno",
            "zamekK@showAs" => "Otevřeno",
            "datObj" => Carbon::parse($order->created_at)->toDateString(),
            "doprava" => $this->getDeliveryType($order->delivery_type),
            "formaUhradyCis" => $this->getPaymentCode($order->payment_method),
            "datVyst" => Carbon::parse($order->created_at)->toDateString(),
            "poznam" => $order->client_note . "\n" . $order->admin_note,
            "sumZklCelkemMen" => $order->payment_price,
            "sumDphZaklMen" => $order->total_tax,
            "sumCelkemMen" => $order->total_pay,
            "nazFirmy" => $order->billing_address_name . ' ' . $order->billing_address_surname,
            "ulice" => $order->billing_address_street,
            "mesto" => $order->billing_address_city,
            "psc" => $order->billing_address_zip,
            "ic" => $order->cin,
            "dic" => $order->vat_no,
            "faNazev2" => $order->company,
            "faUlice" => $order->delivery_address_street,
            "faMesto" => $order->delivery_address_city,
            "faPsc" => $order->delivery_address_zip,
            //following two field should be static afaik
            "typDokl" => "code:OBP",
            "typDokl@showAs" => "OBP: Objednávka přijatá",
            "mena" => "code:" . $this->getCurrencyCode($order->language),
            "stat@showAs" => $this->getAfbCountry($order->language),
            "kontaktJmeno" => $order->billing_address_name . " " . $order->billing_address_surname,
            "kontaktEmail" => $order->email,
            "kontaktTel" => $order->phone,
        ];

        if ($updateRecord) {
            $item['id'] = $flexiOrder['id'];
            $item['lastUpdate'] = Carbon::parse($order->updated_at)->toDateString();
        }

        $deleteIds = $this->getDeleteIds($order, $flexiOrder);

        //attach order items
        $item["polozkyDokladu"] = [
            $this->getOrderItemsPayload($order, $withItemIds, $deleteIds)
        ];

        return $item;
    }

    /**
     * Ids that we want to delete if any
     */
    protected function getDeleteIds(Order $order, array $flexiOrder)
    {
        $availableIds = $order->items->pluck('flexibee_id')->all();
        $flexiBeeIds = Arr::pluck(Arr::get($flexiOrder, 'polozkyDokladu', []), 'id');

        return array_diff($flexiBeeIds, $availableIds);
    }

    /**
     *
     * prepare update order payload.
     *
     * reference: https://demo.flexibee.eu/c/demo/objednavka-prijata/properties
     *
     */
    public function updatePayload(Order $order, array $flexiOrder)
    {
        $payload = [
            "winstrom" => [
                "@version" => "1.0",
                "objednavka-prijata" => [
                    $this->getMetaData($order, true, true, $flexiOrder)
                ]
            ]
        ];

        return $payload;
    }
}
