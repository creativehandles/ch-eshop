<?php

namespace Creativehandles\ChEshop\Services\FlexiBee;

use Exception;
use Facade\FlareClient\Http\Exceptions\BadResponse;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Log;
use SimpleXMLElement;

class FlexiBeeClient
{

    protected $client;

    public function __construct()
    {
        $this->client = new Client([
            "base_uri" => config("ch-eshop.flexibee.base_url") . config("ch-eshop.flexibee.company"),
            "auth" => [config("ch-eshop.flexibee.api_username"), config("ch-eshop.flexibee.api_password")],
            "headers" => [
                "Accept" => "application/json",
                "Content-Type" => "application/json"
            ]
        ]);
    }


    /**
     * $requestType - get or post
     * $url - url requesting
     * $responseType -> json or xml.
     * ex c/demo/adresar.xml?detail=custom:kod&limit=1 => for xml
     * ex c/demo/adresar.json?detail=custom:kod&limit=1 => for json
     *
     */
    public function processRequest(string $requestType, string $url, array $options = [], string $responseType = "json")
    {
        try {
            Log::info(static::class . " " . __FUNCTION__ . " requesting with " . $requestType . " " . $url, $options);

            $response = $this->client->request($requestType, $url, $options);

            Log::info(static::class . " " . __FUNCTION__ . " requested with " . $requestType . " " . $url . json_encode($options) . " response " . (string) $response->getBody());

            if (!in_array($response->getStatusCode(), [200, 201])) {
                return $this->setFailedResponse();
            }

            if ($responseType == "json") {
                return json_decode((string) $response->getBody(), true);
            }

            return  new SimpleXMLElement((string) $response->getBody());
        } catch (ClientException $e) {
            $result = json_decode($e->getResponse()->getBody()->getContents(), true);
            Log::error('failed response ', $result);
            return $result;
        } catch (BadResponseException $e) {
            $result = json_decode($e->getResponse()->getBody()->getContents(), true);
            Log::error('failed response ', $result);
            return $result;
        } catch (Exception $e) {
            Log::error($e);
            return $this->setFailedResponse();
        }
    }

    protected function setFailedResponse()
    {
        return ['winstorm' => ['success' => false]];
    }
}
