<?php

namespace Creativehandles\ChEshop\Services;

use Creativehandles\ChEshop\Models\ChEshopAttachment;
use Creativehandles\ChEshop\Models\Product;
use Creativehandles\ChEshop\Repositories\ProductRepository;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ProductService extends BaseEshopService
{
    private ProductRepository $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function getAllProduct()
    {
        return $this->productRepository->all($this->productRepository::RELATIONS);
    }

    public function getProduct($product, array $with = null)
    {
        return $product instanceof Product ?
            $product->with($with ?? $this->productRepository::RELATIONS)->first() :
            $this->productRepository->find($product, $with ?? $this->productRepository::RELATIONS);
    }

    /**
     * Store a product
     *
     * @param Request $request
     * @return Product
     * @throws Exception
     */
    public function storeProduct(Request $request)
    {
        try {
            $data = $request->validated();

            DB::beginTransaction();
            $product = $this->productRepository->make($data);
            DB::commit();

            return $product;
        } catch (Exception $ex) {
            DB::rollBack();

            throw $ex;
        }
    }

    /**
     * Update a product
     *
     * @param Request $request
     * @param Product $product
     * @return Product
     * @throws Exception
     */
    public function updateProduct(Request $request, Product $product)
    {
        try {
            $data = $request->validated();

            DB::beginTransaction();
            $product = $this->productRepository->updateDetails($data, $product);
            DB::commit();

            return $product;
        } catch (Exception $ex) {
            DB::rollBack();

            throw $ex;
        }
    }
    
    public function getAllVariant()
    {
        return $this->productRepository->allVariant();
    }
    
    public function getAllVariantOrderItemData()
    {
        return $this->productRepository->getAllVariantOrderItemData();
    }

    public function deleteVariant($variant)
    {
        return $this->productRepository->deleteVariant($variant);
    }

    public function deleteProduct($product)
    {
        return $this->productRepository->deleteProduct($product);
    }

    public function removeAttachment(ChEshopAttachment $attachment, Model $parent)
    {
        $filePath = $attachment->value;
        $this->productRepository->removeAttachment($attachment, $parent);

        if (Storage::disk(config('ch-eshop.files.disk'))->exists($filePath)) {
            Storage::disk(config('ch-eshop.files.disk'))->delete($filePath);
        }
    }
}
