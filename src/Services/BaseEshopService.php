<?php

namespace Creativehandles\ChEshop\Services;

use App\Services\Response\ExternalApiResponse;

class BaseEshopService
{
    const VIEW_PATH = 'ch-eshop::';

    public function render($view = 'index', $param = [])
    {
        return view(self::VIEW_PATH . $view, $param);
    }
}
