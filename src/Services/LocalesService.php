<?php

namespace Creativehandles\ChEshop\Services;

use Illuminate\Support\Arr;

class LocalesService
{
    /**
     * @var
     */
    private array $localeConfigurations;

    public function __construct()
    {
        $json = file_get_contents(__DIR__.'/../../storage/app/config/locales.json');
        $this->localeConfigurations = json_decode($json, true);
    }

    /**
     * Get locale configurations
     *
     * @return array [['localeCode', 'country', 'currency': ['code', 'symbol', 'symbolPositionInAmounts', 'minorUnits', 'rounding', 'minorUnitsSeparator', 'thousandSeparator'], 'tax': ['rate']]]
     */
    public function getLocaleConfigurations()
    {
        return collect($this->localeConfigurations)
            ->map(function ($locale) {
                return [
                    'localeCode' => Arr::get($locale, 'localeCode'),
                    'afb_country' => Arr::get($locale, 'afb_country'),
                    'country' => __('ch-eshop::general.countries.' . Arr::get($locale, 'country')),
                    'currency' => [
                        'code' => Arr::get($locale, 'currency.code'),
                        'symbol' => Arr::get($locale, 'currency.symbol'),
                        'symbolPositionInAmounts' => Arr::get($locale, 'currency.symbolPositionInAmounts'),
                        'minorUnits' => Arr::get($locale, 'currency.minorUnits', 0),
                        'rounding' => Arr::get($locale, 'currency.rounding', true),
                        'minorUnitsSeparator' => Arr::get($locale, 'currency.minorUnitsSeparator'),
                        'thousandSeparator' => Arr::get($locale, 'currency.thousandSeparator'),
                    ],
                    'tax' => [
                        'rate' => Arr::get($locale, 'tax.rate'),
                    ],
                ];
            })
            ->toArray();
    }

    /**
     * Get a locale configuration
     *
     * @param string $locale
     * @return array ['localeCode', 'country', 'currency': ['code', 'symbol', 'symbolPositionInAmounts', 'minorUnits', 'rounding', 'minorUnitsSeparator', 'thousandSeparator'], 'tax': ['rate']]
     */
    public function getLocaleConfiguration(string $locale)
    {
        return Arr::get($this->getLocaleConfigurations(), $locale);
    }

    /**
     * Get locale codes
     *
     * @return array
     */
    public function getLocaleCodes()
    {
        return array_keys($this->getLocaleConfigurations());
    }

    /**
     * Get currency for a locale
     *
     * @param string $locale
     * @return array ['code', 'symbol', 'symbolPositionInAmounts', 'minorUnits', 'rounding', 'minorUnitsSeparator', 'thousandSeparator']
     */
    public function getCurrencyForLocale(string $locale)
    {
        return Arr::get($this->getLocaleConfigurations(), $locale . '.currency');
    }

    /**
     * Get tax for a locale
     *
     * @param string $locale
     * @return array ['rate']
     */
    public function getTaxForLocale(string $locale)
    {
        return Arr::get($this->getLocaleConfigurations(), $locale . '.tax');
    }

    /**
     * Get tax amount for a price for a locale
     *
     * @param string $locale
     * @param string $price
     * @param string|null $taxRate
     * @return string
     */
    public function getTaxAmountForPrice(string $locale, string $price, string $taxRate = null)
    {
        $tax = $taxRate !== null ? ['rate' => $taxRate] : $this->getTaxForLocale($locale);
        $decimalTaxRate = bcdiv($tax['rate'], 100, 4);

        return $this->multiply($locale, $price, $decimalTaxRate, true);
    }

    /**
     * Calculate tax inclusive price for a locale
     *
     * @param string $locale
     * @param string $price
     * @param string|null $taxRate
     * @return string
     */
    public function getTaxInclusivePrice(string $locale, string $price, string $taxRate = null)
    {
        $taxAmount = $this->getTaxAmountForPrice($locale, $price, $taxRate);
        $currency = $this->getCurrencyForLocale($locale);
        // increase the scale to increase the accuracy of rounding
        $scale = bcadd($currency['minorUnits'], 2);
        $priceWithTax = bcadd($price, $taxAmount, $scale);

        return $this->getRoundedPrice($locale, $priceWithTax);
    }

    /**
     * Round a price as per the locale currency configuration
     *
     * @param string $locale
     * @param string $price
     * @return string
     */
    public function getRoundedPrice(string $locale, string $price)
    {
        $currency = $this->getCurrencyForLocale($locale);

        if ($currency['rounding']) {
            $price = round($price, $currency['minorUnits'], PHP_ROUND_HALF_UP);
        }

        return bcmul($price, 1, $currency['minorUnits']);
    }

    /**
     * Multiply accurately as per the locale currency configuration
     *
     * @param string $locale
     * @param string $multiplicand
     * @param string $multiplier
     * @param bool $round
     * @return string
     */
    public function multiply(string $locale, string $multiplicand, string $multiplier, bool $round = false)
    {
        $currency = $this->getCurrencyForLocale($locale);
        // increase the scale to increase the accuracy of rounding
        $scale = bcadd($currency['minorUnits'], 2);

        $result = bcmul($multiplicand, $multiplier, $scale);

        return $round ? $this->getRoundedPrice($locale, $result) : $result;
    }

    /**
     * Format a price
     *
     * @param string $locale
     * @param string $price
     * @return string
     */
    public static function formatPrice(string $locale, string $price = null)
    {
        $self = app(self::class);
        $currency = $self->getCurrencyForLocale($locale);
        $price = $price ?? '0';

        return number_format($price, $currency['minorUnits'], $currency['minorUnitsSeparator'], $currency['thousandSeparator']);
    }

    /**
     * Format a price with currency symbol
     *
     * @param string $locale
     * @param string $price
     * @return string
     */
    public static function formatPriceWithCurrencySymbol(string $locale, string $price = null)
    {
        $self = app(self::class);
        $currency = $self->getCurrencyForLocale($locale);
        $price = $price ?? '0';
        $amount = number_format($price, $currency['minorUnits'], $currency['minorUnitsSeparator'], $currency['thousandSeparator']);

        switch ($currency['symbolPositionInAmounts']) {
            case 'right_w_space':
                return $amount . ' ' . $currency['symbol'];
                break;

            case 'left_no_space':
            default:
                return $currency['symbol'] . $amount;
                break;
        }
    }
}