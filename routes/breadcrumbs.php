<?php

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;

/**
 * Order BreadCrumbs
 */
// Dashboard > Order
Breadcrumbs::for('admin.ch-eshop.order.index', function ($trail) {
    $trail->parent('dashboard');
    $trail->push(__('ch-eshop::breadcrumb.order.index.header'), route('admin.ch-eshop.order.index'));
});

Breadcrumbs::for('admin.ch-eshop.order.create', function ($trail) {
    $trail->parent('admin.ch-eshop.order.index');
    $trail->push(__('ch-eshop::breadcrumb.order.create.header'), route('admin.ch-eshop.order.create'));
});

Breadcrumbs::for('admin.ch-eshop.order.edit', function ($trail,$order) {
    $trail->parent('admin.ch-eshop.order.index');
    $trail->push(__('ch-eshop::breadcrumb.order.edit.header'), route('admin.ch-eshop.order.edit',$order->id ?? ''));
});

/**
 * Product BreadCrumbs
 */
// Dashboard > Product
Breadcrumbs::for('admin.ch-eshop.product.index', function ($trail) {
    $trail->parent('dashboard');
    $trail->push(__('ch-eshop::breadcrumb.product.index.header'), route('admin.ch-eshop.product.index'));
});

Breadcrumbs::for('admin.ch-eshop.product.create', function ($trail) {
    $trail->parent('admin.ch-eshop.product.index');
    $trail->push(__('ch-eshop::breadcrumb.product.create.header'), route('admin.ch-eshop.product.create'));
});

Breadcrumbs::for('admin.ch-eshop.product.edit', function ($trail,$order) {
    $trail->parent('admin.ch-eshop.product.index');
    $trail->push(__('ch-eshop::breadcrumb.product.edit.header'), route('admin.ch-eshop.product.edit',$order->id ?? ''));
});