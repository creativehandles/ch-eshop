<?php

/**
 * This route file is not published.
 * It is loaded from the package Service Provider.
 */

use Creativehandles\ChEshop\Http\Controllers\PluginsControllers\EShopController;
use Creativehandles\ChEshop\Http\Controllers\PluginsControllers\OrderController;
use Creativehandles\ChEshop\Http\Controllers\PluginsControllers\ProductController;
use Creativehandles\ChEshop\Http\Controllers\PluginsControllers\StatusController;
use Illuminate\Support\Facades\Route;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Mcamara\LaravelLocalization\Middleware\LaravelLocalizationRedirectFilter;
use Mcamara\LaravelLocalization\Middleware\LaravelLocalizationViewPath;
use Mcamara\LaravelLocalization\Middleware\LocaleSessionRedirect;

Route::middleware([
        'web',
        LocaleSessionRedirect::class,
        LaravelLocalizationRedirectFilter::class,
        LaravelLocalizationViewPath::class,
        'auth',
        'admin'
    ])
    ->prefix(LaravelLocalization::setLocale().'/admin')
    ->name('admin.')
    ->group(function () {
        Route::group([
            'permissionNeeded' => true, // flag to mark that these routes need permissions
            'menuCompatible' => true,   // flag to mark that routes in this group are menu compatible
        ], function () {
            Route::name('ch-eshop.')
                ->prefix('eshop')
                ->group(function () {
                    Route::delete('product/variant/{variant}', [ProductController::class, 'deleteVariant'])
                        ->name('product.delete.variant');
                    Route::resource('product', ProductController::class);

                    Route::post('order/{order}/change-state/{status}', [OrderController::class, 'ChangeOrderState'])
                        ->name('order.change.state');
                    Route::post('order/{order}/order-item', [OrderController::class, 'addOrderItem'])
                        ->name('order.add.orderItem');
                    Route::delete('order/{order}/order-item/{orderItem}', [OrderController::class, 'removeOrderItem'])
                        ->name('order.delete.orderItem');
                    Route::resource('order', OrderController::class)
                        ->except(['show']);
                    Route::get('order/create-afb/{order}', [OrderController::class, 'createAfbRecord'])
                        ->name('order.create-afb-record'); 
                    Route::get('order/update-afb/{order}', [OrderController::class, 'updateAfbRecord'])
                        ->name('order.update-afb-record');     
                    Route::get('order/download-afb/{order}', [OrderController::class, 'downloadInvoice'])
                        ->name('order.download-invoice');        

                    Route::get('home', [EShopController::class, 'index'])
                        ->name('home');
            });
        });
    });