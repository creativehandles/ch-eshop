<?php

use Creativehandles\ChEshop\Http\Controllers\APIControllers\PluginsControllers\ChEShop\DeliveryMethodsController;
use Creativehandles\ChEshop\Http\Controllers\APIControllers\PluginsControllers\ChEShop\LocaleController;
use Creativehandles\ChEshop\Http\Controllers\APIControllers\PluginsControllers\ChEShop\OrderAPIController;
use Creativehandles\ChEshop\Http\Controllers\APIControllers\PluginsControllers\ChEShop\PaymentMethodsController;
use Creativehandles\ChEshop\Http\Controllers\APIControllers\PluginsControllers\ChEShop\ProductAPIController;
use Creativehandles\ChEshop\Http\Controllers\APIControllers\PluginsControllers\ChEShop\ProductOptionsController;
use Illuminate\Support\Facades\Route;

/**
 * This route file is not published.
 * It is loaded from the package Service Provider.
 */

Route::middleware('api')
    ->prefix('api/v1/eshop/')
    ->name('api.ch-eshop.')
    ->group(function () {
        route::resource('locales', LocaleController::class)->only(['index']);
        route::resource('delivery-methods', DeliveryMethodsController::class)->only(['index']);
        route::resource('payment-methods', PaymentMethodsController::class)->only(['index']);
        route::resource('product-options', ProductOptionsController::class)->only(['index']);
        Route::resource('product', ProductAPIController::class)->only(['index', 'show']);

        // order routes
        Route::put('order/{id}/5JfMEzEj8Moh', [OrderAPIController::class, 'updateOrderStatus'])
            ->name('order.update-status');
        Route::resource('order', OrderAPIController::class)->only(['store']);
    });