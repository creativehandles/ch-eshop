<?php

/*
 * You can place your custom package configuration in here.
 */

use Carbon\Carbon;
use Ramsey\Uuid\Uuid;

return [
    'notify_emails' => explode(',', env('ESHOP_NOTIFY_EMAILS')),

    'home_item' => [
        'Status',
        'Products',
        'Orders',
    ],

    'files' => [
        'disk' => 'local',
        'storagePath' => 'ch-eshop'
    ],

    'invoice_prefix' => 'L',

    'czech_post' => [
        'url' => env('CZECH_POST_URL', 'https://b2b-test.postaonline.cz:444/restservices/ZSKService/v1/'),
        'api_token' => env('CZECH_POST_API_TOKEN', 'ae9007cf-57b9-471b-b443-cda0bb2ba31e'),
        'idcck' => env('CZECH_POST_API_IDCCK', '167463001'),
        'secret_key' => env('CZECH_POST_API_SECRET_KEY', '/qzdUBd3JnRFOcpLJNVdKGNi4sq2XpMGIMpl51bQ4XhXSc4FEZKzIxoPhbxLO4wk9hknaJjo4NEcyc2XK5JUYA=='),
        'nonce' => Uuid::uuid4(),
        'auth_timestamp' => Carbon::now()->timestamp,
        'in_test' => env('CZECH_POST_API_TEST', true),
        'loonoy_location_id' => env('CZECH_POST_LOONOY_LOCATION_ID', 2),

        // API token 1 data
        // 'test_parcel_data'=> '{ "parcelHeader": { "transmissionDate": "2022-02-01", "customerID": "M00074", "postCode": "77072", "locationNumber": 1, "goodToAccept": true }, "parcelDataList": [ { "parcelParams": { "recordID": "1", "prefixParcelCode": "DR", "weight": "1.20", "insuredValue": 1500, "amount": 1500, "currency": "CZK", "vsVoucher": "28988358", "length": 100, "width": 34, "height": 12 }, "parcelAddress": { "recordID": "1", "firstName": "Jan", "surname": "Novák", "aditionAddress": "areál MÚ", "subject": "F", "address": { "street": "Těšínská", "houseNumber": "42", "sequenceNumber": "2672", "cityPart": "Předměstí", "city": "Opava", "zipCode": "74601", "isoCountry": "CZ", "addressCode": 30821647 }, "mobilNumber": "+420603123456", "emailAddress": "jan.novak@email.cz", "custCardNum": "1000008444201" }, "parcelServices": [ "7", "41", "45", "M" ] } ] }',
        // API token 3 data
        'test_parcel_data' => '{"parcelHeader":{"transmissionDate":"2022-02-01","customerID":"M00074","postCode":"77072","locationNumber":1,"goodToAccept":true},"parcelDataList":[{"parcelParams":{"recordID":"1","prefixParcelCode":"DR","weight":"1.20","insuredValue":1500,"amount":1500,"currency":"CZK","vsVoucher":"28988358","length":100,"width":34,"height":12},"parcelAddress":{"recordID":"1","firstName":"Jan","surname":"Novák","aditionAddress":"areál MÚ","subject":"F","address":{"street":"Těšínská","houseNumber":"42","sequenceNumber":"2672","cityPart":"Předměstí","city":"Opava","zipCode":"74601","isoCountry":"CZ","addressCode":30821647},"mobilNumber":"+420603123456","emailAddress":"jan.novak@email.cz","custCardNum":"1000008444201"},"parcelServices":["7","41","45","M"]}]}',
    ],

    'flexibee' => [
        "base_url" => env("FLEXIBEE_BASE_URL", "https://demo.flexibee.eu/v2/c/"),
        "company" => env("FLEXIBEE_COMPANY", "demo/"),
        "api_username" => env("FLEXIBEE_USERNAME", "winstrom"),
        "api_password" => env("FLEXIBEE_PASSWORD", "winstrom"),
    ]
];
